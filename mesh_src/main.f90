program main
use R3D_maths
use R3D_types
use R3D_to_plots_routines
use A3D_types
use A3D_data_structure
use A3D_2D_reorder_unstrctrd
use A3Dg_2D_routines
use RSM_tensor_types

implicit none

integer              , dimension(:),   allocatable :: grp_lmnt
integer              , dimension(:,:), allocatable :: tab_int
integer              , dimension(5)                :: tag
integer                                            :: i, j, type_count_0, type_count_1, type_count_2, type_count_3
integer                                            :: k_vrbs, k_case, k_error
integer                                            :: Nps, Nlmnts, Ndgs
integer                                            :: Nps_lmnt, Nlmnts_np, Ndgs_lmnt, Nlmnts_lmnt
logical                                            :: inq_exists
logical              , dimension(:),   allocatable :: type_mask
real                 , dimension(:),   allocatable :: Area_element, dl_element, d_element, grp_lmnt_real
type(R3Dxyzvector   ), dimension(:),   allocatable :: x_vrtx, x_element
type(R3Dnn0         ), dimension(:),   allocatable :: np_nlmnt
type(R3Dnn0         ), dimension(:),   allocatable :: nlmnt_np
type(R3Ddataedge    ), dimension(:),   allocatable :: dg_data
type(R3Dnn0         ), dimension(:),   allocatable :: ndg_nlmnt
type(R3Dnn0         ), dimension(:),   allocatable :: nlmnt_nlmnt
type(R3Dnn0         ), dimension(:),   allocatable :: nlmnt_lvl
!-----------------------------------------------------------------------------------------------------
!-                                      --------------------------------------------------------------
!-ASCII msh file interface              --------------------------------------------------------------
!-                                      --------------------------------------------------------------
!-----------------------------------------------------------------------------------------------------
inquire(file='naca0012_aoa4deg.msh',exist=inq_exists); write(*,*) 'naca0012_aoa4deg.msh =', inq_exists
if (.not.inq_exists) stop ' Mesh file not found: aborting'
open (100, file='naca0012_aoa4deg.msh')
do i = 1,9; read(100,*); end do    !skip 9 first lines
read (100, *) Nps
allocate(x_vrtx      (Nps))
allocate(nlmnt_np    (Nps))
do i = 1, Nps, 1; read (100, *) j, x_vrtx(i)%x, x_vrtx(i)%y, x_vrtx(i)%z; end do
do i = 1,2; read(100,*); end do    !skip 2 next lines
read (100, *) Nlmnts
allocate(tab_int     (Nlmnts, 5))
allocate(type_mask   (Nlmnts))
tab_int = -1
do i = 1, Nlmnts, 1
read (100, *) (tab_int(i,j), j = 1, 5, 1)
! write(*  , fmt='(5(I5))') (tab_int(i,1:5))
end do
close (100)
!-get point element count--------------------------
type_mask(:) = tab_int(:,2) == 15                  !
type_count_0 = count(type_mask)                    !
!--------------------------------------------------
!-get line  element count--------------------------
type_mask(:) = tab_int(:,2) == 1                   !
type_count_1 = count(type_mask)                    !
!--------------------------------------------------
!-get tri   element count--------------------------
type_mask(:) = tab_int(:,2) == 2                   !
type_count_2 = count(type_mask)                    !
!--------------------------------------------------
!-get quad  element count--------------------------
type_mask(:) = tab_int(:,2) == 3                   !
type_count_3 = count(type_mask)                    !
!--------------------------------------------------
Nlmnts = type_count_2 + type_count_3
allocate(np_nlmnt    (Nlmnts))
allocate(ndg_nlmnt   (Nlmnts))
allocate(nlmnt_nlmnt (Nlmnts))
open (101, file='naca0012_aoa4deg.msh')
do i = 1,10+Nps+3+type_count_0+type_count_1,1; read(101,*); end do    !skip until polygon elements
!-read triangle elements---------------------------
do i = 1,type_count_2,1                            !
allocate(np_nlmnt(i)%n(3))                         !
read (101, *) tag(1:5), np_nlmnt(i)%n(1:3)         !
end do!--------------------------------------------
!-read quadrilateral elements----------------------
do i = type_count_2+1,Nlmnts,1                     !
allocate(np_nlmnt(i)%n(4))                         !
read (101, *) tag(1:5), np_nlmnt(i)%n(1:4)         !
end do!--------------------------------------------
close(101)
! open(102, file='np_nlmnt')
! do i = 1,type_count_2,1;        write(102, fmt='(4I5)') i, np_nlmnt(i)%n(1:3); end do
! do i = type_count_2+1,Nlmnts,1; write(102, fmt='(5I5)') i, np_nlmnt(i)%n(1:4); end do
! close(102)
!-----------------------------------------------------------------------------------------------------
!-                                      --------------------------------------------------------------
!-Data structure generation             --------------------------------------------------------------
!-                                      --------------------------------------------------------------
!-----------------------------------------------------------------------------------------------------
k_vrbs = 0
k_case = 0
allocate(x_element    (Nlmnts))
allocate(Area_element (Nlmnts))
allocate(dl_element   (Nlmnts))
allocate(d_element    (Nlmnts))
call R3Dg_2D_plgns_vrtcs (np_nlmnt, nlmnt_np)
call R3Dg_2D_edge_based_structure (k_vrbs, np_nlmnt, nlmnt_np, dg_data, ndg_nlmnt)
call R3D_2D_element_toolbox (k_case, k_vrbs, np_nlmnt, x_vrtx, x_element, Area_element, dl_element, d_element,&
                             k_error)
call R3D_to_plots_2D_unstrcrd (103, 'Area', 'write', 'vtk', 'A', 'A',&
                               x_vrtx, np_nlmnt, Area_element, 'cell')
call reorder_unstrctrd(ndg_nlmnt, dg_data, grp_lmnt, nlmnt_lvl)
allocate(grp_lmnt_real(size(grp_lmnt)))                                                     
grp_lmnt_real(:) = real(grp_lmnt(:))                                                        
call R3D_to_plots_2D_unstrcrd (104, 'reordered_grid', 'write', 'vtk', 'result', 'group', &  
                               x_vrtx, np_nlmnt, grp_lmnt_real, 'cell')                     
end program main
