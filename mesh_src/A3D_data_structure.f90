  module  A3D_data_structure
!-----------------------------------------------------------------------------------------------------------------------------------
!-                                                                                                                                --
!-A3D_data_structure_2D: defines data_structure types for 2-D unstructured code                                                   --
!-                                                                                                                                --
!-                                                                                                                                --
!-                                                                                                                                --
!-                                                                                                                                --
!-                                                                                                                                --
!-                                                                                                                                --
!-                                                                                                                                --
!-                                                                                                                                --
!-                                                                                                                                --
!-----------------------------------------------------------------------------------------------------------------------------------
  use RSM_tensor_types
!-----------------------------------------------------------------------
  type R3Dnn0
  sequence
  integer              , dimension(:), allocatable :: n
  end type R3Dnn0
!-----------------------------------------------------------------------
  type R3Dvn0
  sequence
  real                 , dimension(:), allocatable :: v
  end type R3Dvn0
!-----------------------------------------------------------------------
  type R3Dvvn0
  sequence
  real                 , dimension(:,:), allocatable :: v
  end type R3Dvvn0
!-----------------------------------------------------------------------
  type R3Dln0
  sequence
  logical              , dimension(:), allocatable :: l
  end type R3Dln0
!-----------------------------------------------------------------------
  type R3Dxn0
  sequence
  type(R3Dxyzvector)   , dimension(:), allocatable :: xyz
  end type R3Dxn0
!-----------------------------------------------------------------------
  type R3Ddataedge
  sequence
  integer                                          :: n_element_1
  integer                                          :: n_element_2
  integer                                          :: n_point_1
  integer                                          :: n_point_2
  end type R3Ddataedge
!-----------------------------------------------------------------------
  type A3DxyzwGp
  sequence
  type(R3Dxyzvector)   , dimension(:), allocatable :: xyzGp
  type(R3Dxyzvector)   , dimension(:), allocatable ::  dSGp
  real                 , dimension(:), allocatable ::   wGp
  end type A3DxyzwGp
!-----------------------------------------------------------------------
  type R3Dpcoeffs
  sequence
  type(R3Dvn0)         , dimension(:), allocatable :: coeffs
  end type R3Dpcoeffs
!-----------------------------------------------------------------------
  type R3Dngbdata
  sequence
  integer                                          :: n                  !           index
  integer                                          :: ngb                ! neighbour index
  type(R3Dxyzvector)                               :: d                  ! add d to ngb to get phantom
  end type R3Dngbdata
!-----------------------------------------------------------------------
  type R3DAlsqconddata
  sequence
  integer                                          :: N_cffcnts
  integer                                          :: N_ngbs
  integer                                          :: N_ngb_lvls
  real                                             :: A_lsq_norm_inf
  real                                             :: A_lsq_sym_error
  real                                             :: A_lsq_inv_norm_inf
  real                                             :: A_lsq_inv_sym_error
  end type R3DAlsqconddata
!-----------------------------------------------------------------------
  end module A3D_data_structure
