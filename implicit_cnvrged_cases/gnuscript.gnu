#set terminal postfile       (These commented lines would be used to )
#set output  "results.ps"    (generate a postscript file.            )

set terminal png size 1600,1000 enhanced font 'Verdana,14'
set output "airfoil_errors.png"
set title "Laminar Airfoil Test-case"
set autoscale xy
set xlabel "Iteration"
set ylabel "Error"
set format y "%6.2E"
set tics font 'Verdana,12'
set key right top
set grid
plot "ksrdr03RK2HLLC_0241x0229/A3D_NS_2D_error" u 1:2 w l t "RK2 CFL=1.0", "it_70000_CFL=1_Lin_its=10_JACOBI/A3D_NS_2D_error" u 1:2 w l t "Jacobi CFL=1.0", "it_70000_CFL=10_Lin_its=10_JACOBI/A3D_NS_2D_error" u 1:2 w l t "Jacobi CFL=10.0",  "it_70000_CFL=10_Lin_its=5_BLU/A3D_NS_2D_error" u 1:2 w l t "BLU CFL=10.0", "it_21000_CFL=20_Lin_its=20_JACOBI/A3D_NS_2D_error" u 1:2 w l t "Jacobi CFL=20.0", "it_12000_CFL=40_Lin_its=40_JACOBI/A3D_NS_2D_error" u 1:2 w l t "Jacobi CFL=40.0"

set terminal png size 1600,1000 enhanced font 'Verdana,14'
set output "airfoil_times.png"
set title "Laminar Airfoil Test-case"
set autoscale xy
set xlabel "Iteration"
set ylabel "wck time(s)"
set format y "%6.2E"
set tics font 'Verdana,12'
set key right top
set grid
plot "ksrdr03RK2HLLC_0241x0229/A3D_NS_2D_error" u 1:3 w l t "RK2 CFL=1.0", "it_70000_CFL=1_Lin_its=10_JACOBI/A3D_NS_2D_error" u 1:3 w l t "Jacobi CFL=1.0", "it_70000_CFL=10_Lin_its=10_JACOBI/A3D_NS_2D_error" u 1:3 w l t "Jacobi CFL=10.0",  "it_70000_CFL=10_Lin_its=5_BLU/A3D_NS_2D_error" u 1:3 w l t "BLU CFL=10.0", "it_21000_CFL=20_Lin_its=20_JACOBI/A3D_NS_2D_error" u 1:3 w l t "Jacobi CFL=20.0", "it_12000_CFL=40_Lin_its=40_JACOBI/A3D_NS_2D_error" u 1:3 w l t "Jacobi CFL=40.0"
