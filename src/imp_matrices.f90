module imp_matrices
use A3D_vrbls_types
 contains
!--------------------------------------------------------------------------------------------------
!-                     ----------------------------------------------------------------------------
!-J matrices (disabled)----------------------------------------------------------------------------
!-                     ----------------------------------------------------------------------------
!--------------------------------------------------------------------------------------------------
! subroutine nrml_flux_jac(gam,nx,ny,v0,v1,v2,v4,vxx,vxy,vyy,vzz,ve, jac)
! real,                 intent(in)   :: gam,nx,ny,v0,v1,v2,v4,vxx,vxy,vyy,vzz,ve
! real, dimension(9,9), intent(  out):: jac
! 
! jac = 0.
! jac(1,2) = nx
! jac(1,3) = ny 
! jac(2,1) = (v1**2*(gam-3)*nx - 2*v1*v2*ny + v2**2*(gam-1)*nx)/(2*v0**2)
! jac(2,2) = (v2*ny - v1*(gam-3)*nx)/(v0)
! jac(2,3) = (v1*ny - v2*(gam-1)*nx)/(v0)
! jac(2,4) = (gam-1)*nx
! jac(3,1) = (v1**2*(gam-1)*ny - 2*v1*v2*nx + v2**2*(gam-3)*ny)/(2*v0**2)
! jac(3,2) = (v2*nx - v1*(gam-1)*ny)/(v0)
! jac(3,3) = (v1*nx - v2*(gam-3)*ny)/(v0)
! jac(3,4) = (gam-1)*ny
! jac(4,1) = -(((v1*nx+v2*ny)*(v0*v4*gam - (gam-1)*(v1**2+v2**2)))/(v0**3))
! jac(4,2) = (2*gam*v0*v4*nx - (gam-1)*(3*v1**2*nx+2*v1*v2*ny+v2**2*nx))/(2*v0**2)
! jac(4,3) = (2*gam*v0*v4*ny - (gam-1)*(v1**2*ny+2*v1*v2*nx+3*v2**2*ny))/(2*v0**2)
! jac(4,4) = (gam*(v1*nx+v2*ny))/(v0)
! jac(5,1) = -(vxx*(v1*nx+v2*ny)/v0**2)
! jac(5,2) = (vxx*nx/v0)
! jac(5,3) = (vxx*ny/v0)
! jac(5,5) = (v1*nx+v2*ny)/v0
! jac(6,1) = -(vxy*(v1*nx+v2*ny)/v0**2)
! jac(6,2) = (vxy*nx/v0)
! jac(6,3) = (vxy*ny/v0)
! jac(6,6) = (v1*nx+v2*ny)/v0
! jac(7,1) = -(vyy*(v1*nx+v2*ny)/v0**2)
! jac(7,2) = (vyy*nx/v0)
! jac(7,3) = (vyy*ny/v0)
! jac(7,7) = (v1*nx+v2*ny)/v0
! jac(8,1) = -(vzz*(v1*nx+v2*ny)/v0**2)
! jac(8,2) = (vzz*nx/v0)
! jac(8,3) = (vzz*ny/v0)
! jac(8,8) = (v1*nx+v2*ny)/v0
! jac(9,1) = -(ve*(v1*nx+v2*ny)/v0**2)
! jac(9,2) = (ve*nx/v0)
! jac(9,3) = (ve*ny/v0)
! jac(9,9) = (v1*nx+v2*ny)/v0
! return
! end subroutine
!--------------------------------------------------------------------------------------------------
!-                  -------------------------------------------------------------------------------
!-J matrices laminar-------------------------------------------------------------------------------
!-                  -------------------------------------------------------------------------------
!--------------------------------------------------------------------------------------------------
subroutine nrml_flux_jac_l(gam,nx,ny,u, jac)
type(A3DuNS),                              intent(in   ) :: u
real        ,                              intent(in   ) :: gam,nx,ny
real        , dimension(4,4),              intent(  out) :: jac
!-local variables----------------------------------------------------------------------------------
real                                                     :: u0, u1, u2, u4

u0 = u%rho
u1 = u%rhoV%x
u2 = u%rhoV%y
u4 = u%rhoet

jac = 0.
jac(1,2) = nx
jac(1,3) = ny 
jac(2,1) = (u1**2*(gam-3)*nx - 2*u1*u2*ny + u2**2*(gam-1)*nx)/(2*u0**2)
jac(2,2) = (u2*ny - u1*(gam-3)*nx)/(u0)
jac(2,3) = (u1*ny - u2*(gam-1)*nx)/(u0)
jac(2,4) = (gam-1)*nx
jac(3,1) = (u1**2*(gam-1)*ny - 2*u1*u2*nx + u2**2*(gam-3)*ny)/(2*u0**2)
jac(3,2) = (u2*nx - u1*(gam-1)*ny)/(u0)
jac(3,3) = (u1*nx - u2*(gam-3)*ny)/(u0)
jac(3,4) = (gam-1)*ny
jac(4,1) = -(((u1*nx+u2*ny)*(u0*u4*gam - (gam-1)*(u1**2+u2**2)))/(u0**3))
jac(4,2) = (2*gam*u0*u4*nx - (gam-1)*(3*u1**2*nx+2*u1*u2*ny+u2**2*nx))/(2*u0**2)
jac(4,3) = (2*gam*u0*u4*ny - (gam-1)*(u1**2*ny+2*u1*u2*nx+3*u2**2*ny))/(2*u0**2)
jac(4,4) = (gam*(u1*nx+u2*ny))/(u0)
return
end subroutine
!--------------------------------------------------------------------------------------------------
!-                                -----------------------------------------------------------------
!-Conversion matrices T (disabled)-----------------------------------------------------------------
!-                                -----------------------------------------------------------------
!--------------------------------------------------------------------------------------------------
! subroutine T_conversion (gam,v0,v1,v2,v4,vxx,vxy,vyy,vzz,ve, T)
! real,                 intent(in)   :: gam,v0,v1,v2,v4,vxx,vxy,vyy,vzz,ve
! real, dimension(9,9), intent(  out):: T
! 
! T = 0.
! T(1,1)    = 1.
! T(2,1)    = v1/v0
! T(2,2)    = v0
! T(3,1)    = v2/v0
! T(3,3)    = v0
! T(4,1)    = 0.5*((v1/v0)**2+(v2/v0)**2)
! T(4,2)    = v1
! T(4,3)    = v2
! T(4,4)    = 1/(gam-1.)
! T(5,1)    = vxx/v0
! T(5,5)    = v0
! T(6,1)    = vxy/v0
! T(6,6)    = v0
! T(7,1)    = vyy/v0
! T(7,7)    = v0
! T(8,1)    = vzz/v0
! T(8,8)    = v0
! T(9,1)    = ve/v0
! T(9,9)    = v0
! return 
! end subroutine
!--------------------------------------------------------------------------------------------------
!-                             --------------------------------------------------------------------
!-Conversion matrices T laminar--------------------------------------------------------------------
!-                             --------------------------------------------------------------------
!--------------------------------------------------------------------------------------------------
subroutine T_conversion_l (gam,u, T)
type(A3DuNS),                              intent(in   ) :: u
real        ,                              intent(in   ) :: gam
real        , dimension(4,4),              intent(  out) :: T
!-local variables----------------------------------------------------------------------------------
real                                                     :: u0, u1, u2, u4

u0 = u%rho
u1 = u%rhoV%x
u2 = u%rhoV%y
u4 = u%rhoet

T = 0.
T(1,1)    = 1.
T(2,1)    = u1/u0
T(2,2)    = u0
T(3,1)    = u2/u0
T(3,3)    = u0
T(4,1)    = 0.5*((u1/u0)**2+(u2/u0)**2)
T(4,2)    = u1
T(4,3)    = u2
T(4,4)    = 1/(gam-1.)
return 
end subroutine
!--------------------------------------------------------------------------------------------------
!-                                           ------------------------------------------------------
!-Inverse conversion matrices Tinv (disabled)------------------------------------------------------
!-                                           ------------------------------------------------------
!--------------------------------------------------------------------------------------------------
! subroutine Tinv_conversion (gam,v0,v1,v2,v4,vxx,vxy,vyy,vzz,ve, Tinv)
! real,                 intent(in)   :: gam,v0,v1,v2,v4,vxx,vxy,vyy,vzz,ve
! real, dimension(9,9), intent(  out):: Tinv
! 
! Tinv = 0.
! Tinv(1,1) = 1.
! Tinv(2,1) = -v1/v0**2
! Tinv(2,2) = 1/v0
! Tinv(3,1) = -v2/v0**2
! Tinv(3,3) = 1/v0
! Tinv(4,1) = (gam-1)*0.5*((v1/v0)**2+(v2/v0)**2)
! Tinv(4,2) = (1-gam)*(v1/v0)
! Tinv(4,3) = (1-gam)*(v2/v0)
! Tinv(4,4) = gam-1
! Tinv(5,1) = -vxx/v0**2
! Tinv(5,5) = 1/v0
! Tinv(6,1) = -vxy/v0**2
! Tinv(6,6) = 1/v0
! Tinv(7,1) = -vyy/v0**2
! Tinv(7,7) = 1/v0
! Tinv(8,1) = -vzz/v0**2
! Tinv(8,8) = 1/v0
! Tinv(9,1) = -ve/v0**2
! Tinv(9,9) = 1/v0
! return 
! end subroutine
!--------------------------------------------------------------------------------------------------
!-                                        ---------------------------------------------------------
!-Inverse conversion matrices Tinv laminar---------------------------------------------------------
!-                                        ---------------------------------------------------------
!--------------------------------------------------------------------------------------------------
subroutine Tinv_conversion_l (gam,u, Tinv)
type(A3DuNS),                              intent(in   ) :: u
real        ,                              intent(in   ) :: gam
real        , dimension(4,4),              intent(  out) :: Tinv
!-local variables----------------------------------------------------------------------------------
real                                                     :: u0, u1, u2, u4

u0 = u%rho
u1 = u%rhoV%x
u2 = u%rhoV%y
u4 = u%rhoet

Tinv = 0.
Tinv(1,1) = 1.
Tinv(2,1) = -u1/u0**2
Tinv(2,2) = 1/u0
Tinv(3,1) = -u2/u0**2
Tinv(3,3) = 1/u0
Tinv(4,1) = (gam-1)*0.5*((u1/u0)**2+(u2/u0)**2)
Tinv(4,2) = (1-gam)*(u1/u0)
Tinv(4,3) = (1-gam)*(u2/u0)
Tinv(4,4) = gam-1
return 
end subroutine
!--------------------------------------------------------------------------------------------------
!-                                                  -----------------------------------------------
!-Unified Implicit Subsonic Inflow/Outflow Treatment-----------------------------------------------
!-10/01/2019 Xiasu Yang                             -----------------------------------------------
!-                                                  -----------------------------------------------
!--------------------------------------------------------------------------------------------------
subroutine sub_uni_jij(reservoir, gam, nx, ny, vIN, vBC, jac)
logical                     , intent(in   ) :: reservoir
real                        , intent(in   ) :: gam
real                        , intent(in   ) :: nx
real                        , intent(in   ) :: ny
type(A3DvNS)                , intent(in   ) :: vIN
type(A3DvNS)                , intent(in   ) :: vBC
real        , dimension(4,4), intent(  out) :: jac
!-local variables----------------------------------------------------------------------------------
real                                        :: rho_i, p_i, a_i, rho_j, u_j, v_j, u_i, v_i, p_j, a_j
real                                        :: un_j, ut_j, un_i, M1, M2

rho_i = vIN%rho
rho_j = vBC%rho
p_i   = vIN%p
p_j   = vBC%p
u_i   = vIN%V%x
u_j   = vBC%V%x
v_i   = vIN%V%y
v_j   = vBC%V%y
a_i   = sqrt(gam*p_i/rho_i)
a_j   = sqrt(gam*p_j/rho_j)
un_i  =  u_i*nx + v_i*ny
un_j  =  u_j*nx + v_j*ny
ut_j  = -u_j*ny + v_j*nx
if (reservoir) then
    M1    = un_j/a_j
    M2    = ut_j/a_j
else
    M1    = 1.
    M2    = 0.
end if

jac = 0.

if (un_i < 0) then!=outflow: entropy variation========================================================================!
    jac(1,1) = (-M1/(1.+M1))*rho_j*a_i/((gam-1.)*rho_i*a_j) + rho_j*a_i**2./((gam-1.)*p_i)                            !
                                                                                                                      !
    jac(1,2) = (-(M1*nx-M2*ny)/(1.+M1))*rho_j/a_j                                                                     !
                                                                                                                      !
    jac(1,3) = (-(M1*ny+M2*nx)/(1.+M1))*rho_j/a_j                                                                     !
                                                                                                                      !
    jac(1,4) = (M1/(1.+M1))*rho_j*a_i/((gam-1.)*p_i*a_j) - rho_j/((gam-1.)*p_i)                                       !
                                                                                                                      !
    jac(2,1) = (1./(1.+M1))*a_i*nx/((gam-1.)*rho_i)                                                                   !
                                                                                                                      !
    jac(2,2) = (1./(1.+M1))*(nx**2.+M2*nx*ny) + ny**2.                                                                !
                                                                                                                      !
    jac(2,3) = (1./(1.+M1))*(nx*ny-M2*nx**2.) - nx*ny                                                                 !
                                                                                                                      !
    jac(2,4) = (-1./(1.+M1))*a_i*nx/((gam-1.)*p_i)                                                                    !
                                                                                                                      !
    jac(3,1) = (1./(1.+M1))*a_i*ny/((gam-1.)*rho_i)                                                                   !
                                                                                                                      !
    jac(3,2) = (1./(1.+M1))*(nx*ny+M2*ny**2.) - nx*ny                                                                 !
                                                                                                                      !
    jac(3,3) = (1./(1.+M1))*(ny**2.-M2*nx*ny) + nx**2.                                                                !
                                                                                                                      !
    jac(3,4) = (-1./(1.+M1))*a_i*ny/((gam-1.)*p_i)                                                                    !
                                                                                                                      !
    jac(4,1) = (-M1/(1.+M1))*(gam*p_j)*a_i/((gam-1.)*rho_i*a_j) + p_j*a_i**2./((gam-1.)*p_i)                          !
                                                                                                                      !
    jac(4,2) = (-(M1*nx-M2*ny)/(1.+M1))*(gam*p_j)/a_j                                                                 !
                                                                                                                      !
    jac(4,3) = (-(M1*ny+M2*nx)/(1.+M1))*(gam*p_j)/a_j                                                                 !
                                                                                                                      !
    jac(4,4) = (M1/(1.+M1))*(gam*p_j)*a_i/((gam-1.)*p_i*a_j) - p_j/((gam-1.)*p_i)                                     !
else!=inflow: isentropic==============================================================================================!
    jac(1,1) = (M1/(1.+M1))*rho_j*a_i/(rho_i*a_j)                                                                     !
                                                                                                                      !
    jac(1,2) = (-(M1*nx-M2*ny)/(1.+M1))*rho_j/a_j                                                                     !
                                                                                                                      !
    jac(1,3) = (-(M1*ny+M2*nx)/(1.+M1))*rho_j/a_j                                                                     !
                                                                                                                      !
    jac(1,4) = 0.                                                                                                     !
                                                                                                                      !
    jac(2,1) = (-1./(1.+M1))*a_i*nx/rho_i                                                                             !
                                                                                                                      !
    jac(2,2) = (1./(1.+M1))*(nx**2.+M2*nx*ny) + ny**2.                                                                !
                                                                                                                      !
    jac(2,3) = (1./(1.+M1))*(nx*ny-M2*nx**2.) - nx*ny                                                                 !
                                                                                                                      !
    jac(2,4) = 0.                                                                                                     !
                                                                                                                      !
    jac(3,1) = (-1./(1.+M1))*a_i*ny/rho_i                                                                             !
                                                                                                                      !
    jac(3,2) = (1./(1.+M1))*(nx*ny+M2*ny**2.) - nx*ny                                                                 !
                                                                                                                      !
    jac(3,3) = (1./(1.+M1))*(ny**2.-M2*nx*ny) + nx**2.                                                                !
                                                                                                                      !
    jac(3,4) = 0.                                                                                                     !
                                                                                                                      !
    jac(4,1) = (M1/(1.+M1))*(gam*p_j)*a_i/(rho_i*a_j)                                                                 !
                                                                                                                      !
    jac(4,2) = (-(M1*nx-M2*ny)/(1.+M1))*(gam*p_j)/a_j                                                                 !
                                                                                                                      !
    jac(4,3) = (-(M1*ny+M2*nx)/(1.+M1))*(gam*p_j)/a_j                                                                 !
                                                                                                                      !
    jac(4,4) = 0.                                                                                                     !
end if!===============================================================================================================
return
end subroutine sub_uni_jij
!--------------------------------------------------------------------------------------------------
!-                                                                ---------------------------------
!-Exact Jacobian matrices for Riemann BC obtained from Wolfram Lab---------------------------------
!-11/01/2019 Xiasu Yang                                           ---------------------------------
!-                                                                ---------------------------------
!--------------------------------------------------------------------------------------------------
subroutine riemann_jij(gam, RG, nx, ny, db, pb, vxb, vyb, Tb, vIN, jac)
real                        , intent(in   ) :: gam
real                        , intent(in   ) :: RG
real                        , intent(in   ) :: nx
real                        , intent(in   ) :: ny
real                        , intent(in   ) :: db
real                        , intent(in   ) :: pb
real                        , intent(in   ) :: vxb
real                        , intent(in   ) :: vyb
real                        , intent(in   ) :: Tb
type(A3DvNS)                , intent(in   ) :: vIN
real        , dimension(4,4), intent(  out) :: jac
!-local variables----------------------------------------------------------------------------------
real                                        :: di, pi, vxi, vyi, Ti, vni

di  = vIN%rho
pi  = vIN%p
vxi = vIN%V%x
vyi = vIN%V%y
Ti  = pi/RG/di   
vni = vxi*nx + vyi*ny
jac = 0.
if (vni >= 0) then !=INFLOW================================================= 
jac(1,1)=-((2**((-5+gam)/(-1+gam))*db*Sqrt((gam*pi)/di)*((2*(Sqrt((gam*pb)/db)+Sqrt((gam*pi)/di))+(-1+gam)*nx*(vxb-vxi)+(-1+gam)*ny*(vyb-vyi))**2/(gam*RG*Tb))**(1/(-1+gam)))/(di*(-1+gam)*(2*(Sqrt((gam*pb)/db)+Sqrt((gam*pi)/di))+(-1+gam)*nx*(vxb-vxi)+(-1+gam)*ny*(vyb-vyi))))

jac(1,2)=-((2**((-5+gam)/(-1+gam))*db*nx*((2*(Sqrt((gam*pb)/db)+Sqrt((gam*pi)/di))+(-1+gam)*nx*(vxb-vxi)+(-1+gam)*ny*(vyb-vyi))**2/(gam*RG*Tb))**(1/(-1+gam)))/(2*(Sqrt((gam*pb)/db)+Sqrt((gam*pi)/di))+(-1+gam)*nx*(vxb-vxi)+(-1+gam)*ny*(vyb-vyi)))

jac(1,3)=-((2**((-5+gam)/(-1+gam))*db*ny*((2*(Sqrt((gam*pb)/db)+Sqrt((gam*pi)/di))+(-1+gam)*nx*(vxb-vxi)+(-1+gam)*ny*(vyb-vyi))**2/(gam*RG*Tb))**(1/(-1+gam)))/(2*(Sqrt((gam*pb)/db)+Sqrt((gam*pi)/di))+(-1+gam)*nx*(vxb-vxi)+(-1+gam)*ny*(vyb-vyi)))

jac(1,4)=(2**((-5+gam)/(-1+gam))*db*Sqrt((gam*pi)/di)*((2*(Sqrt((gam*pb)/db)+Sqrt((gam*pi)/di))+(-1+gam)*nx*(vxb-vxi)+(-1+gam)*ny*(vyb-vyi))**2/(gam*RG*Tb))**(1/(-1+gam)))/((-1+gam)*pi*(2*(Sqrt((gam*pb)/db)+Sqrt((gam*pi)/di))+(-1+gam)*nx*(vxb-vxi)+(-1+gam)*ny*(vyb-vyi)))

jac(2,1)=-((nx*Sqrt((gam*pi)/di))/(2*di-2*di*gam))

jac(2,2)=nx**2/2.

jac(2,3)=(nx*ny)/2.

jac(2,4)=(nx*Sqrt((gam*pi)/di))/(2*pi-2*gam*pi)

jac(3,1)=-((ny*Sqrt((gam*pi)/di))/(2*di-2*di*gam))

jac(3,2)=(nx*ny)/2.

jac(3,3)=ny**2/2.

jac(3,4)=(ny*Sqrt((gam*pi)/di))/(2*pi-2*gam*pi)

jac(4,1)=-((2**(1-(4*gam)/(-1+gam))*pb*((gam*pi)/di)**1.5*((2*(Sqrt((gam*pb)/db)+Sqrt((gam*pi)/di))+(-1+gam)*nx*(vxb-vxi)+(-1+gam)*ny*(vyb-vyi))**2/(gam*RG*Tb))**(gam/(-1+gam)))/((-1+gam)*pi*(2*(Sqrt((gam*pb)/db)+Sqrt((gam*pi)/di))+(-1+gam)*nx*(vxb-vxi)+(-1+gam)*ny*(vyb-vyi))))

jac(4,2)=-((2**(1-(4*gam)/(-1+gam))*gam*nx*pb*((2*(Sqrt((gam*pb)/db)+Sqrt((gam*pi)/di))+(-1+gam)*nx*(vxb-vxi)+(-1+gam)*ny*(vyb-vyi))**2/(gam*RG*Tb))**(gam/(-1+gam)))/(2*(Sqrt((gam*pb)/db)+Sqrt((gam*pi)/di))+(-1+gam)*nx*(vxb-vxi)+(-1+gam)*ny*(vyb-vyi)))

jac(4,3)=-((2**(1-(4*gam)/(-1+gam))*gam*ny*pb*((2*(Sqrt((gam*pb)/db)+Sqrt((gam*pi)/di))+(-1+gam)*nx*(vxb-vxi)+(-1+gam)*ny*(vyb-vyi))**2/(gam*RG*Tb))**(gam/(-1+gam)))/(2*(Sqrt((gam*pb)/db)+Sqrt((gam*pi)/di))+(-1+gam)*nx*(vxb-vxi)+(-1+gam)*ny*(vyb-vyi)))

jac(4,4)=(2**(1-(4*gam)/(-1+gam))*gam*pb*Sqrt((gam*pi)/di)*((2*(Sqrt((gam*pb)/db)+Sqrt((gam*pi)/di))+(-1+gam)*nx*(vxb-vxi)+(-1+gam)*ny*(vyb-vyi))**2/(gam*RG*Tb))**(gam/(-1+gam)))/((-1+gam)*pi*(2*(Sqrt((gam*pb)/db)+Sqrt((gam*pi)/di))+(-1+gam)*nx*(vxb-vxi)+(-1+gam)*ny*(vyb-vyi)))
else!=OUTFLOW================================================
jac(1,1)=16**(1/(1-gam))*(1-(2*Sqrt((gam*pi)/di))/((-1+gam)*(2*(Sqrt((gam*pb)/db)+Sqrt((gam*pi)/di))+(-1+gam)*nx*(vxb-vxi)+(-1+gam)*ny*(vyb-vyi))))*((2*(Sqrt((gam*pb)/db)+Sqrt((gam*pi)/di))+(-1+gam)*nx*(vxb-vxi)+(-1+gam)*ny*(vyb-vyi))**2/(gam*RG*Ti))**(1/(-1+gam))

jac(1,2)=-((2**((-5+gam)/(-1+gam))*di*nx*((2*(Sqrt((gam*pb)/db)+Sqrt((gam*pi)/di))+(-1+gam)*nx*(vxb-vxi)+(-1+gam)*ny*(vyb-vyi))**2/(gam*RG*Ti))**(1/(-1+gam)))/(2*(Sqrt((gam*pb)/db)+Sqrt((gam*pi)/di))+(-1+gam)*nx*(vxb-vxi)+(-1+gam)*ny*(vyb-vyi)))

jac(1,3)=-((2**((-5+gam)/(-1+gam))*di*ny*((2*(Sqrt((gam*pb)/db)+Sqrt((gam*pi)/di))+(-1+gam)*nx*(vxb-vxi)+(-1+gam)*ny*(vyb-vyi))**2/(gam*RG*Ti))**(1/(-1+gam)))/(2*(Sqrt((gam*pb)/db)+Sqrt((gam*pi)/di))+(-1+gam)*nx*(vxb-vxi)+(-1+gam)*ny*(vyb-vyi)))

jac(1,4)=(2**((-5+gam)/(-1+gam))*gam*((2*(Sqrt((gam*pb)/db)+Sqrt((gam*pi)/di))+(-1+gam)*nx*(vxb-vxi)+(-1+gam)*ny*(vyb-vyi))**2/(gam*RG*Ti))**(1/(-1+gam)))/((-1+gam)*Sqrt((gam*pi)/di)*(2*(Sqrt((gam*pb)/db)+Sqrt((gam*pi)/di))+(-1+gam)*nx*(vxb-vxi)+(-1+gam)*ny*(vyb-vyi)))

jac(2,1)=-((nx*Sqrt((gam*pi)/di))/(2*di-2*di*gam))

jac(2,2)=1-nx**2/2.

jac(2,3)=-(nx*ny)/2.

jac(2,4)=(nx*Sqrt((gam*pi)/di))/(2*pi-2*gam*pi)

jac(3,1)=-((ny*Sqrt((gam*pi)/di))/(2*di-2*di*gam))

jac(3,2)=-(nx*ny)/2.

jac(3,3)=1-ny**2/2.

jac(3,4)=(ny*Sqrt((gam*pi)/di))/(2*pi-2*gam*pi)

jac(4,1)=-((2**(1-(4*gam)/(-1+gam))*((gam*pi)/di)**1.5*((2*(Sqrt((gam*pb)/db)+Sqrt((gam*pi)/di))+(-1+gam)*nx*(vxb-vxi)+(-1+gam)*ny*(vyb-vyi))**2/(gam*RG*Ti))**(gam/(-1+gam)))/((-1+gam)*(2*(Sqrt((gam*pb)/db)+Sqrt((gam*pi)/di))+(-1+gam)*nx*(vxb-vxi)+(-1+gam)*ny*(vyb-vyi))))

jac(4,2)=-((2**(1-(4*gam)/(-1+gam))*gam*nx*pi*((2*(Sqrt((gam*pb)/db)+Sqrt((gam*pi)/di))+(-1+gam)*nx*(vxb-vxi)+(-1+gam)*ny*(vyb-vyi))**2/(gam*RG*Ti))**(gam/(-1+gam)))/(2*(Sqrt((gam*pb)/db)+Sqrt((gam*pi)/di))+(-1+gam)*nx*(vxb-vxi)+(-1+gam)*ny*(vyb-vyi)))

jac(4,3)=-((2**(1-(4*gam)/(-1+gam))*gam*ny*pi*((2*(Sqrt((gam*pb)/db)+Sqrt((gam*pi)/di))+(-1+gam)*nx*(vxb-vxi)+(-1+gam)*ny*(vyb-vyi))**2/(gam*RG*Ti))**(gam/(-1+gam)))/(2*(Sqrt((gam*pb)/db)+Sqrt((gam*pi)/di))+(-1+gam)*nx*(vxb-vxi)+(-1+gam)*ny*(vyb-vyi)))

jac(4,4)=((1+(2*gam*Sqrt((gam*pi)/di))/((-1+gam)*(2*(Sqrt((gam*pb)/db)+Sqrt((gam*pi)/di))+(-1+gam)*nx*(vxb-vxi)+(-1+gam)*ny*(vyb-vyi))))*((2*(Sqrt((gam*pb)/db)+Sqrt((gam*pi)/di))+(-1+gam)*nx*(vxb-vxi)+(-1+gam)*ny*(vyb-vyi))**2/(gam*RG*Ti))**(gam/(-1+gam)))/2**((4*gam)/(-1+gam))
end if
return
end subroutine riemann_jij

end module imp_matrices
