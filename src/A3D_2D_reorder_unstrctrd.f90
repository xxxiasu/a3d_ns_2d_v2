module A3D_2D_reorder_unstrctrd
 contains
subroutine reorder_unstrctrd(n_edge_element, dg_data,  n_group_element, n_element_lvl)
!-----------------------------------------------------------------------
!-    ------------------------------------------------------------------
!-type------------------------------------------------------------------
!-    ------------------------------------------------------------------
!-----------------------------------------------------------------------
use R3D_types
use A3D_data_structure
!-----------------------------------------------------------------------
!-         -------------------------------------------------------------
!-interface-------------------------------------------------------------
!-         -------------------------------------------------------------
!-----------------------------------------------------------------------
type(R3Dnn0         ), dimension(:)  , allocatable, intent(in   ) :: n_edge_element
type(R3Ddataedge    ), dimension(:)  , allocatable, intent(in   ) :: dg_data
integer              , dimension(:)  , allocatable, intent(  out) :: n_group_element ! reordered group for each element
type(R3Dnn0         ), dimension(:)  , allocatable, intent(  out) :: n_element_lvl
!-----------------------------------------------------------------------
!-               -------------------------------------------------------
!-local_variables-------------------------------------------------------
!-               -------------------------------------------------------
!-----------------------------------------------------------------------
integer                                                           :: grph_clr       !select graph coloring algorithm
integer                                                           :: error_count    !final connectivity check, error if > 0
integer                                                           :: N_elements
integer                                                           :: N_edges_element
integer              , dimension(:,:), allocatable                :: n_group_element_pr
integer              , dimension(:)  , allocatable                :: group_old
integer              , dimension(:)  , allocatable                :: group_new
logical                                                           :: dividable      !for my method
logical                                                           :: connected      !for Welsh-Powell method
logical                                                           :: mask_one
logical              , dimension(:)  , allocatable                :: mask
logical              , dimension(:)  , allocatable                :: mask_count
type(R3Dnn0         ), dimension(:)  , allocatable                :: n_lmnt_lmnt
!-----------------------------------------------------------------------
!-                    --------------------------------------------------
!-inquire_and_allocate--------------------------------------------------
!-                    --------------------------------------------------
!-----------------------------------------------------------------------
N_elements=size(n_edge_element)
if (allocated(n_group_element_pr)) deallocate(n_group_element_pr); allocate (n_group_element_pr(N_elements,2))
if (allocated(n_group_element))    deallocate(n_group_element);    allocate (n_group_element   (N_elements))
if (allocated(mask))               deallocate(mask);               allocate (mask              (N_elements))
if (allocated(mask_count))         deallocate(mask_count);         allocate (mask_count        (N_elements))
if (allocated(group_new))          deallocate(group_new);          allocate (group_new         (N_elements))
if (allocated(n_lmnt_lmnt))        deallocate(n_lmnt_lmnt);        allocate (n_lmnt_lmnt       (N_elements))
!-----------------------------------------------------------------------
!-                                   -----------------------------------
!-build elment-element data structure-----------------------------------
!-                                   -----------------------------------
!-----------------------------------------------------------------------
do n_element                  = 1, N_elements!------------------------------------------------------------------
N_edges_element               = size(n_edge_element(n_element)%n)                                               !
allocate (n_lmnt_lmnt(n_element)%n(N_edges_element))                                                            !
do ind                        = 1, N_edges_element; n_edge  = n_edge_element(n_element)%n(ind)!--------------   !
n_lmnt_lmnt(n_element)%n(ind) = dg_data(n_edge)%n_element_1 + dg_data(n_edge)%n_element_2 - n_element        !  !
end do!------------------------------------------------------------------------------------------------------   !
end do!---------------------------------------------------------------------------------------------------------
!-----------------------------------------------------------------------
!-                       -----------------------------------------------
!-define reordered groups-----------------------------------------------
!-                       -----------------------------------------------
!-----------------------------------------------------------------------
n_group_element_pr(:,1)         = 0; n_group_element_pr(:,2) = 0
mask                            = .true.
!-----------------------------------------------------------------------
!-                         ---------------------------------------------
!-define preliminary groups---------------------------------------------
!-                         ---------------------------------------------
!-----------------------------------------------------------------------
N_one                           = 1 ! number of cells in the FIRST hyperplane (user-defined)
n_level                         = 1
group_new(1)                    = 1
n_group_element_pr(1,1)         = n_level ! set element No.1 as group 1
mask                (1)         = .false.
mask_one                        = .false.
N_new                           = 1
allocate (group_old(1))
group_old                       = 1
do n_element                    = 2, N_elements!----------------------------------------------------------------
do ind                          = 1, size(group_old)!--------------------------------------------------------   !
mask_one                        = any(n_lmnt_lmnt(group_old(ind))%n == n_element)                            !  !
if (mask_one) exit                                                                                           !  !
end do!------------------------------------------------------------------------------------------------------   !
if ((.not.(mask_one)) .and. N_new<N_one) then!---------------------------------------------------------------   !
N_new                           = N_new + 1                                                                  !  !
group_new(N_new)                = n_element                                                                  !  !
n_group_element_pr(n_element,1) = n_level                                                                    !  !
mask              (n_element)   = .false.                                                                    !  !
deallocate (group_old); allocate (group_old(N_new)); group_old(1:N_new) = group_new(1:N_new)                 !  !
end if!------------------------------------------------------------------------------------------------------   !
end do!---------------------------------------------------------------------------------------------------------
do while (any(mask))!-------------------------------------------------------------------------------------------------    
n_level                            =  n_level + 1                                                                     !
N_new                              =  0                                                                               !
group_new                          =  0                                                                               !
do ind = 1, size(group_old)               ; n_element    = group_old(ind)!-----------------------------------------   !
do jnd = 1, size(n_lmnt_lmnt(n_element)%n); n_element_nb = n_lmnt_lmnt(n_element)%n(jnd)!-----------------------   !  !
if (mask(n_element_nb)) then!--------------------------------------------------------------------------------   !  !  !
N_new                              = N_new + 1                                                               !  !  !  !
group_new(N_new)                   = n_element_nb                                                            !  !  !  !
n_group_element_pr(n_element_nb,1) = n_level                                                                 !  !  !  !
mask              (n_element_nb)   = .false.                                                                 !  !  !  !
end if!------------------------------------------------------------------------------------------------------   !  !  !
end do!---------------------------------------------------------------------------------------------------------   !  !
end do!------------------------------------------------------------------------------------------------------------   !
deallocate (group_old); allocate (group_old(N_new)); group_old(1:N_new) = group_new(1:N_new)                          !
!-----------------------------------------------------------------------                                              !
!-                                  ------------------------------------                                              !
!-Sort into valence descending order------------------------------------                                              !
!-                                  ------------------------------------                                              !
!-----------------------------------------------------------------------                                              !
!=insertion sort=============================================================================================         !
do  i  = 2, N_new; n_element_i = group_old(i)!---------------------------------                              !        !
nbs_i  = 0                                                                     !                             !        !
do ind = 1, size(n_lmnt_lmnt(n_element_i)%n)!-------------------------         !                             !        !
if (any(group_old==n_lmnt_lmnt(n_element_i)%n(ind))) nbs_i = nbs_i+1  !        !                             !        !
end do!---------------------------------------------------------------         !                             !        !
    j  = i-1     ; n_element_j = group_old(j)                                  !                             !        !
nbs_j  = 0                                                                     !                             !        !
do jnd = 1, size(n_lmnt_lmnt(n_element_j)%n)!-------------------------         !                             !        !
if (any(group_old==n_lmnt_lmnt(n_element_j)%n(jnd))) nbs_j = nbs_j+1  !        !                             !        !
end do!---------------------------------------------------------------         !                             !        !
do while ((j>=1).and.(nbs_i>nbs_j))!----------------------------------------   !                             !        !
group_old(j+1) = group_old(j); j = j-1                                      !  !                             !        !
if (j>=1) then!----------------------------------------------------------   !  !                             !        !
n_element_j = group_old(j)                                               !  !  !                             !        !
nbs_j  = 0                                                               !  !  !                             !        !
do jnd = 1, size(n_lmnt_lmnt(n_element_j)%n)!-------------------------   !  !  !                             !        !
if (any(group_old==n_lmnt_lmnt(n_element_j)%n(jnd))) nbs_j = nbs_j+1  !  !  !  !                             !        !
end do!---------------------------------------------------------------   !  !  !                             !        !
end if!------------------------------------------------------------------   !  !                             !        !
end do!---------------------------------------------------------------------   !                             !        !
group_old(j+1) = n_element_i                                                   !                             !        !
end do!------------------------------------------------------------------------                              !        !
!============================================================================================================         !
!-----------------------------------------------------------------------                                              !
!-                                             -------------------------                                              !
!-HP subdivision with graph coloring algorithms-------------------------                                              !
!-                                             -------------------------                                              !
!-----------------------------------------------------------------------                                              !
grph_clr = 1                                                                                                          !
select case(grph_clr)                                                                                                 !
case(1)                                                                                                               !
!=my method==================================================================================================         !
k       = 0                                                                                                  !        !
dividable = .true.                                                                                           !        !
do while (dividable)!-------------------------------------------------------------                           !        !
do i    = 1, N_new; if (n_group_element_pr(group_old(i),2) == k) then!---------   !                          !        !
do j    = 1, size(n_lmnt_lmnt(group_old(i))%n)!-----------------------------   !  !                          !        !
if ((n_group_element_pr(n_lmnt_lmnt(group_old(i))%n(j),2) == k).and.&       !  !  !                          !        !
    (any(group_old == n_lmnt_lmnt(group_old(i))%n(j)))) then!------------   !  !  !                          !        !
n_group_element_pr(n_lmnt_lmnt(group_old(i))%n(j),2)  = k+1              !  !  !  !                          !        !
end if; end do!-------------------------------------------------------------   !  !                          !        !
end if; end do!----------------------------------------------------------------   !                          !        !
mask_count  = n_group_element_pr(:,2) == k+1                                      !                          !        !
if (count(mask_count) == 0) dividable = .false.                                   !                          !        !
k       = k+1                                                                     !                          !        !
end do!---------------------------------------------------------------------------                           !        !
!============================================================================================================         !
case(2)                                                                                                               !
!=Welsh-Powell method========================================================================================         !
k         = 1                                                                                                !        !
do while (any(n_group_element_pr(group_old,2) == 0))!------------------------------------------              !        !
do i      = 1, N_new; ind_glb = group_old(i); if (n_group_element_pr(ind_glb,2) == 0) then!-   !             !        !
connected = .false.                                                                         !  !             !        !
do j      = 1, size(n_lmnt_lmnt(ind_glb)%n); jnd_glb = n_lmnt_lmnt(ind_glb)%n(j)!-          !  !             !        !
if (any(group_old == jnd_glb)) connected = n_group_element_pr(jnd_glb,2) == k     !         !  !             !        !
if (connected) exit                                                               !         !  !             !        !
end do!---------------------------------------------------------------------------          !  !             !        !
if (.not.(connected)) n_group_element_pr(ind_glb,2) = k                                     !  !             !        !
end if; end do!-----------------------------------------------------------------------------   !             !        !
k         = k+1                                                                                !             !        !
end do!----------------------------------------------------------------------------------------              !        !
n_group_element_pr(group_old,2) = n_group_element_pr(group_old,2) - 1                                        !        !
!============================================================================================================         !
end select                                                                                                            !
n_level     = n_level + maxval(n_group_element_pr(group_old,2))                                                       !
! print*, 'number of TRUE cells:', count(mask), ' at level:', n_level                                                 !
end do!---------------------------------------------------------------------------------------------------------------    
!-----------------------------------------------------------------------
!-                   ---------------------------------------------------
!-define final groups---------------------------------------------------
!-                   ---------------------------------------------------
!-----------------------------------------------------------------------
n_group_element(:) = n_group_element_pr(:,1) + n_group_element_pr(:,2)
N_levels           = maxval(n_group_element(:))
if (allocated(n_element_lvl)) deallocate(n_element_lvl);allocate(n_element_lvl(N_levels))
do n_level         = 1, N_levels!----------------------------------------------------------------------------------
N_elements_lvl     = 0                                                                                             !
do n_element       = 1, N_elements!--------------------------------------------------------------------------      !
if (n_group_element(n_element) == n_level) N_elements_lvl = N_elements_lvl + 1                               !     !
end do!------------------------------------------------------------------------------------------------------      !
allocate(n_element_lvl(n_level)%n(N_elements_lvl))                                                                 !
ind                = 0                                                                                             !
do n_element       = 1, N_elements!-----------------------------------------------------------------------------   !
if (n_group_element(n_element) == n_level) then!-------------------------------------------------------------   !  !
ind                             = ind + 1                                                                    !  !  !
n_element_lvl(n_level)%n(ind)   = n_element                                                                  !  !  !
end if!------------------------------------------------------------------------------------------------------   !  !
end do!---------------------------------------------------------------------------------------------------------   !
end do!------------------------------------------------------------------------------------------------------------
!-----------------------------------------------------------------------
!-                              ----------------------------------------
!-connectivity check for each hp----------------------------------------
!-                              ----------------------------------------
!-----------------------------------------------------------------------
error_count    = 0
do n_level     = 1, N_levels!--------------------------------------------------------------------------------------
N_elements_lvl = size(n_element_lvl(n_level)%n)                                                                    !
do i = 1, N_elements_lvl; ind_glb = n_element_lvl(n_level)%n(i)!------------------------------------------------   !
do j = 1, N_elements_lvl; jnd_glb = n_element_lvl(n_level)%n(j)!---------------------------------------------   !  !
if (any(n_lmnt_lmnt(ind_glb)%n == jnd_glb)) then!---------------------------------------------------------   !  !  !
error_count    = error_count+1                                                                            !  !  !  !
write(*,*) "Connectivity detected at HP #", n_level, "!!!!!!!!"                                           !  !  !  !
end if!---------------------------------------------------------------------------------------------------   !  !  !
end do!------------------------------------------------------------------------------------------------------   !  !
end do!---------------------------------------------------------------------------------------------------------   !
end do!------------------------------------------------------------------------------------------------------------
!-----------------------------------------------------------------------
!-                                 -------------------------------------
!-deallocate large local structures-------------------------------------
!-                                 -------------------------------------
!-----------------------------------------------------------------------
deallocate(n_group_element_pr)
deallocate(n_lmnt_lmnt       )
if (error_count == 0) then
write(*,*) "Grid reordering complete with success !!!!!!"
else
write(*,*) "Grid reordering failed with residual connectivity !!!!!!"
end if
end subroutine reorder_unstrctrd

end module A3D_2D_reorder_unstrctrd
