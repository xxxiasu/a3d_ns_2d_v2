!--------------------------------------------------------------------------------------------------
!-                                                                                               --
!-                                                                                               --
!-                                                                                               --
!-                                                                                               --
!-Second development version, based on new data structures with phantom cells in A3D_NS_2D       --
!-(with OMP parallelization)                                                                     --
!-                                                                                               --
!-                                                                                               --
!-                                                                                               --
!-                                                                                               --
!-                                                                                      jan 2019 --
!--------------------------------------------------------------------------------------------------
module A3D_2D_sch_imp
!$ use OMP_LIB

use R3D_types
use A3D_data_structure
use R3D_2D_lsq_WENO
use R3D_LA_routines
use R3D_maths
use A3Dg_2D_routines
use A3D_vrbls_types
use A3D_thrm
use A3D_sch_NS
use imp_matrices

 contains
subroutine flu_sgs_unstrctrd(A3D_thrm_data, A3D_UGDS_cells, A3D_UGDS_dgs, A3D_UGR1_2D_cells, A3D_UGDS_2D_bndrs, isreallmnt,&
                             A3D_Rvs, A3D_vrbls, A3D_uold, A3D_unew, xnueqdg, Smaxdg, dtlcl, grp_lmnt, nlmnt_lvl, rhoBC, pBC, VxBC, VyBC, TBC) 
!-----------------------------------------------------------------------------------------------------------------------------------
!-         -------------------------------------------------------------------------------------------------------------------------
!-interface-------------------------------------------------------------------------------------------------------------------------
!-         -------------------------------------------------------------------------------------------------------------------------
!-----------------------------------------------------------------------------------------------------------------------------------
real                        ,                            intent(in   ) :: rhoBC
real                        ,                            intent(in   ) :: pBC
real                        ,                            intent(in   ) :: VxBC
real                        ,                            intent(in   ) :: VyBC
real                        ,                            intent(in   ) :: TBC
type(A3DthrmdataBSL        ),                            intent(in   ) :: A3D_thrm_data
type(A3DUGDS2Dplgnstplg    ),                            intent(in   ) :: A3D_UGDS_cells
type(A3DUGDS2Dplgnsdgsvrtcs),                            intent(in   ) :: A3D_UGDS_dgs
type(A3DUGR12Dcells        ),                            intent(in   ) :: A3D_UGR1_2D_cells
type(A3D2DNSbndrs          ),                            intent(in   ) :: A3D_UGDS_2D_bndrs  ! including boundary variables such as p_inf, etc.
logical                     , dimension(:), allocatable, intent(in   ) :: isreallmnt         ! phantom cells filter
type(A3DuNS                ), dimension(:), allocatable, intent(in   ) :: A3D_Rvs
type(A3DvNS                ), dimension(:), allocatable, intent(in   ) :: A3D_vrbls
real                        , dimension(:), allocatable, intent(in   ) :: xnueqdg            ! (Ndgs)
real                        , dimension(:), allocatable, intent(in   ) :: Smaxdg             ! (Ndgs)
real                        , dimension(:), allocatable, intent(in   ) :: dtlcl              ! (Nlmnts)
integer                     , dimension(:), allocatable, intent(in   ) :: grp_lmnt
type(R3Dnn0                ), dimension(:), allocatable, intent(in   ) :: nlmnt_lvl
type(A3DuNS                ), dimension(:), allocatable, intent(in   ) :: A3D_uold
type(A3DuNS                ), dimension(:), allocatable, intent(inout) :: A3D_unew
!-----------------------------------------------------------------------------------------------------------------------------------
!-               -------------------------------------------------------------------------------------------------------------------
!-local_variables-------------------------------------------------------------------------------------------------------------------
!-               -------------------------------------------------------------------------------------------------------------------
!-----------------------------------------------------------------------------------------------------------------------------------
logical                                                                :: file_exists
real                                                                   :: x_dist_elements
real                                                                   :: y_dist_elements
real                                                                   :: dist_elements
real                                                                   :: xmid_pnt
real                                                                   :: ymid_pnt
real                                                                   :: xbary
real                                                                   :: ybary
real                                                                   :: inwd
real                                                                   :: nxlcl
real                                                                   :: nylcl
real                                                                   :: rhodg
real                                                                   :: udg
real                                                                   :: vdg
real                                                                   :: pdg
real                        , dimension(:)    , allocatable            :: lambdadg
real                        , dimension(:)    , allocatable            :: nx
real                        , dimension(:)    , allocatable            :: ny
real                        , dimension(:)    , allocatable            :: dS
real                        , dimension(:,:,:), allocatable            :: D
real                        , dimension(:,:)  , allocatable            :: Dlmnt
real                        , dimension(:,:)  , allocatable            :: Dinv
real                        , dimension(4,4)                           :: Id
real                        , dimension(4,4)                           :: T
real                        , dimension(4,4)                           :: Tinv
real                        , dimension(4,4)                           :: Ji
real                        , dimension(4,4)                           :: Jj
real                        , dimension(4,4)                           :: Jij
real                        , dimension(:,:)  , allocatable            :: dus
real                        , dimension(:,:)  , allocatable            :: du_new
real                        , dimension(:)    , allocatable            :: df

Ndgs    = size(A3D_UGDS_dgs%dg_data)
Nlmnts  = size(A3D_UGDS_dgs%ndg_nlmnt)
Ndgs    = size(A3D_UGDS_dgs%dg_data)
Nbnddgs = size(A3D_UGDS_2D_bndrs%kBC_dg)
allocate(nx      (Ndgs              ))
allocate(ny      (Ndgs              ))
allocate(dS      (Ndgs              ))
allocate(lambdadg(Ndgs              ))
allocate(D       (4   ,4     ,Nlmnts))
allocate(Dlmnt   (4   ,4            ))
allocate(Dinv    (4   ,4            ))
allocate(dus     (4   ,Nlmnts       ))
allocate(du_new  (4   ,Nlmnts       ))
allocate(df      (4                 ))

!-----------------------------------------------------------------------------------------------------------------------------------
!-                                       -------------------------------------------------------------------------------------------
!-compute spectral radius Lambda on edges-------------------------------------------------------------------------------------------
!-                                       -------------------------------------------------------------------------------------------
!-----------------------------------------------------------------------------------------------------------------------------------
!$OMP PARALLEL DO SCHEDULE(STATIC) DEFAULT(PRIVATE) &
!$OMP SHARED(Ndgs, A3D_UGDS_dgs, A3D_UGR1_2D_cells, A3D_UGDS_cells, A3D_vrbls, isreallmnt, Smaxdg, xnueqdg, nx, ny, dS, lambdadg)
do ndg = 1, Ndgs, 1!---------------------------------------------------------------------------------------------------------------
nlmnt_1 = A3D_UGDS_dgs%dg_data(ndg)%n_element_1                                                                                    !
nlmnt_2 = A3D_UGDS_dgs%dg_data(ndg)%n_element_2                                                                                    !
if (isreallmnt(nlmnt_1) .or. isreallmnt(nlmnt_2)) then!-----------------------------------------------------------------------     !
nx(ndg) = A3D_UGR1_2D_cells%Gpnt_xyzw(ndg)%dSGp(1)%x                                                                          !    !
ny(ndg) = A3D_UGR1_2D_cells%Gpnt_xyzw(ndg)%dSGp(1)%y                                                                          !    !
dS(ndg) = sqrt(nx(ndg)**2. + ny(ndg)**2.)                                                                                     !    !
nx(ndg) = -nx(ndg)/dS(ndg)                                                                                                    !    !
ny(ndg) = -ny(ndg)/dS(ndg)                                                                                                    !    !
                                                                                                                              !    !
x_dist_elements   = A3D_UGDS_cells%x_lmnt(nlmnt_1)%x - A3D_UGDS_cells%x_lmnt(nlmnt_2)%x                                       !    !
y_dist_elements   = A3D_UGDS_cells%x_lmnt(nlmnt_1)%y - A3D_UGDS_cells%x_lmnt(nlmnt_2)%y                                       !    !
dist_elements     = abs(x_dist_elements*nx(ndg)     + y_dist_elements*ny(ndg))                                                !    !
rhodg             = 0.5   * (A3D_vrbls(nlmnt_1)%rho + A3D_vrbls(nlmnt_2)%rho)                                                 !    !
udg               = 0.5   * (A3D_vrbls(nlmnt_1)%V%x + A3D_vrbls(nlmnt_2)%V%x)                                                 !    !
vdg               = 0.5   * (A3D_vrbls(nlmnt_1)%V%y + A3D_vrbls(nlmnt_2)%V%y)                                                 !    !
pdg               = 0.5   * (A3D_vrbls(nlmnt_1)%p   + A3D_vrbls(nlmnt_2)%p)                                                   !    !
lambdadg(ndg)     = abs(udg*nx(ndg) + vdg*ny(ndg))  + Smaxdg(ndg) +(2.*xnueqdg(ndg))/dist_elements                            !    !
end if!-----------------------------------------------------------------------------------------------------------------------     !
end do!----------------------------------------------------------------------------------------------------------------------------
!$OMP END PARALLEL DO

gam   = A3D_thrm_data%gamma
RG    = A3D_thrm_data%Rg
Nlvls = size(nlmnt_lvl)
Id    = 0.
do i  = 1, 4, 1; Id(i,i) = 1.; end do
!-----------------------------------------------------------------------------------------------------------------------------------
!-                     -------------------------------------------------------------------------------------------------------------
!-D matrices assembling-------------------------------------------------------------------------------------------------------------
!-                     -------------------------------------------------------------------------------------------------------------
!-----------------------------------------------------------------------------------------------------------------------------------
!$OMP PARALLEL DO SCHEDULE(STATIC) DEFAULT(PRIVATE) &
!$OMP SHARED(Nlmnts, Nbnddgs, A3D_UGDS_dgs, A3D_UGDS_cells, A3D_UGDS_2D_bndrs, A3D_uold, A3D_vrbls, D, Id, dtlcl, &
!$OMP isreallmnt, nx, ny, dS, lambdadg, gam, RG, rhoBC, pBC, VxBC, VyBC, TBC)
do nlmnt     = 1, Nlmnts, 1; if (isreallmnt(nlmnt)) then!--------------------------------------------------------------------------------------------------
vol          = A3D_UGDS_cells%A_lmnt(nlmnt)                                                                                                                !
dt           = dtlcl(nlmnt)                                                                                                                                !
D(:,:,nlmnt) = Id                                                                                                                                          !
do ind       = 1, size(A3D_UGDS_dgs%ndg_nlmnt(nlmnt)%n)!--------------------------------------------------------------------------------------------       !
ndg          = A3D_UGDS_dgs%ndg_nlmnt(nlmnt)%n(ind)                                                                                                 !      !
D(:,:,nlmnt) = D(:,:,nlmnt) + 0.5*dt/vol*lambdadg(ndg)*dS(ndg)*Id                                                                                   !      !
xmid_pnt     = 0.5*(A3D_UGDS_cells%x_vrtx(A3D_UGDS_dgs%dg_data(ndg)%n_point_1)%x + A3D_UGDS_cells%x_vrtx(A3D_UGDS_dgs%dg_data(ndg)%n_point_2)%x)    !      !
ymid_pnt     = 0.5*(A3D_UGDS_cells%x_vrtx(A3D_UGDS_dgs%dg_data(ndg)%n_point_1)%y + A3D_UGDS_cells%x_vrtx(A3D_UGDS_dgs%dg_data(ndg)%n_point_2)%y)    !      !
xbary        = A3D_UGDS_cells%x_lmnt(nlmnt)%x                                                                                                       !      !
ybary        = A3D_UGDS_cells%x_lmnt(nlmnt)%y                                                                                                       !      !
inwd         = (xbary-xmid_pnt)*nx(ndg) + (ybary-ymid_pnt)*ny(ndg)                                                                                  !      !
if (inwd > 0) then!-----------------------------------------------------------------------------------------                                        !      !
nxlcl        = -nx(ndg)                                                                                     !                                       !      !
nylcl        = -ny(ndg)                                                                                     !                                       !      !
else                                                                                                        !                                       !      !
nxlcl        = nx(ndg)                                                                                      !                                       !      !
nylcl        = ny(ndg)                                                                                      !                                       !      !
end if!-----------------------------------------------------------------------------------------------------                                        !      !
!-compute D matrices for boundary edges                                                                                                             !      !
if (any(A3D_UGDS_2D_bndrs%ndg_nbnddg == ndg)) then!-----------------------------------------------------------------------------------------        !      !
do i = 1, Nbnddgs; if (A3D_UGDS_2D_bndrs%ndg_nbnddg(i) == ndg) nbnddg = i; end do                                                           !       !      !
kBC  = A3D_UGDS_2D_bndrs%kBC_dg(nbnddg)                                                                                                     !       !      !
Jij  = 0.; Ji = 0.; Jj = 0.; Tinv = 0.; T = 0.                                                                                              !       !      !
nlmnt_nb = A3D_UGDS_dgs%dg_data(ndg)%n_element_1 + A3D_UGDS_dgs%dg_data(ndg)%n_element_2 - nlmnt                                            !       !      !
call Tinv_conversion_l (gam,A3D_uold(nlmnt), Tinv)                                                                                          !       !      !
call nrml_flux_jac_l(gam,nxlcl,nylcl,A3D_uold(nlmnt), Ji)                                                                                   !       !      !
if      (kBC == 0)  then         ! adiabatic wall-----------------------------------------------------------                                !       !      !
Jij(1,1) = 1.; Jij(2,2) = -1.; Jij(3,3) = -1.; Jij(4,4) = 1.                                                !                               !       !      !
else if (kBC == 40) then         ! subsonic Riemann BCs (based on inward-pointing normals)------------------!                               !       !      !
!-enforce inward-pointing normals for boundary edges                                                        !                               !       !      !
call riemann_jij(gam, RG, -nxlcl, -nylcl, rhoBC, pBC, VxBC, VyBC, TBC, A3D_vrbls(nlmnt), Jij)               !                               !       !      !
end if!-----------------------------------------------------------------------------------------------------                                !       !      !
call T_conversion_l (gam,A3D_uold(nlmnt_nb), T)                                                                                             !       !      !
call nrml_flux_jac_l(gam,nxlcl,nylcl,A3D_uold(nlmnt_nb), Jj)                                                                                !       !      !
D(:,:,nlmnt) = D(:,:,nlmnt) + 0.5*dt/vol*dS(ndg)*(matmul(2.*(Jj-lambdadg(ndg)*Id), matmul(T, matmul(Jij, Tinv))) + (Ji+lambdadg(ndg)*Id))   !       !      !
end if!-------------------------------------------------------------------------------------------------------------------------------------        !      !
end do!---------------------------------------------------------------------------------------------------------------------------------------------       !
end if; end do!--------------------------------------------------------------------------------------------------------------------------------------------
!$OMP END PARALLEL DO

!$OMP PARALLEL WORKSHARE
du_new(:,:)  = 0.
dus   (:,:)  = 0.                                                                                                                                                        
!$OMP END PARALLEL WORKSHARE
!-----------------------------------------------------------------------------------------------------------------------------------                                    
!-                     -------------------------------------------------------------------------------------------------------------                                    
!-FLU-SGS forward sweep-------------------------------------------------------------------------------------------------------------                                    
!-                     -------------------------------------------------------------------------------------------------------------                                    
!-----------------------------------------------------------------------------------------------------------------------------------                                    
do nlvl     = 1, Nlvls, 1!---------------------------------------------------------------------------------------------------------------------------------------       
Nlmnts_lvl  = size(nlmnt_lvl(nlvl)%n)                                                                                                                            !      
!$OMP PARALLEL DO SCHEDULE(STATIC) DEFAULT(PRIVATE) &                                                                                                            !      
!$OMP SHARED(Nlmnts_lvl, A3D_UGDS_dgs, A3D_UGDS_cells, A3D_UGDS_2D_bndrs, A3D_uold, A3D_vrbls, A3D_Rvs, du_new, dus, D, Id, dtlcl, &                             !      
!$OMP isreallmnt, nx, ny, dS, lambdadg, gam, RG, nlvl, nlmnt_lvl, grp_lmnt)                                                                                      !      
do ind_lmnt = 1, Nlmnts_lvl, 1; nlmnt = nlmnt_lvl(nlvl)%n(ind_lmnt); if (isreallmnt(nlmnt)) then!----------------------------------------------------------      !      
df          = 0.                                                                                                                                           !     !      
dt          = dtlcl(nlmnt)                                                                                                                                 !     !      
vol         = A3D_UGDS_cells%A_lmnt(nlmnt)                                                                                                                 !     !      
do ind      = 1, size(A3D_UGDS_dgs%ndg_nlmnt(nlmnt)%n), 1!-----------------------------------------------------------------------------------------        !     !      
ndg         = A3D_UGDS_dgs%ndg_nlmnt(nlmnt)%n(ind)                                                                                                 !       !     !      
xmid_pnt    = 0.5*(A3D_UGDS_cells%x_vrtx(A3D_UGDS_dgs%dg_data(ndg)%n_point_1)%x + A3D_UGDS_cells%x_vrtx(A3D_UGDS_dgs%dg_data(ndg)%n_point_2)%x)    !       !     !      
ymid_pnt    = 0.5*(A3D_UGDS_cells%x_vrtx(A3D_UGDS_dgs%dg_data(ndg)%n_point_1)%y + A3D_UGDS_cells%x_vrtx(A3D_UGDS_dgs%dg_data(ndg)%n_point_2)%y)    !       !     !      
xbary       = A3D_UGDS_cells%x_lmnt(nlmnt)%x                                                                                                       !       !     !      
ybary       = A3D_UGDS_cells%x_lmnt(nlmnt)%y                                                                                                       !       !     !      
inwd        = (xbary-xmid_pnt)*nx(ndg) + (ybary-ymid_pnt)*ny(ndg)                                                                                  !       !     !      
if (inwd > 0) then!--------------------------------------------------------------------------------------                                          !       !     !      
nxlcl       = -nx(ndg)                                                                                   !                                         !       !     !      
nylcl       = -ny(ndg)                                                                                   !                                         !       !     !      
else                                                                                                     !                                         !       !     !      
nxlcl       = nx(ndg)                                                                                    !                                         !       !     !      
nylcl       = ny(ndg)                                                                                    !                                         !       !     !      
end if!--------------------------------------------------------------------------------------------------                                          !       !     !      
if (.not.(any(A3D_UGDS_2D_bndrs%ndg_nbnddg == ndg))) then!-----------------------------------------------                                          !       !     !      
nlmnt_nb     = A3D_UGDS_dgs%dg_data(ndg)%n_element_1 + A3D_UGDS_dgs%dg_data(ndg)%n_element_2 - nlmnt     !                                         !       !     !      
call nrml_flux_jac_l(gam,nxlcl,nylcl,A3D_uold(nlmnt_nb), Jj)                                             !                                         !       !     !      
if      (grp_lmnt(nlmnt_nb)<grp_lmnt(nlmnt)) then!---------------------------------------------------    !                                         !       !     !      
df           = df + 0.5*dt*dS(ndg)/vol*matmul(Jj-lambdadg(ndg)*Id, dus(:,nlmnt_nb))                  !   !                                         !       !     !      
end if!----------------------------------------------------------------------------------------------    !                                         !       !     !      
end if!--------------------------------------------------------------------------------------------------                                          !       !     !      
end do!--------------------------------------------------------------------------------------------------------------------------------------------        !     !      
Dlmnt(:,:)   = D(:,:,nlmnt)                                                                                                                                !     !      
if (any(A3D_UGDS_2D_bndrs%nlmnt_nbndlmnt == nlmnt)) then                                                                                                   !     !      
call R3D_invert_square_matrix (Dlmnt, Dinv)                                                                                                                !     !      
else                                                                                                                                                       !     !      
Dinv = Dlmnt                                                                                                                                               !     !      
do i = 1, 4; Dinv(i,i) = 1./Dlmnt(i,i); end do                                                                                                             !     !      
end if                                                                                                                                                     !     !      
df (1)       = -df(1) - A3D_Rvs(nlmnt)%rho*dt                                                                                                              !     !      
df (2)       = -df(2) - A3D_Rvs(nlmnt)%rhoV%x*dt                                                                                                           !     !      
df (3)       = -df(3) - A3D_Rvs(nlmnt)%rhoV%y*dt                                                                                                           !     !      
df (4)       = -df(4) - A3D_Rvs(nlmnt)%rhoet*dt                                                                                                            !     !      
dus(:,nlmnt) = matmul(Dinv, df)                                                                                                                            !     !      
end if; end do!--------------------------------------------------------------------------------------------------------------------------------------------      !      
!$OMP END PARALLEL DO                                                                                                                                            !      
end do!----------------------------------------------------------------------------------------------------------------------------------------------------------       
!-----------------------------------------------------------------------------------------------------------------------------------                                    
!-                      ------------------------------------------------------------------------------------------------------------                                    
!-FLU-SGS backward sweep------------------------------------------------------------------------------------------------------------                                    
!-                      ------------------------------------------------------------------------------------------------------------                                    
!-----------------------------------------------------------------------------------------------------------------------------------                                    
do nlvl     = Nlvls, 1, -1!--------------------------------------------------------------------------------------------------------------------------------------       
Nlmnts_lvl  = size(nlmnt_lvl(nlvl)%n)                                                                                                                            !      
!$OMP PARALLEL DO SCHEDULE(STATIC) DEFAULT(PRIVATE) &                                                                                                            !      
!$OMP SHARED(Nlmnts_lvl, A3D_UGDS_dgs, A3D_UGDS_cells, A3D_UGDS_2D_bndrs, A3D_uold, A3D_vrbls, A3D_Rvs, du_new, dus, D, Id, dtlcl, &                             !      
!$OMP isreallmnt, nx, ny, dS, lambdadg, gam, RG, nlvl, nlmnt_lvl, grp_lmnt)                                                                                      !      
do ind_lmnt = Nlmnts_lvl, 1, -1; nlmnt = nlmnt_lvl(nlvl)%n(ind_lmnt); if (isreallmnt(nlmnt)) then!---------------------------------------------------------      !      
df          = 0.                                                                                                                                           !     !      
dt          = dtlcl(nlmnt)                                                                                                                                 !     !      
vol         = A3D_UGDS_cells%A_lmnt(nlmnt)                                                                                                                 !     !      
do ind      = size(A3D_UGDS_dgs%ndg_nlmnt(nlmnt)%n), 1, -1!----------------------------------------------------------------------------------------        !     !      
ndg         = A3D_UGDS_dgs%ndg_nlmnt(nlmnt)%n(ind)                                                                                                 !       !     !      
xmid_pnt    = 0.5*(A3D_UGDS_cells%x_vrtx(A3D_UGDS_dgs%dg_data(ndg)%n_point_1)%x + A3D_UGDS_cells%x_vrtx(A3D_UGDS_dgs%dg_data(ndg)%n_point_2)%x)    !       !     !      
ymid_pnt    = 0.5*(A3D_UGDS_cells%x_vrtx(A3D_UGDS_dgs%dg_data(ndg)%n_point_1)%y + A3D_UGDS_cells%x_vrtx(A3D_UGDS_dgs%dg_data(ndg)%n_point_2)%y)    !       !     !      
xbary       = A3D_UGDS_cells%x_lmnt(nlmnt)%x                                                                                                       !       !     !      
ybary       = A3D_UGDS_cells%x_lmnt(nlmnt)%y                                                                                                       !       !     !      
inwd        = (xbary-xmid_pnt)*nx(ndg) + (ybary-ymid_pnt)*ny(ndg)                                                                                  !       !     !      
if (inwd > 0) then!--------------------------------------------------------------------------------------                                          !       !     !      
nxlcl       = -nx(ndg)                                                                                   !                                         !       !     !      
nylcl       = -ny(ndg)                                                                                   !                                         !       !     !      
else                                                                                                     !                                         !       !     !      
nxlcl       = nx(ndg)                                                                                    !                                         !       !     !      
nylcl       = ny(ndg)                                                                                    !                                         !       !     !      
end if!--------------------------------------------------------------------------------------------------                                          !       !     !      
if (.not.(any(A3D_UGDS_2D_bndrs%ndg_nbnddg == ndg))) then!-----------------------------------------------                                          !       !     !      
nlmnt_nb        = A3D_UGDS_dgs%dg_data(ndg)%n_element_1 + A3D_UGDS_dgs%dg_data(ndg)%n_element_2 - nlmnt  !                                         !       !     !      
call nrml_flux_jac_l(gam,nxlcl,nylcl,A3D_uold(nlmnt_nb), Jj)                                             !                                         !       !     !      
if (grp_lmnt(nlmnt_nb)>grp_lmnt(nlmnt)) then!--------------------------------------------------------    !                                         !       !     !      
df              = df + 0.5*dt*dS(ndg)/vol*matmul(Jj-lambdadg(ndg)*Id, du_new(:,nlmnt_nb))            !   !                                         !       !     !      
end if!----------------------------------------------------------------------------------------------    !                                         !       !     !      
end if!--------------------------------------------------------------------------------------------------                                          !       !     !      
end do!--------------------------------------------------------------------------------------------------------------------------------------------        !     !      
Dlmnt(:,:)      = D(:,:,nlmnt)                                                                                                                             !     !      
if (any(A3D_UGDS_2D_bndrs%nlmnt_nbndlmnt == nlmnt)) then!--------------------------------------------                                                      !     !      
call R3D_invert_square_matrix (Dlmnt, Dinv)                                                          !                                                     !     !      
else                                                                                                 !                                                     !     !      
Dinv = Dlmnt                                                                                         !                                                     !     !      
do i = 1, 4; Dinv(i,i) = 1./Dlmnt(i,i); end do                                                       !                                                     !     !      
end if!----------------------------------------------------------------------------------------------                                                      !     !      
du_new(:,nlmnt) = dus(:,nlmnt) - matmul(Dinv, df)                                                                                                          !     !      
end if; end do!--------------------------------------------------------------------------------------------------------------------------------------------      !      
!$OMP END PARALLEL DO                                                                                                                                            !      
end do!----------------------------------------------------------------------------------------------------------------------------------------------------------       
!$OMP PARALLEL DO SCHEDULE(STATIC) DEFAULT(PRIVATE) SHARED(Nlmnts, A3D_unew, A3D_uold, du_new, isreallmnt)
do nlmnt = 1, Nlmnts, 1; if (isreallmnt(nlmnt)) then!----------------------------------------------------
A3D_unew(nlmnt)%rho    = A3D_uold(nlmnt)%rho    + du_new(1,nlmnt)                                        !
A3D_unew(nlmnt)%rhoV%x = A3D_uold(nlmnt)%rhoV%x + du_new(2,nlmnt)                                        !
A3D_unew(nlmnt)%rhoV%y = A3D_uold(nlmnt)%rhoV%y + du_new(3,nlmnt)                                        !
A3D_unew(nlmnt)%rhoet  = A3D_uold(nlmnt)%rhoet  + du_new(4,nlmnt)                                        !
end if; end do!------------------------------------------------------------------------------------------
!$OMP END PARALLEL DO
end subroutine flu_sgs_unstrctrd

subroutine blu_sgs_unstrctrd(K_it, omg, A3D_thrm_data, A3D_UGDS_cells, A3D_UGDS_dgs, A3D_UGR1_2D_cells, A3D_UGDS_2D_bndrs, isreallmnt,&
                             A3D_Rvs, A3D_vrbls, A3D_uold, A3D_unew, xnueqdg, Smaxdg, dtlcl, grp_lmnt, nlmnt_lvl, rhoBC, pBC, VxBC, VyBC, TBC) 
!-----------------------------------------------------------------------------------------------------------------------------------
!-         -------------------------------------------------------------------------------------------------------------------------
!-interface-------------------------------------------------------------------------------------------------------------------------
!-         -------------------------------------------------------------------------------------------------------------------------
!-----------------------------------------------------------------------------------------------------------------------------------
integer                     ,                            intent(in   ) :: K_it
real                        ,                            intent(in   ) :: omg
real                        ,                            intent(in   ) :: rhoBC
real                        ,                            intent(in   ) :: pBC
real                        ,                            intent(in   ) :: VxBC
real                        ,                            intent(in   ) :: VyBC
real                        ,                            intent(in   ) :: TBC
type(A3DthrmdataBSL        ),                            intent(in   ) :: A3D_thrm_data
type(A3DUGDS2Dplgnstplg    ),                            intent(in   ) :: A3D_UGDS_cells
type(A3DUGDS2Dplgnsdgsvrtcs),                            intent(in   ) :: A3D_UGDS_dgs
type(A3DUGR12Dcells        ),                            intent(in   ) :: A3D_UGR1_2D_cells
type(A3D2DNSbndrs          ),                            intent(in   ) :: A3D_UGDS_2D_bndrs  ! including boundary variables such as p_inf, etc.
logical                     , dimension(:), allocatable, intent(in   ) :: isreallmnt         ! phantom cells filter
type(A3DuNS                ), dimension(:), allocatable, intent(in   ) :: A3D_Rvs
type(A3DvNS                ), dimension(:), allocatable, intent(in   ) :: A3D_vrbls
real                        , dimension(:), allocatable, intent(in   ) :: xnueqdg            ! (Ndgs)
real                        , dimension(:), allocatable, intent(in   ) :: Smaxdg             ! (Ndgs)
real                        , dimension(:), allocatable, intent(in   ) :: dtlcl              ! (Nlmnts)
integer                     , dimension(:), allocatable, intent(in   ) :: grp_lmnt
type(R3Dnn0                ), dimension(:), allocatable, intent(in   ) :: nlmnt_lvl
type(A3DuNS                ), dimension(:), allocatable, intent(in   ) :: A3D_uold
type(A3DuNS                ), dimension(:), allocatable, intent(inout) :: A3D_unew
!-----------------------------------------------------------------------------------------------------------------------------------
!-               -------------------------------------------------------------------------------------------------------------------
!-local_variables-------------------------------------------------------------------------------------------------------------------
!-               -------------------------------------------------------------------------------------------------------------------
!-----------------------------------------------------------------------------------------------------------------------------------
logical                                                                :: file_exists
real                                                                   :: x_dist_elements
real                                                                   :: y_dist_elements
real                                                                   :: dist_elements
real                                                                   :: xmid_pnt
real                                                                   :: ymid_pnt
real                                                                   :: xbary
real                                                                   :: ybary
real                                                                   :: inwd
real                                                                   :: nxlcl
real                                                                   :: nylcl
real                                                                   :: rhodg
real                                                                   :: udg
real                                                                   :: vdg
real                                                                   :: pdg
real                        , dimension(:)    , allocatable            :: lambdadg
real                        , dimension(:)    , allocatable            :: nx
real                        , dimension(:)    , allocatable            :: ny
real                        , dimension(:)    , allocatable            :: dS
real                        , dimension(:,:,:), allocatable            :: D
real                        , dimension(:,:)  , allocatable            :: Dlmnt
real                        , dimension(:,:)  , allocatable            :: Dinv
real                        , dimension(4,4)                           :: Id
real                        , dimension(4,4)                           :: T
real                        , dimension(4,4)                           :: Tinv
real                        , dimension(4,4)                           :: Ji
real                        , dimension(4,4)                           :: Jj
real                        , dimension(4,4)                           :: Jij
real                        , dimension(:,:)  , allocatable            :: dus
real                        , dimension(:,:)  , allocatable            :: du_old
real                        , dimension(:,:)  , allocatable            :: du_new
real                        , dimension(:)    , allocatable            :: df
real                                                                   :: error_l

Ndgs    = size(A3D_UGDS_dgs%dg_data)
Nlmnts  = size(A3D_UGDS_dgs%ndg_nlmnt)
Ndgs    = size(A3D_UGDS_dgs%dg_data)
Nbnddgs = size(A3D_UGDS_2D_bndrs%kBC_dg)
allocate(nx      (Ndgs              ))
allocate(ny      (Ndgs              ))
allocate(dS      (Ndgs              ))
allocate(lambdadg(Ndgs              ))
allocate(D       (4   ,4     ,Nlmnts))
allocate(Dlmnt   (4   ,4            ))
allocate(Dinv    (4   ,4            ))
allocate(dus     (4   ,Nlmnts       ))
allocate(du_old  (4   ,Nlmnts       ))
allocate(du_new  (4   ,Nlmnts       ))
allocate(df      (4                 ))

!-----------------------------------------------------------------------------------------------------------------------------------
!-                                       -------------------------------------------------------------------------------------------
!-compute spectral radius Lambda on edges-------------------------------------------------------------------------------------------
!-                                       -------------------------------------------------------------------------------------------
!-----------------------------------------------------------------------------------------------------------------------------------
!$OMP PARALLEL DO SCHEDULE(STATIC) DEFAULT(PRIVATE) &
!$OMP SHARED(Ndgs, A3D_UGDS_dgs, A3D_UGR1_2D_cells, A3D_UGDS_cells, A3D_vrbls, isreallmnt, Smaxdg, xnueqdg, nx, ny, dS, lambdadg)
do ndg = 1, Ndgs, 1!---------------------------------------------------------------------------------------------------------------
nlmnt_1 = A3D_UGDS_dgs%dg_data(ndg)%n_element_1                                                                                    !
nlmnt_2 = A3D_UGDS_dgs%dg_data(ndg)%n_element_2                                                                                    !
if (isreallmnt(nlmnt_1) .or. isreallmnt(nlmnt_2)) then!-----------------------------------------------------------------------     !
nx(ndg) = A3D_UGR1_2D_cells%Gpnt_xyzw(ndg)%dSGp(1)%x                                                                          !    !
ny(ndg) = A3D_UGR1_2D_cells%Gpnt_xyzw(ndg)%dSGp(1)%y                                                                          !    !
dS(ndg) = sqrt(nx(ndg)**2. + ny(ndg)**2.)                                                                                     !    !
nx(ndg) = -nx(ndg)/dS(ndg)                                                                                                    !    !
ny(ndg) = -ny(ndg)/dS(ndg)                                                                                                    !    !
                                                                                                                              !    !
x_dist_elements   = A3D_UGDS_cells%x_lmnt(nlmnt_1)%x - A3D_UGDS_cells%x_lmnt(nlmnt_2)%x                                       !    !
y_dist_elements   = A3D_UGDS_cells%x_lmnt(nlmnt_1)%y - A3D_UGDS_cells%x_lmnt(nlmnt_2)%y                                       !    !
dist_elements     = abs(x_dist_elements*nx(ndg)     + y_dist_elements*ny(ndg))                                                !    !
rhodg             = 0.5   * (A3D_vrbls(nlmnt_1)%rho + A3D_vrbls(nlmnt_2)%rho)                                                 !    !
udg               = 0.5   * (A3D_vrbls(nlmnt_1)%V%x + A3D_vrbls(nlmnt_2)%V%x)                                                 !    !
vdg               = 0.5   * (A3D_vrbls(nlmnt_1)%V%y + A3D_vrbls(nlmnt_2)%V%y)                                                 !    !
pdg               = 0.5   * (A3D_vrbls(nlmnt_1)%p   + A3D_vrbls(nlmnt_2)%p)                                                   !    !
lambdadg(ndg)     = abs(udg*nx(ndg) + vdg*ny(ndg))  + Smaxdg(ndg) +(2.*xnueqdg(ndg))/dist_elements                            !    !
end if!-----------------------------------------------------------------------------------------------------------------------     !
end do!----------------------------------------------------------------------------------------------------------------------------
!$OMP END PARALLEL DO

gam   = A3D_thrm_data%gamma
RG    = A3D_thrm_data%Rg
Nlvls = size(nlmnt_lvl)
Id    = 0.
do i  = 1, 4, 1; Id(i,i) = 1.; end do
!-----------------------------------------------------------------------------------------------------------------------------------
!-                     -------------------------------------------------------------------------------------------------------------
!-D matrices assembling-------------------------------------------------------------------------------------------------------------
!-                     -------------------------------------------------------------------------------------------------------------
!-----------------------------------------------------------------------------------------------------------------------------------
!$OMP PARALLEL DO SCHEDULE(STATIC) DEFAULT(PRIVATE) &
!$OMP SHARED(Nlmnts, Nbnddgs, A3D_UGDS_dgs, A3D_UGDS_cells, A3D_UGDS_2D_bndrs, A3D_uold, A3D_vrbls, D, Id, dtlcl, &
!$OMP isreallmnt, nx, ny, dS, lambdadg, gam, RG, rhoBC, pBC, VxBC, VyBC, TBC)
do nlmnt     = 1, Nlmnts, 1; if (isreallmnt(nlmnt)) then!--------------------------------------------------------------------------------------------------
vol          = A3D_UGDS_cells%A_lmnt(nlmnt)                                                                                                                !
dt           = dtlcl(nlmnt)                                                                                                                                !
D(:,:,nlmnt) = Id                                                                                                                                          !
do ind       = 1, size(A3D_UGDS_dgs%ndg_nlmnt(nlmnt)%n)!--------------------------------------------------------------------------------------------       !
ndg          = A3D_UGDS_dgs%ndg_nlmnt(nlmnt)%n(ind)                                                                                                 !      !
D(:,:,nlmnt) = D(:,:,nlmnt) + 0.5*dt/vol*lambdadg(ndg)*dS(ndg)*Id                                                                                   !      !
xmid_pnt     = 0.5*(A3D_UGDS_cells%x_vrtx(A3D_UGDS_dgs%dg_data(ndg)%n_point_1)%x + A3D_UGDS_cells%x_vrtx(A3D_UGDS_dgs%dg_data(ndg)%n_point_2)%x)    !      !
ymid_pnt     = 0.5*(A3D_UGDS_cells%x_vrtx(A3D_UGDS_dgs%dg_data(ndg)%n_point_1)%y + A3D_UGDS_cells%x_vrtx(A3D_UGDS_dgs%dg_data(ndg)%n_point_2)%y)    !      !
xbary        = A3D_UGDS_cells%x_lmnt(nlmnt)%x                                                                                                       !      !
ybary        = A3D_UGDS_cells%x_lmnt(nlmnt)%y                                                                                                       !      !
inwd         = (xbary-xmid_pnt)*nx(ndg) + (ybary-ymid_pnt)*ny(ndg)                                                                                  !      !
if (inwd > 0) then!-----------------------------------------------------------------------------------------                                        !      !
nxlcl        = -nx(ndg)                                                                                     !                                       !      !
nylcl        = -ny(ndg)                                                                                     !                                       !      !
else                                                                                                        !                                       !      !
nxlcl        = nx(ndg)                                                                                      !                                       !      !
nylcl        = ny(ndg)                                                                                      !                                       !      !
end if!-----------------------------------------------------------------------------------------------------                                        !      !
!-compute D matrices for boundary edges                                                                                                             !      !
if (any(A3D_UGDS_2D_bndrs%ndg_nbnddg == ndg)) then!-----------------------------------------------------------------------------------------        !      !
do i = 1, Nbnddgs; if (A3D_UGDS_2D_bndrs%ndg_nbnddg(i) == ndg) nbnddg = i; end do                                                           !       !      !
kBC  = A3D_UGDS_2D_bndrs%kBC_dg(nbnddg)                                                                                                     !       !      !
Jij  = 0.; Ji = 0.; Jj = 0.; Tinv = 0.; T = 0.                                                                                              !       !      !
nlmnt_nb = A3D_UGDS_dgs%dg_data(ndg)%n_element_1 + A3D_UGDS_dgs%dg_data(ndg)%n_element_2 - nlmnt                                            !       !      !
call Tinv_conversion_l (gam,A3D_uold(nlmnt), Tinv)                                                                                          !       !      !
call nrml_flux_jac_l(gam,nxlcl,nylcl,A3D_uold(nlmnt), Ji)                                                                                   !       !      !
if      (kBC == 0)  then         ! adiabatic wall-----------------------------------------------------------                                !       !      !
Jij(1,1) = 1.; Jij(2,2) = -1.; Jij(3,3) = -1.; Jij(4,4) = 1.                                                !                               !       !      !
else if (kBC == 40) then         ! subsonic Riemann BCs (based on inward-pointing normals)------------------!                               !       !      !
!-enforce inward-pointing normals for boundary edges                                                        !                               !       !      !
call riemann_jij(gam, RG, -nxlcl, -nylcl, rhoBC, pBC, VxBC, VyBC, TBC, A3D_vrbls(nlmnt), Jij)               !                               !       !      !
end if!-----------------------------------------------------------------------------------------------------                                !       !      !
call T_conversion_l (gam,A3D_uold(nlmnt_nb), T)                                                                                             !       !      !
call nrml_flux_jac_l(gam,nxlcl,nylcl,A3D_uold(nlmnt_nb), Jj)                                                                                !       !      !
D(:,:,nlmnt) = D(:,:,nlmnt) + 0.5*dt/vol*dS(ndg)*(matmul(2.*(Jj-lambdadg(ndg)*Id), matmul(T, matmul(Jij, Tinv))) + (Ji+lambdadg(ndg)*Id))   !       !      !
end if!-------------------------------------------------------------------------------------------------------------------------------------        !      !
end do!---------------------------------------------------------------------------------------------------------------------------------------------       !
end if; end do!--------------------------------------------------------------------------------------------------------------------------------------------
!$OMP END PARALLEL DO

!$OMP PARALLEL WORKSHARE
du_old(:,:)  = 0.
du_new(:,:)  = 0.
!$OMP END PARALLEL WORKSHARE
!-----------------------------------------------------------------------------------------------------------------------------------
!-                    --------------------------------------------------------------------------------------------------------------
!-BLU-SGS inner k-loop--------------------------------------------------------------------------------------------------------------
!-                    --------------------------------------------------------------------------------------------------------------
!-----------------------------------------------------------------------------------------------------------------------------------
do k = 1, K_it, 1!=====================================================================================================================================================
!-----------------------------------------------------------------------------------------------------------------------------------                                   !
!-                     -------------------------------------------------------------------------------------------------------------                                   !
!-BLU-SGS forward sweep-------------------------------------------------------------------------------------------------------------                                   !
!-                     -------------------------------------------------------------------------------------------------------------                                   !
!-----------------------------------------------------------------------------------------------------------------------------------                                   !
!$OMP PARALLEL WORKSHARE                                                                                                                                               !
dus(:,:)    = 0.                                                                                                                                                       !
!$OMP END PARALLEL WORKSHARE                                                                                                                                           !
do nlvl     = 1, Nlvls, 1!---------------------------------------------------------------------------------------------------------------------------------------      !
Nlmnts_lvl  = size(nlmnt_lvl(nlvl)%n)                                                                                                                            !     !
!$OMP PARALLEL DO SCHEDULE(STATIC) DEFAULT(PRIVATE) &                                                                                                            !     !
!$OMP SHARED(Nlmnts_lvl, A3D_UGDS_dgs, A3D_UGDS_cells, A3D_UGDS_2D_bndrs, A3D_uold, A3D_vrbls, A3D_Rvs, du_old, du_new, dus, D, Id, dtlcl, &                     !     !
!$OMP isreallmnt, nx, ny, dS, lambdadg, gam, RG, nlvl, nlmnt_lvl, grp_lmnt, omg )                                                                                !     !
do ind_lmnt = 1, Nlmnts_lvl, 1; nlmnt = nlmnt_lvl(nlvl)%n(ind_lmnt); if (isreallmnt(nlmnt)) then!----------------------------------------------------------      !     !
df          = 0.                                                                                                                                           !     !     !
dt          = dtlcl(nlmnt)                                                                                                                                 !     !     !
vol         = A3D_UGDS_cells%A_lmnt(nlmnt)                                                                                                                 !     !     !
do ind      = 1, size(A3D_UGDS_dgs%ndg_nlmnt(nlmnt)%n), 1!-----------------------------------------------------------------------------------------        !     !     !
ndg         = A3D_UGDS_dgs%ndg_nlmnt(nlmnt)%n(ind)                                                                                                 !       !     !     !
xmid_pnt    = 0.5*(A3D_UGDS_cells%x_vrtx(A3D_UGDS_dgs%dg_data(ndg)%n_point_1)%x + A3D_UGDS_cells%x_vrtx(A3D_UGDS_dgs%dg_data(ndg)%n_point_2)%x)    !       !     !     !
ymid_pnt    = 0.5*(A3D_UGDS_cells%x_vrtx(A3D_UGDS_dgs%dg_data(ndg)%n_point_1)%y + A3D_UGDS_cells%x_vrtx(A3D_UGDS_dgs%dg_data(ndg)%n_point_2)%y)    !       !     !     !
xbary       = A3D_UGDS_cells%x_lmnt(nlmnt)%x                                                                                                       !       !     !     !
ybary       = A3D_UGDS_cells%x_lmnt(nlmnt)%y                                                                                                       !       !     !     !
inwd        = (xbary-xmid_pnt)*nx(ndg) + (ybary-ymid_pnt)*ny(ndg)                                                                                  !       !     !     !
if (inwd > 0) then!--------------------------------------------------------------------------------------                                          !       !     !     !
nxlcl       = -nx(ndg)                                                                                   !                                         !       !     !     !
nylcl       = -ny(ndg)                                                                                   !                                         !       !     !     !
else                                                                                                     !                                         !       !     !     !
nxlcl       = nx(ndg)                                                                                    !                                         !       !     !     !
nylcl       = ny(ndg)                                                                                    !                                         !       !     !     !
end if!--------------------------------------------------------------------------------------------------                                          !       !     !     !
if (.not.(any(A3D_UGDS_2D_bndrs%ndg_nbnddg == ndg))) then!-----------------------------------------------                                          !       !     !     !
nlmnt_nb     = A3D_UGDS_dgs%dg_data(ndg)%n_element_1 + A3D_UGDS_dgs%dg_data(ndg)%n_element_2 - nlmnt     !                                         !       !     !     !
call nrml_flux_jac_l(gam,nxlcl,nylcl,A3D_uold(nlmnt_nb), Jj)                                             !                                         !       !     !     !
if      (grp_lmnt(nlmnt_nb)<grp_lmnt(nlmnt)) then!---------------------------------------------------    !                                         !       !     !     !
df           = df + omg*0.5*dt*dS(ndg)/vol*matmul(Jj-lambdadg(ndg)*Id, dus(:,nlmnt_nb))              !   !                                         !       !     !     !
else if (grp_lmnt(nlmnt_nb)>grp_lmnt(nlmnt)) then                                                    !   !                                         !       !     !     !
df           = df + omg*0.5*dt*dS(ndg)/vol*matmul(Jj-lambdadg(ndg)*Id, du_old(:,nlmnt_nb))           !   !                                         !       !     !     !
end if!----------------------------------------------------------------------------------------------    !                                         !       !     !     !
end if!--------------------------------------------------------------------------------------------------                                          !       !     !     !
end do!--------------------------------------------------------------------------------------------------------------------------------------------        !     !     !
Dlmnt(:,:)   = D(:,:,nlmnt)                                                                                                                                !     !     !
if (any(A3D_UGDS_2D_bndrs%nlmnt_nbndlmnt == nlmnt)) then!--------------------------------------------                                                      !     !     !
call R3D_invert_square_matrix (Dlmnt, Dinv)                                                          !                                                     !     !     !
else                                                                                                 !                                                     !     !     !
Dinv = Dlmnt                                                                                         !                                                     !     !     !
do i = 1, 4; Dinv(i,i) = 1./Dlmnt(i,i); end do                                                       !                                                     !     !     !
end if!----------------------------------------------------------------------------------------------                                                      !     !     !
df (1)       = -df(1) - omg*A3D_Rvs(nlmnt)%rho*dt                                                                                                          !     !     !
df (2)       = -df(2) - omg*A3D_Rvs(nlmnt)%rhoV%x*dt                                                                                                       !     !     !
df (3)       = -df(3) - omg*A3D_Rvs(nlmnt)%rhoV%y*dt                                                                                                       !     !     !
df (4)       = -df(4) - omg*A3D_Rvs(nlmnt)%rhoet*dt                                                                                                        !     !     !
dus(:,nlmnt) = matmul(Dinv, df) + (1.-omg)*du_old(:,nlmnt)                                                                                                 !     !     !
end if; end do!--------------------------------------------------------------------------------------------------------------------------------------------      !     !
!$OMP END PARALLEL DO                                                                                                                                            !     !
end do!----------------------------------------------------------------------------------------------------------------------------------------------------------      !
!-----------------------------------------------------------------------------------------------------------------------------------                                   !
!-                      ------------------------------------------------------------------------------------------------------------                                   !
!-BLU-SGS backward sweep------------------------------------------------------------------------------------------------------------                                   !
!-                      ------------------------------------------------------------------------------------------------------------                                   !
!-----------------------------------------------------------------------------------------------------------------------------------                                   !
do nlvl     = Nlvls, 1, -1!--------------------------------------------------------------------------------------------------------------------------------------      !
Nlmnts_lvl  = size(nlmnt_lvl(nlvl)%n)                                                                                                                            !     !
!$OMP PARALLEL DO SCHEDULE(STATIC) DEFAULT(PRIVATE) &                                                                                                            !     !
!$OMP SHARED(Nlmnts_lvl, A3D_UGDS_dgs, A3D_UGDS_cells, A3D_UGDS_2D_bndrs, A3D_uold, A3D_vrbls, A3D_Rvs, du_old, du_new, dus, D, Id, dtlcl, &                     !     !
!$OMP isreallmnt, nx, ny, dS, lambdadg, gam, RG, nlvl, nlmnt_lvl, grp_lmnt, omg )                                                                                !     !
do ind_lmnt = Nlmnts_lvl, 1, -1; nlmnt = nlmnt_lvl(nlvl)%n(ind_lmnt); if (isreallmnt(nlmnt)) then!---------------------------------------------------------      !     !
df          = 0.                                                                                                                                           !     !     !
dt          = dtlcl(nlmnt)                                                                                                                                 !     !     !
vol         = A3D_UGDS_cells%A_lmnt(nlmnt)                                                                                                                 !     !     !
do ind      = size(A3D_UGDS_dgs%ndg_nlmnt(nlmnt)%n), 1, -1!----------------------------------------------------------------------------------------        !     !     !
ndg         = A3D_UGDS_dgs%ndg_nlmnt(nlmnt)%n(ind)                                                                                                 !       !     !     !
xmid_pnt    = 0.5*(A3D_UGDS_cells%x_vrtx(A3D_UGDS_dgs%dg_data(ndg)%n_point_1)%x + A3D_UGDS_cells%x_vrtx(A3D_UGDS_dgs%dg_data(ndg)%n_point_2)%x)    !       !     !     !
ymid_pnt    = 0.5*(A3D_UGDS_cells%x_vrtx(A3D_UGDS_dgs%dg_data(ndg)%n_point_1)%y + A3D_UGDS_cells%x_vrtx(A3D_UGDS_dgs%dg_data(ndg)%n_point_2)%y)    !       !     !     !
xbary       = A3D_UGDS_cells%x_lmnt(nlmnt)%x                                                                                                       !       !     !     !
ybary       = A3D_UGDS_cells%x_lmnt(nlmnt)%y                                                                                                       !       !     !     !
inwd        = (xbary-xmid_pnt)*nx(ndg) + (ybary-ymid_pnt)*ny(ndg)                                                                                  !       !     !     !
if (inwd > 0) then!--------------------------------------------------------------------------------------                                          !       !     !     !
nxlcl       = -nx(ndg)                                                                                   !                                         !       !     !     !
nylcl       = -ny(ndg)                                                                                   !                                         !       !     !     !
else                                                                                                     !                                         !       !     !     !
nxlcl       = nx(ndg)                                                                                    !                                         !       !     !     !
nylcl       = ny(ndg)                                                                                    !                                         !       !     !     !
end if!--------------------------------------------------------------------------------------------------                                          !       !     !     !
if (.not.(any(A3D_UGDS_2D_bndrs%ndg_nbnddg == ndg))) then!-----------------------------------------------                                          !       !     !     !
nlmnt_nb        = A3D_UGDS_dgs%dg_data(ndg)%n_element_1 + A3D_UGDS_dgs%dg_data(ndg)%n_element_2 - nlmnt  !                                         !       !     !     !
call nrml_flux_jac_l(gam,nxlcl,nylcl,A3D_uold(nlmnt_nb), Jj)                                             !                                         !       !     !     !
if      (grp_lmnt(nlmnt_nb)<grp_lmnt(nlmnt)) then!---------------------------------------------------    !                                         !       !     !     !
df              = df + omg*0.5*dt*dS(ndg)/vol*matmul(Jj-lambdadg(ndg)*Id, dus(:,nlmnt_nb))           !   !                                         !       !     !     !
else if (grp_lmnt(nlmnt_nb)>grp_lmnt(nlmnt)) then                                                    !   !                                         !       !     !     !
df              = df + omg*0.5*dt*dS(ndg)/vol*matmul(Jj-lambdadg(ndg)*Id, du_new(:,nlmnt_nb))        !   !                                         !       !     !     !
end if!----------------------------------------------------------------------------------------------    !                                         !       !     !     !
end if!--------------------------------------------------------------------------------------------------                                          !       !     !     !
end do!--------------------------------------------------------------------------------------------------------------------------------------------        !     !     !
Dlmnt(:,:)      = D(:,:,nlmnt)                                                                                                                             !     !     !
if (any(A3D_UGDS_2D_bndrs%nlmnt_nbndlmnt == nlmnt)) then!--------------------------------------------                                                      !     !     !
call R3D_invert_square_matrix (Dlmnt, Dinv)                                                          !                                                     !     !     !
else                                                                                                 !                                                     !     !     !
Dinv = Dlmnt                                                                                         !                                                     !     !     !
do i = 1, 4; Dinv(i,i) = 1./Dlmnt(i,i); end do                                                       !                                                     !     !     !
end if!----------------------------------------------------------------------------------------------                                                      !     !     !
df (1)          = -df(1) - omg*A3D_Rvs(nlmnt)%rho*dt                                                                                                       !     !     !
df (2)          = -df(2) - omg*A3D_Rvs(nlmnt)%rhoV%x*dt                                                                                                    !     !     !
df (3)          = -df(3) - omg*A3D_Rvs(nlmnt)%rhoV%y*dt                                                                                                    !     !     !
df (4)          = -df(4) - omg*A3D_Rvs(nlmnt)%rhoet*dt                                                                                                     !     !     !
du_new(:,nlmnt) = matmul(Dinv, df) + (1.-omg)*dus(:,nlmnt)                                                                                                 !     !     !
end if; end do!--------------------------------------------------------------------------------------------------------------------------------------------      !     !
!$OMP END PARALLEL DO                                                                                                                                            !     !
end do!----------------------------------------------------------------------------------------------------------------------------------------------------------      !
error_l         = 0.                                                                                                                                                   !
!$OMP PARALLEL DO SCHEDULE(STATIC) DEFAULT(SHARED) PRIVATE(nlmnt) REDUCTION( + :error_l)                                                                               !
do nlmnt        = 1, Nlmnts, 1; if (isreallmnt(nlmnt)) then!---------------------------------------------                                                              !
error_l         = error_l + norm2(du_new(:,nlmnt)-du_old(:,nlmnt))                                       !                                                             !
end if; end do!------------------------------------------------------------------------------------------                                                              !
!$OMP END PARALLEL DO                                                                                                                                                  !
error_l         = log10(error_l)                                                                                                                                       !
!-write out iterative errors for debugging                                                                                                                             !
! print*, "linear error at iteration #", k, "is: ", error_l                                                                                                            !
!$OMP PARALLEL DO SCHEDULE(STATIC) DEFAULT(PRIVATE) SHARED(Nlmnts, du_old, du_new, isreallmnt)                                                                         !
do nlmnt        = 1, Nlmnts, 1; if (isreallmnt(nlmnt)) du_old(:,nlmnt) = du_new(:,nlmnt); end do                                                                       !
!$OMP END PARALLEL DO                                                                                                                                                  !
end do!================================================================================================================================================================
!-write out the linear error of the last linear iteration K
!========================================================================================================
inquire(file='last_error_l_blu.txt', exist=file_exists)                                                  !
if (file_exists) then; open(unit = 10, file = 'last_error_l_blu.txt', position = 'append')               !
else; open(unit = 10, file = 'last_error_l_blu.txt'                     ); end if                        !
write(unit=10, fmt="(F10.6)") error_l                                                                    !
close(unit = 10)                                                                                         !
!========================================================================================================
!$OMP PARALLEL DO SCHEDULE(STATIC) DEFAULT(PRIVATE) SHARED(Nlmnts, A3D_unew, A3D_uold, du_new, isreallmnt)
do nlmnt = 1, Nlmnts, 1; if (isreallmnt(nlmnt)) then!----------------------------------------------------
A3D_unew(nlmnt)%rho    = A3D_uold(nlmnt)%rho    + du_new(1,nlmnt)                                        !
A3D_unew(nlmnt)%rhoV%x = A3D_uold(nlmnt)%rhoV%x + du_new(2,nlmnt)                                        !
A3D_unew(nlmnt)%rhoV%y = A3D_uold(nlmnt)%rhoV%y + du_new(3,nlmnt)                                        !
A3D_unew(nlmnt)%rhoet  = A3D_uold(nlmnt)%rhoet  + du_new(4,nlmnt)                                        !
end if; end do!------------------------------------------------------------------------------------------
!$OMP END PARALLEL DO
end subroutine blu_sgs_unstrctrd

subroutine jacobi_unstrctrd(K_it, A3D_thrm_data, A3D_UGDS_cells, A3D_UGDS_dgs, A3D_UGR1_2D_cells, A3D_UGDS_2D_bndrs, isreallmnt,&
                            A3D_Rvs, A3D_vrbls, A3D_uold, A3D_unew, xnueqdg, Smaxdg, dtlcl, rhoBC, pBC, VxBC, VyBC, TBC) 
!-----------------------------------------------------------------------------------------------------------------------------------
!-         -------------------------------------------------------------------------------------------------------------------------
!-interface-------------------------------------------------------------------------------------------------------------------------
!-         -------------------------------------------------------------------------------------------------------------------------
!-----------------------------------------------------------------------------------------------------------------------------------
integer                     ,                            intent(in   ) :: K_it
real                        ,                            intent(in   ) :: rhoBC
real                        ,                            intent(in   ) :: pBC
real                        ,                            intent(in   ) :: VxBC
real                        ,                            intent(in   ) :: VyBC
real                        ,                            intent(in   ) :: TBC
type(A3DthrmdataBSL        ),                            intent(in   ) :: A3D_thrm_data
type(A3DUGDS2Dplgnstplg    ),                            intent(in   ) :: A3D_UGDS_cells
type(A3DUGDS2Dplgnsdgsvrtcs),                            intent(in   ) :: A3D_UGDS_dgs
type(A3DUGR12Dcells        ),                            intent(in   ) :: A3D_UGR1_2D_cells
type(A3D2DNSbndrs          ),                            intent(in   ) :: A3D_UGDS_2D_bndrs  ! including boundary variables such as p_inf, etc.
logical                     , dimension(:), allocatable, intent(in   ) :: isreallmnt         ! phantom cells filter
type(A3DuNS                ), dimension(:), allocatable, intent(in   ) :: A3D_Rvs
type(A3DvNS                ), dimension(:), allocatable, intent(in   ) :: A3D_vrbls
real                        , dimension(:), allocatable, intent(in   ) :: xnueqdg            ! (Ndgs)
real                        , dimension(:), allocatable, intent(in   ) :: Smaxdg             ! (Ndgs)
real                        , dimension(:), allocatable, intent(in   ) :: dtlcl              ! (Nlmnts)
type(A3DuNS                ), dimension(:), allocatable, intent(in   ) :: A3D_uold
type(A3DuNS                ), dimension(:), allocatable, intent(inout) :: A3D_unew
!-----------------------------------------------------------------------------------------------------------------------------------
!-               -------------------------------------------------------------------------------------------------------------------
!-local_variables-------------------------------------------------------------------------------------------------------------------
!-               -------------------------------------------------------------------------------------------------------------------
!-----------------------------------------------------------------------------------------------------------------------------------
logical                                                                :: file_exists
real                                                                   :: x_dist_elements
real                                                                   :: y_dist_elements
real                                                                   :: dist_elements
real                                                                   :: xmid_pnt
real                                                                   :: ymid_pnt
real                                                                   :: xbary
real                                                                   :: ybary
real                                                                   :: inwd
real                                                                   :: nxlcl
real                                                                   :: nylcl
real                                                                   :: rhodg
real                                                                   :: udg
real                                                                   :: vdg
real                                                                   :: pdg
real                        , dimension(:)    , allocatable            :: lambdadg
real                        , dimension(:)    , allocatable            :: nx
real                        , dimension(:)    , allocatable            :: ny
real                        , dimension(:)    , allocatable            :: dS
real                        , dimension(:,:,:), allocatable            :: D
real                        , dimension(:,:)  , allocatable            :: Dlmnt
real                        , dimension(:,:)  , allocatable            :: Dinv
real                        , dimension(4,4)                           :: Id
real                        , dimension(4,4)                           :: T
real                        , dimension(4,4)                           :: Tinv
real                        , dimension(4,4)                           :: Ji
real                        , dimension(4,4)                           :: Jj
real                        , dimension(4,4)                           :: Jij
real                        , dimension(:,:)  , allocatable            :: dus
real                        , dimension(:,:)  , allocatable            :: du_old
real                        , dimension(:,:)  , allocatable            :: du_new
real                        , dimension(:)    , allocatable            :: df
real                                                                   :: error_l

Ndgs    = size(A3D_UGDS_dgs%dg_data)
Nlmnts  = size(A3D_UGDS_dgs%ndg_nlmnt)
Ndgs    = size(A3D_UGDS_dgs%dg_data)
Nbnddgs = size(A3D_UGDS_2D_bndrs%kBC_dg)
allocate(nx      (Ndgs               ))
allocate(ny      (Ndgs               ))
allocate(dS      (Ndgs               ))
allocate(lambdadg(Ndgs               ))
allocate(D       (4   ,4     ,Nlmnts ))
allocate(Dlmnt   (4   ,4             ))
allocate(Dinv    (4   ,4             ))
allocate(dus     (4   ,Nlmnts        ))
allocate(du_old  (4   ,Nlmnts        ))
allocate(du_new  (4   ,Nlmnts        ))
allocate(df      (4                  ))

!-----------------------------------------------------------------------------------------------------------------------------------
!-                                       -------------------------------------------------------------------------------------------
!-compute spectral radius Lambda on edges-------------------------------------------------------------------------------------------
!-                                       -------------------------------------------------------------------------------------------
!-----------------------------------------------------------------------------------------------------------------------------------
!$OMP PARALLEL DO SCHEDULE(STATIC) DEFAULT(PRIVATE) &
!$OMP SHARED(Ndgs, A3D_UGDS_dgs, A3D_UGR1_2D_cells, A3D_UGDS_cells, A3D_vrbls, isreallmnt, Smaxdg, xnueqdg, nx, ny, dS, lambdadg)
do ndg = 1, Ndgs, 1!---------------------------------------------------------------------------------------------------------------
nlmnt_1 = A3D_UGDS_dgs%dg_data(ndg)%n_element_1                                                                                    !
nlmnt_2 = A3D_UGDS_dgs%dg_data(ndg)%n_element_2                                                                                    !
if (isreallmnt(nlmnt_1) .or. isreallmnt(nlmnt_2)) then!-----------------------------------------------------------------------     !
nx(ndg) = A3D_UGR1_2D_cells%Gpnt_xyzw(ndg)%dSGp(1)%x                                                                          !    !
ny(ndg) = A3D_UGR1_2D_cells%Gpnt_xyzw(ndg)%dSGp(1)%y                                                                          !    !
dS(ndg) = sqrt(nx(ndg)**2. + ny(ndg)**2.)                                                                                     !    !
nx(ndg) = -nx(ndg)/dS(ndg)                                                                                                    !    !
ny(ndg) = -ny(ndg)/dS(ndg)                                                                                                    !    !
                                                                                                                              !    !
x_dist_elements   = A3D_UGDS_cells%x_lmnt(nlmnt_1)%x - A3D_UGDS_cells%x_lmnt(nlmnt_2)%x                                       !    !
y_dist_elements   = A3D_UGDS_cells%x_lmnt(nlmnt_1)%y - A3D_UGDS_cells%x_lmnt(nlmnt_2)%y                                       !    !
dist_elements     = abs(x_dist_elements*nx(ndg)     + y_dist_elements*ny(ndg))                                                !    !
rhodg             = 0.5   * (A3D_vrbls(nlmnt_1)%rho + A3D_vrbls(nlmnt_2)%rho)                                                 !    !
udg               = 0.5   * (A3D_vrbls(nlmnt_1)%V%x + A3D_vrbls(nlmnt_2)%V%x)                                                 !    !
vdg               = 0.5   * (A3D_vrbls(nlmnt_1)%V%y + A3D_vrbls(nlmnt_2)%V%y)                                                 !    !
pdg               = 0.5   * (A3D_vrbls(nlmnt_1)%p   + A3D_vrbls(nlmnt_2)%p)                                                   !    !
lambdadg(ndg)     = abs(udg*nx(ndg) + vdg*ny(ndg))  + Smaxdg(ndg) +(2.*xnueqdg(ndg))/dist_elements                            !    !
end if!-----------------------------------------------------------------------------------------------------------------------     !
end do!----------------------------------------------------------------------------------------------------------------------------
!$OMP END PARALLEL DO

gam   = A3D_thrm_data%gamma
RG    = A3D_thrm_data%Rg
Id    = 0.
do i  = 1, 4, 1; Id(i,i) = 1.; end do
!-----------------------------------------------------------------------------------------------------------------------------------
!-                     -------------------------------------------------------------------------------------------------------------
!-D matrices assembling-------------------------------------------------------------------------------------------------------------
!-                     -------------------------------------------------------------------------------------------------------------
!-----------------------------------------------------------------------------------------------------------------------------------
!$OMP PARALLEL DO SCHEDULE(STATIC) DEFAULT(PRIVATE) &
!$OMP SHARED(Nlmnts, Nbnddgs, A3D_UGDS_dgs, A3D_UGDS_cells, A3D_UGDS_2D_bndrs, A3D_uold, A3D_vrbls, D, Id, dtlcl, &
!$OMP isreallmnt, nx, ny, dS, lambdadg, gam, RG, rhoBC, pBC, VxBC, VyBC, TBC)
do nlmnt     = 1, Nlmnts, 1; if (isreallmnt(nlmnt)) then!--------------------------------------------------------------------------------------------------
vol          = A3D_UGDS_cells%A_lmnt(nlmnt)                                                                                                                !
dt           = dtlcl(nlmnt)                                                                                                                                !
D(:,:,nlmnt) = Id                                                                                                                                          !
do ind       = 1, size(A3D_UGDS_dgs%ndg_nlmnt(nlmnt)%n)!--------------------------------------------------------------------------------------------       !
ndg          = A3D_UGDS_dgs%ndg_nlmnt(nlmnt)%n(ind)                                                                                                 !      !
D(:,:,nlmnt) = D(:,:,nlmnt) + 0.5*dt/vol*lambdadg(ndg)*dS(ndg)*Id                                                                                   !      !
xmid_pnt     = 0.5*(A3D_UGDS_cells%x_vrtx(A3D_UGDS_dgs%dg_data(ndg)%n_point_1)%x + A3D_UGDS_cells%x_vrtx(A3D_UGDS_dgs%dg_data(ndg)%n_point_2)%x)    !      !
ymid_pnt     = 0.5*(A3D_UGDS_cells%x_vrtx(A3D_UGDS_dgs%dg_data(ndg)%n_point_1)%y + A3D_UGDS_cells%x_vrtx(A3D_UGDS_dgs%dg_data(ndg)%n_point_2)%y)    !      !
xbary        = A3D_UGDS_cells%x_lmnt(nlmnt)%x                                                                                                       !      !
ybary        = A3D_UGDS_cells%x_lmnt(nlmnt)%y                                                                                                       !      !
inwd         = (xbary-xmid_pnt)*nx(ndg) + (ybary-ymid_pnt)*ny(ndg)                                                                                  !      !
if (inwd > 0) then!-----------------------------------------------------------------------------------------                                        !      !
nxlcl        = -nx(ndg)                                                                                     !                                       !      !
nylcl        = -ny(ndg)                                                                                     !                                       !      !
else                                                                                                        !                                       !      !
nxlcl        = nx(ndg)                                                                                      !                                       !      !
nylcl        = ny(ndg)                                                                                      !                                       !      !
end if!-----------------------------------------------------------------------------------------------------                                        !      !
!-compute D matrices for boundary edges                                                                                                             !      !
if (any(A3D_UGDS_2D_bndrs%ndg_nbnddg == ndg)) then!-----------------------------------------------------------------------------------------        !      !
do i = 1, Nbnddgs; if (A3D_UGDS_2D_bndrs%ndg_nbnddg(i) == ndg) nbnddg = i; end do                                                           !       !      !
kBC  = A3D_UGDS_2D_bndrs%kBC_dg(nbnddg)                                                                                                     !       !      !
Jij  = 0.; Ji = 0.; Jj = 0.; Tinv = 0.; T = 0.                                                                                              !       !      !
nlmnt_nb = A3D_UGDS_dgs%dg_data(ndg)%n_element_1 + A3D_UGDS_dgs%dg_data(ndg)%n_element_2 - nlmnt                                            !       !      !
call Tinv_conversion_l (gam,A3D_uold(nlmnt), Tinv)                                                                                          !       !      !
call nrml_flux_jac_l(gam,nxlcl,nylcl,A3D_uold(nlmnt), Ji)                                                                                   !       !      !
if      (kBC == 0)  then         ! adiabatic wall-----------------------------------------------------------                                !       !      !
Jij(1,1) = 1.; Jij(2,2) = -1.; Jij(3,3) = -1.; Jij(4,4) = 1.                                                !                               !       !      !
else if (kBC == 40) then         ! subsonic Riemann BCs (based on inward-pointing normals)------------------!                               !       !      !
!-enforce inward-pointing normals for boundary edges                                                        !                               !       !      !
call riemann_jij(gam, RG, -nxlcl, -nylcl, rhoBC, pBC, VxBC, VyBC, TBC, A3D_vrbls(nlmnt), Jij)               !                               !       !      !
end if!-----------------------------------------------------------------------------------------------------                                !       !      !
call T_conversion_l (gam,A3D_uold(nlmnt_nb), T)                                                                                             !       !      !
call nrml_flux_jac_l(gam,nxlcl,nylcl,A3D_uold(nlmnt_nb), Jj)                                                                                !       !      !
D(:,:,nlmnt) = D(:,:,nlmnt) + 0.5*dt/vol*dS(ndg)*(matmul(2.*(Jj-lambdadg(ndg)*Id), matmul(T, matmul(Jij, Tinv))) + (Ji+lambdadg(ndg)*Id))   !       !      !
end if!-------------------------------------------------------------------------------------------------------------------------------------        !      !
end do!---------------------------------------------------------------------------------------------------------------------------------------------       !
end if; end do!--------------------------------------------------------------------------------------------------------------------------------------------
!$OMP END PARALLEL DO

!$OMP PARALLEL WORKSHARE
du_old(:,:)      = 0.
du_new(:,:)      = 0.
!$OMP END PARALLEL WORKSHARE
!-----------------------------------------------------------------------------------------------------------------------------------
!-                   ---------------------------------------------------------------------------------------------------------------
!-Jacobi inner k-loop---------------------------------------------------------------------------------------------------------------
!-                   ---------------------------------------------------------------------------------------------------------------
!-----------------------------------------------------------------------------------------------------------------------------------
do k = 1, K_it, 1!--------------------------------------------------------------------------------------------------------------------------------------
!$OMP PARALLEL DO SCHEDULE(STATIC) DEFAULT(PRIVATE) &                                                                                                   !
!$OMP SHARED(Nlmnts, A3D_UGDS_dgs, A3D_UGDS_cells, A3D_UGDS_2D_bndrs, A3D_uold, A3D_vrbls, A3D_Rvs, du_old, du_new, D, Id, dtlcl, &                     !
!$OMP isreallmnt, nx, ny, dS, lambdadg, gam, RG)                                                                                                        !
do nlmnt = 1, Nlmnts, 1; if (isreallmnt(nlmnt)) then!-----------------------------------------------------------------------------------------------    !
df       = 0.                                                                                                                                       !   !
dt       = dtlcl(nlmnt)                                                                                                                             !   !
vol      = A3D_UGDS_cells%A_lmnt(nlmnt)                                                                                                             !   !
do ind   = 1, size(A3D_UGDS_dgs%ndg_nlmnt(nlmnt)%n), 1!----------------------------------------------------------------------------------------     !   !
ndg      = A3D_UGDS_dgs%ndg_nlmnt(nlmnt)%n(ind)                                                                                                !    !   !
xmid_pnt = 0.5*(A3D_UGDS_cells%x_vrtx(A3D_UGDS_dgs%dg_data(ndg)%n_point_1)%x + A3D_UGDS_cells%x_vrtx(A3D_UGDS_dgs%dg_data(ndg)%n_point_2)%x)   !    !   !
ymid_pnt = 0.5*(A3D_UGDS_cells%x_vrtx(A3D_UGDS_dgs%dg_data(ndg)%n_point_1)%y + A3D_UGDS_cells%x_vrtx(A3D_UGDS_dgs%dg_data(ndg)%n_point_2)%y)   !    !   !
xbary    = A3D_UGDS_cells%x_lmnt(nlmnt)%x                                                                                                      !    !   !
ybary    = A3D_UGDS_cells%x_lmnt(nlmnt)%y                                                                                                      !    !   !
inwd     = (xbary-xmid_pnt)*nx(ndg) + (ybary-ymid_pnt)*ny(ndg)                                                                                 !    !   !
if (inwd > 0) then!--------------------------------------------------------------------------------------                                      !    !   !
nxlcl    = -nx(ndg)                                                                                      !                                     !    !   !
nylcl    = -ny(ndg)                                                                                      !                                     !    !   !
else                                                                                                     !                                     !    !   !
nxlcl    = nx(ndg)                                                                                       !                                     !    !   !
nylcl    = ny(ndg)                                                                                       !                                     !    !   !
end if!--------------------------------------------------------------------------------------------------                                      !    !   !
if (.not.(any(A3D_UGDS_2D_bndrs%ndg_nbnddg == ndg))) then!-----------------------------------------------                                      !    !   !
nlmnt_nb = A3D_UGDS_dgs%dg_data(ndg)%n_element_1 + A3D_UGDS_dgs%dg_data(ndg)%n_element_2 - nlmnt         !                                     !    !   !
call nrml_flux_jac_l(gam,nxlcl,nylcl,A3D_uold(nlmnt_nb), Jj)                                             !                                     !    !   !
df       = df + 0.5*dt*dS(ndg)/vol*matmul(Jj-lambdadg(ndg)*Id, du_old(:,nlmnt_nb))                       !                                     !    !   !
end if!--------------------------------------------------------------------------------------------------                                      !    !   !
end do!----------------------------------------------------------------------------------------------------------------------------------------     !   !
! apply equation (14)                                                                                                                               !   !
Dlmnt(:,:)      = D(:,:,nlmnt)                                                                                                                      !   !
if (any(A3D_UGDS_2D_bndrs%nlmnt_nbndlmnt == nlmnt)) then!------------------------------------------------                                           !   !      
call R3D_invert_square_matrix (Dlmnt, Dinv)                                                              !                                          !   !      
else                                                                                                     !                                          !   !      
Dinv = Dlmnt                                                                                             !                                          !   !
do i = 1, 4; Dinv(i,i) = 1./Dlmnt(i,i); end do                                                           !                                          !   !
end if!--------------------------------------------------------------------------------------------------                                           !   !      
df (1)          = -df(1) - A3D_Rvs(nlmnt)%rho*dt                                                                                                    !   !
df (2)          = -df(2) - A3D_Rvs(nlmnt)%rhoV%x*dt                                                                                                 !   !
df (3)          = -df(3) - A3D_Rvs(nlmnt)%rhoV%y*dt                                                                                                 !   !
df (4)          = -df(4) - A3D_Rvs(nlmnt)%rhoet*dt                                                                                                  !   !
du_new(:,nlmnt) = matmul(Dinv, df)                                                                                                                  !   !
end if; end do!-------------------------------------------------------------------------------------------------------------------------------------    !
!$OMP END PARALLEL DO                                                                                                                                   !
error_l  = 0.                                                                                                                                           !
!$OMP PARALLEL DO SCHEDULE(STATIC) DEFAULT(SHARED) PRIVATE(nlmnt) REDUCTION( + :error_l)                                                                !
do nlmnt = 1, Nlmnts, 1; if (isreallmnt(nlmnt)) then!----------------------------------------------------                                               !
error_l  = error_l + norm2(du_new(:,nlmnt)-du_old(:,nlmnt))                                              !                                              !
end if; end do!------------------------------------------------------------------------------------------                                               !
!$OMP END PARALLEL DO                                                                                                                                   !
error_l  = log10(error_l)                                                                                                                               !
!========================================================================================================                                               !
!-write out iterative errors for debugging                                                               !                                              !
! print*, "linear error at iteration #", k, "is: ", error_l                                              !                                              !
!-write out the linear error of the last linear iteration K                                              !                                              !
! inquire(file='last_error_l_jac.txt', exist=file_exists)                                                !                                              !
! if (file_exists) then; open(unit = 11, file = 'last_error_l_jac.txt', position = 'append')             !                                              !
!                  else; open(unit = 11, file = 'last_error_l_jac.txt'                     ); end if     !                                              !
! write(unit=11, fmt="(F10.6)") error_l                                                                  !                                              !
! close(unit = 11)                                                                                       !                                              !
!========================================================================================================                                               !
!$OMP PARALLEL DO SCHEDULE(STATIC) DEFAULT(PRIVATE) SHARED(Nlmnts, du_old, du_new, isreallmnt)                                                          !
do nlmnt = 1, Nlmnts, 1; if (isreallmnt(nlmnt)) du_old(:,nlmnt) = du_new(:,nlmnt); end do                                                               !
!$OMP END PARALLEL DO                                                                                                                                   !
end do!-------------------------------------------------------------------------------------------------------------------------------------------------
!$OMP PARALLEL DO SCHEDULE(STATIC) DEFAULT(PRIVATE) SHARED(Nlmnts, A3D_unew, A3D_uold, du_new, isreallmnt)
do nlmnt = 1, Nlmnts, 1; if (isreallmnt(nlmnt)) then!----------------------------------------------------
A3D_unew(nlmnt)%rho    = A3D_uold(nlmnt)%rho    + du_new(1,nlmnt)                                        !
A3D_unew(nlmnt)%rhoV%x = A3D_uold(nlmnt)%rhoV%x + du_new(2,nlmnt)                                        !
A3D_unew(nlmnt)%rhoV%y = A3D_uold(nlmnt)%rhoV%y + du_new(3,nlmnt)                                        !
A3D_unew(nlmnt)%rhoet  = A3D_uold(nlmnt)%rhoet  + du_new(4,nlmnt)                                        !
end if; end do!------------------------------------------------------------------------------------------
!$OMP END PARALLEL DO
end subroutine jacobi_unstrctrd

end module A3D_2D_sch_imp
