  module A3D_sch_NS
  use A3D_data_structure
  use A3D_vrbls_types
!-----------------------------------------------------------------------
  type A3Dprfrmnc
  character(len= : ), allocatable :: name
  real                            :: cpu_time_s
  real                            :: wck_time_s
  integer                         :: N_times_called
  integer                         :: ind_A3D
  end type A3Dprfrmnc
!-----------------------------------------------------------------------
  type A3Druntimeprmtrs
  character(len= : ), allocatable :: A3D_prgrm
  real                            :: cpu_time_s_run0
  real                            :: wck_time_s_run0
  real                            :: cpu_time_s_it0
  real                            :: wck_time_s_it0
  real                            :: cpu_time_s
  real                            :: wck_time_s
  end type A3Druntimeprmtrs
!-----------------------------------------------------------------------
  type, extends(A3Druntimeprmtrs) :: A3DruntimeprmtrsNS2D
  integer                         :: k_srdr
  integer                         :: k_sch
  integer                         :: k_RS
  integer                         :: k_WENO
  integer                         :: k_map
  end type A3DruntimeprmtrsNS2D
!-----------------------------------------------------------------------
  type A3D2Dbndrs
  integer     , dimension(:), allocatable :: nlmnt_nbndlmnt  ! (Nbndlmnts)               nlmnt    of boundary-element
  integer     , dimension(:), allocatable :: ndg_nbndlmnt    ! (Nbndlmnts)%(Nbnddgslmnt) ndg      of boundary-element
  integer     , dimension(:), allocatable :: nlmnt_nbnddg    ! (Nbnddgs  )               nlmnt    of boundary-edge
  integer     , dimension(:), allocatable :: nbndlmnt_nbnddg ! (Nbnddgs  )               nbndlmnt of boundary-edge
  integer     , dimension(:), allocatable :: ndg_nbnddg      ! (Nbnddgs  )               ndg      of boundary-edge
  integer     , dimension(:), allocatable :: kBC_lmnt        ! (Nbndlmnts)%(Nbnddgslmnt) kBC      in bndlmnt structure
  integer     , dimension(:), allocatable :: kBC_dg          ! (Nbnddgs  )               kBC      in bnddg   structure
  integer     , dimension(:), allocatable :: nBCdata         ! (Nbnddgs  )               nBC      in BCdata  structure
!--------------------
  integer     , dimension(:), allocatable :: nlmnt_nbndphntm ! (Nbndphntms)              nlmnt    of boundary-phantom
  integer     , dimension(:), allocatable :: nbnddg_nbndphntm! (Nbndphntms)              nbnddg   of boundary-phantom-donnor
  integer     , dimension(:), allocatable :: kBC_phntm
  end type A3D2Dbndrs
! integer     , dimension(:), allocatable :: nbnddg_ndg      ! (Ndgs)                    nbnddg in dg structure
!-----------------------------------------------------------------------
  type                     :: A3DNSBCdata
  type(A3DvNS) :: vrbls
  real         :: qn
  end type A3DNSBCdata
!-----------------------------------------------------------------------
  type, extends(A3D2Dbndrs):: A3D2DNSbndrs
! real ::   pBC
! real :: rhoBC
! real ::   TBC
! real ::   uBC
! real ::   vBC
  type(A3DNSBCdata), dimension(:), allocatable :: BCdata ! BCdata(NBCdatas) addressed via A3D2Dbndrs%nBCdata(nbnddg) !--> COULD BE UPDATED TO bnddg_GPs if needed
  end type A3D2DNSbndrs
!-----------------------------------------------------------------------
! interface A3D_uvw
! module procedure A3D_NS_u_to_v
! module procedure A3D_NS_v_to_u
! end interface A3D_uvw
!-----------------------------------------------------------------------
  contains
!-----------------------------------------------------------------------
! type A3DthrmdataBSL                                                    ! baseline perfect-gas with Sutherland-like viscosity and Stokes-hypothesis
! real                            :: gamma
! real                            :: Rg
! real                            :: xmu0
! real                            :: xlb0
! real                            :: Tmu0
! real                            :: Smu
! real                            :: Alb
! end type A3DthrmdataBSL
!-----------------------------------------------------------------------
! type A3Dvsc
! real                            :: xmu
! real                            :: xlb
! real                            :: xmub
! end type A3Dvsc
!-----------------------------------------------------------------------
! type A3DUGDS2Dplgnstplg
!-----------------------------------------------------------------------
! sequence
! type(R3Dxyzvector   ), dimension(:), allocatable :: x_vrtx            ! (Nps) gridpoints (vertices) xyz-coordinates           ! Nps=Nvrtcs=size(A3DUGDS2Dplgnsgmtr%xvrtx) gridpoints (vertices)
! type(R3Dnn0         ), dimension(:), allocatable :: np_nlmnt          ! (Nlmnts)%n(Nvrtslmnt(nlmnt)) vertices of each element ! Nlmnts    =size(A3DUGDS2Dplgnsgmtr%np_nlmnt) elements (cells or polygons)
! type(R3Dxyzvector   ), dimension(:), allocatable :: x_lmnt            ! (Nlmnts) element-barycenter (BC) xyz-coordinates
! real                 , dimension(:), allocatable :: dl_lmnt           ! (Nlmnts) element-maxlength (maximum distance between vertices)
! real                 , dimension(:), allocatable :: d_lmnt            ! (Nlmnts) element-minlength (twice the minimum normal distance between BC and edges)
! real                 , dimension(:), allocatable :: A_lmnt            ! (Nlmnts) element-area
!-----------------------------------------------------------------------
! end type A3DUGDS2Dplgnstplg
!-----------------------------------------------------------------------------------------------------------------------------------
!-                                                                                               -----------------------------------
!-A3D_unstructured_grid_data_structure::2D_polygons_edgs_and_vrtcs (A3D_UGDS_2D_plgns_edgs_vrtcs)-----------------------------------
!-                                                                                               -----------------------------------
!-----------------------------------------------------------------------------------------------------------------------------------
! type A3DUGDS2Dplgnsdgsvrtcs
!-----------------------------------------------------------------------
! sequence
! type(R3Dnn0         ), dimension(:), allocatable :: nlmnt_np          ! (Nps)%n(Nlmnts_np(np)) elements with vertex np                     ! Nps=Nvrtcs=size(A3DUGDS2Dplgnsdgsvrtcs%nlmnt_np) vertices (gridpoints)
! type(R3Ddataedge    ), dimension(:), allocatable :: dg_data           ! (Ndgs) edge_data; ndg is the index in the edge-based structure     ! Ndgs      =size(A3DUGDS2Dplgnsdgsvrtcs%dg_data) edges
! type(R3Dnn0         ), dimension(:), allocatable :: ndg_nlmnt         ! (Nlmnts)%n(Ndgs_nlmnt(nlmnt)) edges of the contour of each element ! Nlmnts    =size(A3DUGDS2Dplgnsdgsvrtcs%ndg_nlmnt) elements (cells or polygons)
!-----------------------------------------------------------------------
! end type A3DUGDS2Dplgnsdgsvrtcs
! type A3DUGR12Dcells
!-----------------------------------------------------------------------
! sequence
! type(R3Dnn0                ), dimension(:), allocatable :: nstncl
! type(R3Dxyzvector          ), dimension(:), allocatable :: dx_lmnt
! type(R3Dpcoeffs            ), dimension(:), allocatable :: p_R1_coeffs
! type(A3DxyzwGp             ), dimension(:), allocatable :: Gpnt_xyzw
! type(R3DAlsqconddata       ), dimension(:), allocatable :: A_lsq_cond_data
!-----------------------------------------------------------------------
! end type A3DUGR12Dcells
  subroutine A3D_sch_NS_2D ( A3Dr_mssgs, A3D_runtime_NS2D, A3D_thrm_data,&
                             A3D_UGDS_cells, A3D_UGDS_dgs, A3D_UGR1_2D_cells, A3D_vrbls, A3D_Rvs, A3D_UGDS_2D_bndrs, dtlcl, Smaxdg, xnueqdg)!,&
!                            p_RD_coeffs, p_RN_coeffs, p_R1_coeffs)
!-----------------------------------------------------------------------------------------------------------------------------------
!-                                                                                                                                --
!-subroutine A3D_sch_NS_2D                                                                                                        --
!-                                                                                                                                --
!-                                                                                                                                --
!-                                                                                                                                --
!-                                                                                                                                --
!-                                                                                                                                --
!-                                                                                                                                --
!-                                                                                                                                --
!-                                                                                                                                --
!-                                                                                                                                --
!-----------------------------------------------------------------------------------------------------------------------------------
!
!-----------------------------------------------------------------------
!-     -----------------------------------------------------------------
!-types-----------------------------------------------------------------
!-     -----------------------------------------------------------------
!-----------------------------------------------------------------------
!$ use OMP_LIB
   use A3D_thrm
   use A3D_mssgs
   use A3D_types
   use A3D_vrbls_types
   use R3D_types
   use A3D_data_structure
   use A3D_tensors
   use A3Dg_2D_routines
   use R3D_2D_lsq_WENO
   use R3D_to_plots_routines
   use A3D_RS_ARS
!--for_subroutine_divtauq-----------------------------------------------
   use R3D_maths
!-----------------------------------------------------------------------
  implicit none
!-----------------------------------------------------------------------
!-         -------------------------------------------------------------
!-interface-------------------------------------------------------------
!-         -------------------------------------------------------------
!-----------------------------------------------------------------------
  type(R3Dmssgs              ),                            intent(inout) :: A3Dr_mssgs
  type(A3DruntimeprmtrsNS2D  ),                            intent(inout) :: A3D_runtime_NS2D
  type(A3DthrmdataBSL        ),                            intent(in   ) :: A3D_thrm_data
  type(A3DUGDS2Dplgnstplg    ),                            intent(in   ) :: A3D_UGDS_cells
  type(A3DUGDS2Dplgnsdgsvrtcs),                            intent(in   ) :: A3D_UGDS_dgs
  type(A3DUGR12Dcells        ),                            intent(in   ) :: A3D_UGR1_2D_cells
  type(A3DvNS                ), dimension(:),              intent(in   ) :: A3D_vrbls
  type(A3DuNS                ), dimension(:), allocatable, intent(inout) :: A3D_Rvs
  type(A3D2DNSbndrs          ),                            intent(in   ) :: A3D_UGDS_2D_bndrs
  real                        , dimension(:), allocatable, intent(inout) :: dtlcl
  real                        , dimension(:), allocatable, intent(inout) :: Smaxdg
  real                        , dimension(:), allocatable, intent(inout) :: xnueqdg
!-----------------------------------------------------------------------
!-               -------------------------------------------------------
!-local_variables-------------------------------------------------------
!-               -------------------------------------------------------
!-----------------------------------------------------------------------
  real                 , dimension(:), allocatable                :: v
  type(R3Dvn0         ), dimension(:), allocatable                :: v_L
  type(R3Dvn0         ), dimension(:), allocatable                :: v_R
  type(A3DvNS_vn0     ), dimension(:), allocatable                :: A3D_vrbls_L
  type(A3DvNS_vn0     ), dimension(:), allocatable                :: A3D_vrbls_R
!-HACK::R3D_types_legacy_routines---------------------------------------
  type(R3Dxyzvector), dimension(:), allocatable :: xn
  real              , dimension(:), allocatable :: xArea
  type(R3DpvMF     ), dimension(:), allocatable :: vL
  type(R3DpvMF     ), dimension(:), allocatable :: vR
  type(R3DvMF      ), dimension(:), allocatable :: F
       real         , dimension(:), allocatable :: S_max
  type(R3Dnn0      ), dimension(:), allocatable :: nGp
!   real              , dimension(:), allocatable :: Smaxdg
  real              , dimension(:), allocatable :: xnueq
!   real              , dimension(:), allocatable :: xnueqdg
  real              , dimension(:), allocatable :: fwrk
  real              , dimension(:), allocatable :: gwrk
  real              , dimension(:), allocatable :: nwrk
  real              , dimension(:), allocatable :: Rv0
  real              , dimension(:), allocatable :: Rv1
  real              , dimension(:), allocatable :: Rv2 
  real              , dimension(:), allocatable :: Rv4
!-----------------------------------------------------------------------
  integer      :: Ndgs, Nvrtcs, Ncells, Nlmnts, k_sch, k_srdr, ndg, np1, np2, nlmnt, nlmnt1, nlmnt2, nvrbl
  integer      :: NGs, N_G_s, N_L_s, N_R_s, n, nG, N_ngbrs_lsq, mdg, ind, Ndgs_nlmnt
  integer      :: Ndgs_lmnt, Ngbs_lsq, nlmnt_ngb, NGs_dg, mnd, nbndlmnt, Ncs
  real         :: axrho, axVx, axVy, axp, ayrho, ayVx, ayVy, ayp, rho, Vx, Vy, p, Vnlmnt, VnBC, VpxBC, VpyBC! xG, yG
  real         :: wGdS, Smaxlmnt, xnueqlmnt, fctr
  type(A3DuNS) :: FdS_12
  type(A3DuNS) , dimension(:), allocatable :: FdS
!-----------------------------------------------------------------------
  integer      :: nbnddg, Nbnddgs, kBC
  real         :: Riem_i, Riem_o, Vpxlmnt, Vpylmnt
!-for_subroutine_divtauq------------------------------------------------------------------------------------------------------------
  integer, dimension(:,:), allocatable :: npwrs, npwrsd, nc_npwrs
! type(A3Dxyvector_int), dimension(:), allocatable :: drvtvrule
  real          :: T, Theta, VtaudS
  type(A3Dvsc)                                           :: vsc
  type(R3Dxn0), dimension(:), allocatable                :: dTd_L
  type(R3Dxn0), dimension(:), allocatable                :: dTd_R
  type(R3Dxn0), dimension(:), allocatable                :: dud_L
  type(R3Dxn0), dimension(:), allocatable                :: dud_R
  type(R3Dxn0), dimension(:), allocatable                :: dvd_L
  type(R3Dxn0), dimension(:), allocatable                :: dvd_R
  type(R3Dxyzvector)                                     :: dTd, q
  type(R3Dxyzvector)                                     :: dud
  type(R3Dxyzvector)                                     :: dvd
  type(R3Dxyztensor2)                                    :: gradV
  type(R3Dxyzsymtensor2)                                 :: S
  type(R3Dxyzsymtensor2)                                 :: tau
  type(R3Dxyzvector)                                     :: Vlct, wdSdg
!-----------------------------------------------------------------------
  integer                                                 :: ngblmnt, Ngbs_lmnt, mG, MGs_dg
! type(R3Dpcoeffs            ), dimension(:), allocatable :: p_R1_coeffs
! type(R3Dpcoeffs            ), dimension(:), allocatable :: p_RD_coeffs
! type(R3Dpcoeffs            ), dimension(:), allocatable :: p_RN_coeffs
! type(R3Dpcoeffs            )                            :: p_Rc_coeffs
  real                        , dimension(:), allocatable :: udata
  real                        , dimension(:), allocatable :: apRrho
  real                        , dimension(:), allocatable :: apRVx
  real                        , dimension(:), allocatable :: apRVy
  real                        , dimension(:), allocatable :: apRp
  real                        , dimension(:), allocatable :: apRVpx
  real                        , dimension(:), allocatable :: apRVpy
  real                        , dimension(:), allocatable :: apRs
  real                        , dimension(:), allocatable :: apRRi
  real                        , dimension(:), allocatable :: apRRo
  type(R3Dxyzvector   )                                   :: xGp, xlmnt, dxlmnt
  real                 :: xnxn, Vn, Vpx, Vpy, xs, cp, T_ISA, p_ISA, a
  type(R3Dxyzvector)                                     :: xxn
!-----------------------------------------------------------------------------------------------------------------------------------
!-     -----------------------------------------------------------------------------------------------------------------------------
!-sizes-----------------------------------------------------------------------------------------------------------------------------
!-     -----------------------------------------------------------------------------------------------------------------------------
!-----------------------------------------------------------------------------------------------------------------------------------
  Ndgs  = size(A3D_UGDS_dgs%dg_data )
  Nvrtcs= size(A3D_UGDS_cells%x_vrtx)
  Ncells= size(A3D_UGDS_cells%x_lmnt); Nlmnts= Ncells
! write(*,*) 'A3D_sch_NS_2D ::', Ndgs, Nvrtcs, Ncells, size(A3D_UGR1_2D_cells%Gpnt_xyzw) !<-write::DELETE
!-----------------------------------------------------------------------------------------------------------------------------------
!-  --------------------------------------------------------------------------------------------------------------------------------
!-R1--------------------------------------------------------------------------------------------------------------------------------
!-  --------------------------------------------------------------------------------------------------------------------------------
!-----------------------------------------------------------------------------------------------------------------------------------
!---->  k_srdr=2; allocate(v(Nlmnts))
  k_srdr=A3D_runtime_NS2D%k_srdr! write(*,*) ' A3D_sch_NS_2D: k_srdr = ', k_srdr
  allocate(v(Nlmnts))
  call R3D_polynomial_powers ( 2, k_srdr-1, npwrs, 0)
! call A3D_2D_plnml_pwrs ( k_srdr-1, npwrs, nc_npwrs, 0)
  allocate (A3D_vrbls_L(Ndgs))
  allocate (A3D_vrbls_R(Ndgs))
  do nvrbl = 1, 5, 1!-----------------------------------------------------------------------------------------------------------------
!-----------------------------------------------------------------------                                                              !
  select case (nvrbl)                                                                                                                 !
  case (1); call xtrct_dim1_from_A3DvNS (A3D_vrbls, 'p'  , v)                                                                         !
  case (2); call xtrct_dim1_from_A3DvNS (A3D_vrbls, 'rho', v)                                                                         !
  case (3); call xtrct_dim1_from_A3DvNS (A3D_vrbls, 'T'  , v)                                                                         !
  case (4); call xtrct_dim1_from_A3DvNS (A3D_vrbls, 'Vx' , v)                                                                         !
  case (5); call xtrct_dim1_from_A3DvNS (A3D_vrbls, 'Vy' , v)                                                                         !
  end select                                                                                                                          !
!-----------------------------------------------------------------------                                                              !
  call       R3D_2D_lsq_R1_WENO ( k_sch, k_srdr, A3D_UGDS_dgs%ndg_nlmnt, A3D_UGDS_dgs%dg_data, A3D_UGR1_2D_cells%nstncl,&             !
                                  A3D_UGDS_cells%x_vrtx, A3D_UGDS_cells%x_lmnt, A3D_UGR1_2D_cells%p_R1_coeffs,          &             !
                                  A3D_UGR1_2D_cells%dx_lmnt, A3D_UGR1_2D_cells%Gpnt_xyzw, v, v_R, v_L)                                !
! write(*,*) '---->', allocated(v_R), allocated(v_L)                                                                                  !
  if (nvrbl==3)                                                                                                                 &     !
  call       R3D_2D_lsq_R1_WENO_1drvtvs ( k_sch, k_srdr, A3D_UGDS_dgs%ndg_nlmnt, A3D_UGDS_dgs%dg_data, A3D_UGR1_2D_cells%nstncl,&     !
                                          A3D_UGDS_cells%x_vrtx, A3D_UGDS_cells%x_lmnt, A3D_UGR1_2D_cells%p_R1_coeffs,          &     !
                                          A3D_UGR1_2D_cells%dx_lmnt, A3D_UGR1_2D_cells%Gpnt_xyzw, v, dTd_R, dTd_L)                    !
  if (nvrbl==4)                                                                                                                 &     !
  call       R3D_2D_lsq_R1_WENO_1drvtvs ( k_sch, k_srdr, A3D_UGDS_dgs%ndg_nlmnt, A3D_UGDS_dgs%dg_data, A3D_UGR1_2D_cells%nstncl,&     !
                                          A3D_UGDS_cells%x_vrtx, A3D_UGDS_cells%x_lmnt, A3D_UGR1_2D_cells%p_R1_coeffs,          &     !
                                          A3D_UGR1_2D_cells%dx_lmnt, A3D_UGR1_2D_cells%Gpnt_xyzw, v, dud_R, dud_L)                    !
  if (nvrbl==5)                                                                                                                 &     !
  call       R3D_2D_lsq_R1_WENO_1drvtvs ( k_sch, k_srdr, A3D_UGDS_dgs%ndg_nlmnt, A3D_UGDS_dgs%dg_data, A3D_UGR1_2D_cells%nstncl,&     !
                                          A3D_UGDS_cells%x_vrtx, A3D_UGDS_cells%x_lmnt, A3D_UGR1_2D_cells%p_R1_coeffs,          &     !
                                          A3D_UGR1_2D_cells%dx_lmnt, A3D_UGR1_2D_cells%Gpnt_xyzw, v, dvd_R, dvd_L)                    !
!-----------------------------------------------------------------------                                                              !
  select case (nvrbl)                                                                                                                 !
  case (1); call put_dim1_to_A3DvNS_vn0 (A3D_vrbls_L, 'p'  , v_L); call put_dim1_to_A3DvNS_vn0 (A3D_vrbls_R, 'p'  , v_R)              !
  case (2); call put_dim1_to_A3DvNS_vn0 (A3D_vrbls_L, 'rho', v_L); call put_dim1_to_A3DvNS_vn0 (A3D_vrbls_R, 'rho', v_R)              !
  case (3); call put_dim1_to_A3DvNS_vn0 (A3D_vrbls_L, 'T'  , v_L); call put_dim1_to_A3DvNS_vn0 (A3D_vrbls_R, 'T'  , v_R)              !
  case (4); call put_dim1_to_A3DvNS_vn0 (A3D_vrbls_L, 'Vx' , v_L); call put_dim1_to_A3DvNS_vn0 (A3D_vrbls_R, 'Vx' , v_R)              !
  case (5); call put_dim1_to_A3DvNS_vn0 (A3D_vrbls_L, 'Vy' , v_L); call put_dim1_to_A3DvNS_vn0 (A3D_vrbls_R, 'Vy' , v_R)              !
  end select                                                                                                                          !
!-----------------------------------------------------------------------                                                              !
  end do!-----------------------------------------------------------------------------------------------------------------------------
!-----------------------------------------------------------------------------------------------------------------------------------
!-   -------------------------------------------------------------------------------------------------------------------------------
!-ARS-------------------------------------------------------------------------------------------------------------------------------
!-   -------------------------------------------------------------------------------------------------------------------------------
!-----------------------------------------------------------------------------------------------------------------------------------
  if(allocated(A3D_Rvs)) deallocate(A3D_Rvs); allocate(A3D_Rvs(Nlmnts))
!$OMP PARALLEL WORKSHARE DEFAULT(PRIVATE) SHARED(A3D_Rvs, Nlmnts)                                                                 !-|| A3D_sch_NS_2D
  A3D_Rvs(1:Nlmnts:1)%rho   = 0.
  A3D_Rvs(1:Nlmnts:1)%rhoV%x= 0.
  A3D_Rvs(1:Nlmnts:1)%rhoV%y= 0.
  A3D_Rvs(1:Nlmnts:1)%rhoV%z= 0.
  A3D_Rvs(1:Nlmnts:1)%rhoet = 0.
!$OMP END PARALLEL WORKSHARE                                                                                                      !-|| A3D_sch_NS_2D
!-----------------------------------------------------------------------
!-                               ---------------------------------------
!-HACK_to_use_legacy_R3D_routines---------------------------------------
!-                               ---------------------------------------
!-----------------------------------------------------------------------
  N_R_s = 0
  N_L_s = 0
  N_G_s = 0
!$OMP PARALLEL DO SCHEDULE(STATIC) DEFAULT(SHARED) PRIVATE( ndg) REDUCTION( + :N_G_s, N_L_s, N_R_s)                               !-|| A3D_sch_NS_2D
             do ndg = 1, Ndgs, 1; N_G_s = N_G_s + size(A3D_UGR1_2D_cells%Gpnt_xyzw(ndg)%xyzGp)
                                  N_L_s = N_L_s + size(                        v_L(ndg)%v    )
                                  N_R_s = N_R_s + size(                        v_R(ndg)%v    ); end do
!$OMP END PARALLEL DO                                                                                                             !-|| A3D_sch_NS_2D
! write(*,*) 'NGLRs:: ', N_G_s, N_L_s, N_R_s
  deallocate (v  )
  deallocate (v_L)
  deallocate (v_R)
  allocate(vL   (N_G_s))
  allocate(vR   (N_G_s))
  allocate(xn   (N_G_s))
  allocate(xArea(N_G_s))
  allocate(F    (N_G_s))
  allocate(S_max(N_G_s))
  allocate(xnueq(N_G_s))
  allocate(nGp  (Ndgs ))
!-SEQUENTIAL------------------------------------------------------------
  n=0; do ndg = 1, Ndgs, 1; NGs_dg = size(A3D_UGR1_2D_cells%Gpnt_xyzw(ndg)%wGp); allocate(nGp(ndg)%n(NGs_dg))                     !-|| cannot?<--A3D
       do ind = 1, NGs_dg, 1; n = n+1; nGp(ndg)%n(ind) = n; end do; end do                                                        !-|| cannot?<--A3D
!-----------------------------------------------------------------------
!$OMP PARALLEL DO SCHEDULE(STATIC) DEFAULT(PRIVATE)                                                                     &         !-|| A3D_sch_NS_2D
!$OMP                              SHARED( Ndgs, A3D_UGR1_2D_cells, nGp, xArea, xn, vL, vR, A3D_vrbls_L, A3D_vrbls_R)             !-|| A3D_sch_NS_2D
  do ndg = 1, Ndgs, 1; NGs_dg = size(A3D_UGR1_2D_cells%Gpnt_xyzw(ndg)%wGp)! allocate(nGp(ndg)%n(NGs_dg))
  do ind = 1, NGs_dg, 1! n = n+1                                                                                  !-HACK::update
! nGp(ndg)%n(ind) = n                                                                                             !-HACK::update
  n = nGp(ndg)%n(ind)                                                                                             !-HACK::update
  xArea(n) = sqrt(A3D_UGR1_2D_cells%Gpnt_xyzw(ndg)%dSGp(ind)%x**2+A3D_UGR1_2D_cells%Gpnt_xyzw(ndg)%dSGp(ind)%y**2)!-HACK::update
  xn(n)%x = A3D_UGR1_2D_cells%Gpnt_xyzw(ndg)%dSGp(ind)%x/xArea(n)                                                 !-HACK::update
  xn(n)%y = A3D_UGR1_2D_cells%Gpnt_xyzw(ndg)%dSGp(ind)%y/xArea(n)                                                 !-HACK::update
  xn(n)%z = 0.                                                                                                    !-HACK::update
  vL(n)%v_rho = A3D_vrbls_L(ndg)%rho%v(ind)  ; vR(n)%v_rho = A3D_vrbls_R(ndg)%rho%v(ind)                          !-HACK::update
  vL(n)%v_V_x = A3D_vrbls_L(ndg)%V%xyz(ind)%x; vR(n)%v_V_x = A3D_vrbls_R(ndg)%V%xyz(ind)%x                        !-HACK::update
  vL(n)%v_V_y = A3D_vrbls_L(ndg)%V%xyz(ind)%y; vR(n)%v_V_y = A3D_vrbls_R(ndg)%V%xyz(ind)%y                        !-HACK::update
  vL(n)%v_V_z = 0.                           ; vR(n)%v_V_z = 0.                                                   !-HACK::update
  vL(n)%v_p   = A3D_vrbls_L(ndg)%p%v  (ind)  ; vR(n)%v_p   = A3D_vrbls_R(ndg)%p%v  (ind)                          !-HACK::update
  end do; end do                                                                                                  !-HACK::update
!$OMP END PARALLEL DO                                                                                                             !-|| A3D_sch_NS_2D
!-----------------------------------------------------------------------------------------------------------------------------------
!-         -------------------------------------------------------------------------------------------------------------------------
!-apply_BCs-------------------------------------------------------------------------------------------------------------------------
!-         -------------------------------------------------------------------------------------------------------------------------
!-----------------------------------------------------------------------------------------------------------------------------------
!
!-----------------------------------------------------------------------------------------------------------------------------------
!-              --------------------------------------------------------------------------------------------------------------------
!-Riemann_solver--------------------------------------------------------------------------------------------------------------------
!-              --------------------------------------------------------------------------------------------------------------------
!-----------------------------------------------------------------------------------------------------------------------------------
  if (A3D_runtime_NS2D%k_RS==0) call R3D_Euler_RS_1D_n_Gdnv_flux ( 1, N_G_s, vL, vR, F, xn, A3D_thrm_data%gamma, S_max, A3Dr_mssgs)
  if (A3D_runtime_NS2D%k_RS==1) call R3D_sch_3D_ARS_n            ( 1, N_G_s, vL, vR, F, xn, A3D_thrm_data%gamma, S_max, A3Dr_mssgs)
!-----------------------------------------------------------------------------------------------------------------------------------
!-              --------------------------------------------------------------------------------------------------------------------
!-FdS_convective--------------------------------------------------------------------------------------------------------------------
!-              --------------------------------------------------------------------------------------------------------------------
!-----------------------------------------------------------------------------------------------------------------------------------
  allocate(FdS(Ndgs))
!$OMP PARALLEL DO SCHEDULE(STATIC) DEFAULT(PRIVATE) SHARED( Ndgs,A3D_UGR1_2D_cells, F, FdS, nGp)                                  !-|| A3D_sch_NS_2D
  do ndg = 1, Ndgs, 1!---------------------------------------------------------------------------------
  NGs_dg = size(A3D_UGR1_2D_cells%Gpnt_xyzw(ndg)%wGp); FdS(ndg)%rho   = 0.                             !
                                                       FdS(ndg)%rhoV%x= 0.                             !
                                                       FdS(ndg)%rhoV%y= 0.                             !
                                                       FdS(ndg)%rhoV%z= 0.                             !
                                                       FdS(ndg)%rhoet = 0.                             !
  do ind = 1, NGs_dg, 1; wGdS = A3D_UGR1_2D_cells%Gpnt_xyzw(ndg)% wGp(ind)*     &!---------            !
                           sqrt(A3D_UGR1_2D_cells%Gpnt_xyzw(ndg)%dSGp(ind)%x**2+&          !           !
                                A3D_UGR1_2D_cells%Gpnt_xyzw(ndg)%dSGp(ind)%y**2)           !           !
                     FdS(ndg)%rho   =FdS(ndg)%rho   +wGdS*F(nGp(ndg)%n(ind))%v_0           !           !<-HACK::define_interface_operator<-A3D::TODO
                     FdS(ndg)%rhoV%x=FdS(ndg)%rhoV%x+wGdS*F(nGp(ndg)%n(ind))%v_1           !           !<-HACK::define_interface_operator<-A3D::TODO
                     FdS(ndg)%rhoV%y=FdS(ndg)%rhoV%y+wGdS*F(nGp(ndg)%n(ind))%v_2           !           !<-HACK::define_interface_operator<-A3D::TODO
                     FdS(ndg)%rhoet =FdS(ndg)%rhoet +wGdS*F(nGp(ndg)%n(ind))%v_4; end do!<-            !<-HACK::define_interface_operator<-A3D::TODO
  end do!<---------------------------------------------------------------------------------------------
!$OMP END PARALLEL DO                                                                                                             !-|| A3D_sch_NS_2D
  deallocate(F)
!-----------------------------------------------------------------------------------------------------------------------------------
!-              --------------------------------------------------------------------------------------------------------------------
!-+FdS_diffusive--------------------------------------------------------------------------------------------------------------------
!-              --------------------------------------------------------------------------------------------------------------------
!-----------------------------------------------------------------------------------------------------------------------------------
!$OMP PARALLEL DO SCHEDULE(STATIC) DEFAULT(PRIVATE)                                                                     &         !-|| A3D_sch_NS_2D
!$OMP                              SHARED( Ndgs, A3D_UGR1_2D_cells, A3D_vrbls_L, A3D_vrbls_R, xnueq, FdS,               &         !-|| A3D_sch_NS_2D
!$OMP                                      A3D_thrm_data, dTd_L, dTd_R, dud_L, dud_R, dvd_L, dvd_R, nGp, vL, vR)                  !-|| A3D_sch_NS_2D
  do ndg = 1, Ndgs, 1!---------------------------------------------------------------------------------
  NGs_dg = size(A3D_UGR1_2D_cells%Gpnt_xyzw(ndg)%wGp)
  do ind = 1, NGs_dg, 1; n = nGp(ndg)%n(ind)!------------------------------------------------          !
  T = 0.5*(vL(n)%v_p/A3D_thrm_data%Rg/vL(n)%v_rho&                                           !         !
          +vR(n)%v_p/A3D_thrm_data%Rg/vR(n)%v_rho)                                           !         !
  vsc=A3D_Stdvsc( T, A3D_thrm_data)! write(*,*) T, vsc%xmu,vsc%xmub,vsc%xlb                  !         !
  q%x=-vsc%xlb*0.5*(dTd_L(ndg)%xyz(ind)%x+dTd_R(ndg)%xyz(ind)%x)                             !         !
  q%y=-vsc%xlb*0.5*(dTd_L(ndg)%xyz(ind)%y+dTd_R(ndg)%xyz(ind)%y)                             !         !
!-----------------------------------------------------------------------                     !         !
  xnueq(nGp(ndg)%n(ind))=max(4./3.*vsc%xmu,                                   &              !         ! XNUEQ(N)=AMAX1(4./3.*(1.+XMTRT   )*XMU(N)        ,&
                            vsc%xlb/A3D_thrm_data%Rg*(A3D_thrm_data%gamma-1.))&              !         !                      (XKT(N)+XKTURB)/RG*(GAM-1.) )/ V0(N)
                       /((A3D_vrbls_L(ndg)%rho%v(ind)+A3D_vrbls_R(ndg)%rho%v(ind))/2.)       !         !
!-----------------------------------------------------------------------                     !         !
  gradV%xx=0.5*(dud_L(ndg)%xyz(ind)%x+dud_R(ndg)%xyz(ind)%x)                                 !         !
  gradV%xy=0.5*(dud_L(ndg)%xyz(ind)%y+dud_R(ndg)%xyz(ind)%y)                                 !         !
  gradV%xz=0.                                                                                !         !
  gradV%yx=0.5*(dvd_L(ndg)%xyz(ind)%x+dvd_R(ndg)%xyz(ind)%x)                                 !         !
  gradV%yy=0.5*(dvd_L(ndg)%xyz(ind)%y+dvd_R(ndg)%xyz(ind)%y)                                 !         !
  gradV%yz=0.                                                                                !         !
  gradV%zx=0.                                                                                !         !
  gradV%zy=0.                                                                                !         !
  gradV%zz=0.                                                                                !         !
  S=.sym.gradV; tau=2.*vsc%xmu*(.dev.S)+vsc%xmub*(.trc.S)*Idtnsr2(S)                         !         !
  Vlct%x =0.5*(vL(n)%v_V_x+vR(n)%v_V_x)                                                      !         !
  Vlct%y =0.5*(vL(n)%v_V_y+vR(n)%v_V_y)                                                      !         !
  Vlct%z =0.                                                                                 !         !
  wdSdg%x = A3D_UGR1_2D_cells%Gpnt_xyzw(ndg)%wGp(ind)*A3D_UGR1_2D_cells%Gpnt_xyzw(ndg)%dSGp(ind)%x     !
  wdSdg%y = A3D_UGR1_2D_cells%Gpnt_xyzw(ndg)%wGp(ind)*A3D_UGR1_2D_cells%Gpnt_xyzw(ndg)%dSGp(ind)%y     !
  wdSdg%z = 0.                                                                               !         !
  FdS(ndg)%rhoV%x=FdS(ndg)%rhoV%x-(tau%xx*wdSdg%x+tau%xy*wdSdg%y)                            !         !
  FdS(ndg)%rhoV%y=FdS(ndg)%rhoV%y-(tau%xy*wdSdg%x+tau%yy*wdSdg%y)                            !         !
  VtaudS= (Vlct%x*tau%xx+Vlct%y*tau%xy)*wdSdg%x+(Vlct%x*tau%xy+Vlct%y*tau%yy)*wdSdg%y        !         !
  FdS(ndg)%rhoet =FdS(ndg)%rhoet + (wdSdg%x*q%x+wdSdg%y*q%y-VtaudS)                          !         !
                                                                             end do!<--------          !-HACK::update
  end do!<---------------------------------------------------------------------------------------------
!-----------------------------------------------------------------------------------------------------------------------------------
!-   -------------------------------------------------------------------------------------------------------------------------------
!-Rvs-------------------------------------------------------------------------------------------------------------------------------
!-   -------------------------------------------------------------------------------------------------------------------------------
!-----------------------------------------------------------------------------------------------------------------------------------
!$OMP END PARALLEL DO                                                                                                             !-|| A3D_sch_NS_2D
!$OMP PARALLEL DO SCHEDULE(STATIC) DEFAULT(PRIVATE) SHARED( Nlmnts, A3D_UGDS_dgs, A3D_UGDS_cells, FdS, A3D_Rvs)                   !-|| A3D_sch_NS_2D
  do nlmnt = 1, Nlmnts, 1; Ndgs_lmnt = size(A3D_UGDS_dgs%ndg_nlmnt(nlmnt)%n)!--------------------------
  do ind = 1, Ndgs_lmnt, 1; ndg=A3D_UGDS_dgs%ndg_nlmnt(nlmnt)%n(ind)!---------------------             !
                            nlmnt1  =A3D_UGDS_dgs%dg_data(ndg)%n_element_1                !            !
                            nlmnt2  =A3D_UGDS_dgs%dg_data(ndg)%n_element_2                !            !
  if(nlmnt==nlmnt1) A3D_Rvs(nlmnt)=A3D_Rvs(nlmnt)+FdS(ndg)                                !            !
  if(nlmnt==nlmnt2) A3D_Rvs(nlmnt)=A3D_Rvs(nlmnt)-FdS(ndg)                                !            !
  end do!<--------------------------------------------------------------------------------             !
  A3D_Rvs(nlmnt)%rho    = A3D_Rvs(nlmnt)%rho   /A3D_UGDS_cells%A_lmnt(nlmnt)
  A3D_Rvs(nlmnt)%rhoV%x = A3D_Rvs(nlmnt)%rhoV%x/A3D_UGDS_cells%A_lmnt(nlmnt)
  A3D_Rvs(nlmnt)%rhoV%y = A3D_Rvs(nlmnt)%rhoV%y/A3D_UGDS_cells%A_lmnt(nlmnt)
  A3D_Rvs(nlmnt)%rhoet  = A3D_Rvs(nlmnt)%rhoet /A3D_UGDS_cells%A_lmnt(nlmnt)
  end do!<---------------------------------------------------------------------------------------------
!$OMP END PARALLEL DO                                                                                                             !-|| A3D_sch_NS_2D
  deallocate(FdS)
!-----------------------------------------------------------------------------------------------------------------------------------
!-     -----------------------------------------------------------------------------------------------------------------------------
!-dtlcl-----------------------------------------------------------------------------------------------------------------------------
!-     -----------------------------------------------------------------------------------------------------------------------------
!-----------------------------------------------------------------------------------------------------------------------------------
  if(allocated(dtlcl))   deallocate(dtlcl)  ; allocate (dtlcl(Nlmnts))
  if(allocated(xnueqdg)) deallocate(xnueqdg); allocate (xnueqdg(Ndgs))
  if(allocated(Smaxdg))  deallocate(Smaxdg) ; allocate ( Smaxdg(Ndgs))
!$OMP PARALLEL DO SCHEDULE(STATIC) DEFAULT(PRIVATE)                                                                     &         !-|| A3D_sch_NS_2D
!$OMP                              SHARED( Ndgs, A3D_UGR1_2D_cells, nGp, S_max, xnueq, Smaxdg, xnueqdg)                           !-|| A3D_sch_NS_2D
  do ndg = 1, Ndgs, 1; NGs_dg = size(A3D_UGR1_2D_cells%Gpnt_xyzw(ndg)%wGp); allocate (fwrk(NGs_dg))!----
  do ind  = 1,  NGs_dg, 1; fwrk(ind) = S_max(nGp(ndg)%n(ind)); end do;  Smaxdg(ndg) = maxval(fwrk)      !
  do ind  = 1,  NGs_dg, 1; fwrk(ind) = xnueq(nGp(ndg)%n(ind)); end do; xnueqdg(ndg) = maxval(fwrk)      !
  deallocate(fwrk); end do!<----------------------------------------------------------------------------
!$OMP END PARALLEL DO                                                                                                             !-|| A3D_sch_NS_2D
! write(*,*) '!!!!=====>!!!!'
!$OMP PARALLEL DO SCHEDULE(STATIC) DEFAULT(PRIVATE)                                                                     &         !-|| A3D_sch_NS_2D
!$OMP                              SHARED( Nlmnts, A3D_UGDS_dgs, Smaxdg, xnueqdg, dtlcl, A3D_UGDS_cells)                          !-|| A3D_sch_NS_2D
  do nlmnt = 1, Nlmnts, 1; Ndgs_nlmnt = size(A3D_UGDS_dgs%ndg_nlmnt(nlmnt)%n); allocate (fwrk(Ndgs_nlmnt))!-----------------------------
                           do ind = 1, Ndgs_nlmnt, 1; ndg = A3D_UGDS_dgs%ndg_nlmnt(nlmnt)%n(ind); fwrk(ind) =  Smaxdg(ndg); end do      !
                           Smaxlmnt = maxval(fwrk)                                                                                      !
                           do ind = 1, Ndgs_nlmnt, 1; ndg = A3D_UGDS_dgs%ndg_nlmnt(nlmnt)%n(ind); fwrk(ind) = xnueqdg(ndg); end do      !
                           xnueqlmnt = maxval(fwrk)                                                                                     !
                           deallocate(fwrk)                                                                                             !
! write(*,*) nlmnt, Smaxlmnt, A3D_UGDS_cells%d_lmnt(nlmnt)/Smaxlmnt, A3D_UGDS_cells%d_lmnt(nlmnt)**2/xnueqlmnt/2.,&                     !
!                             A3D_UGDS_cells%A_lmnt(nlmnt), A3D_UGDS_cells%d_lmnt(nlmnt)                                                !
  dtlcl(nlmnt)=min(A3D_UGDS_cells%d_lmnt(nlmnt)/Smaxlmnt, A3D_UGDS_cells%d_lmnt(nlmnt)**2/xnueqlmnt/2.)                                 !
  end do!<------------------------------------------------------------------------------------------------------------------------------
!$OMP END PARALLEL DO                                                                                                             !-|| A3D_sch_NS_2D
!   deallocate (Smaxdg)
!   deallocate (xnueqdg)
!------------!$OMP PARALLEL WORKSHARE DEFAULT(PRIVATE) SHARED(A3D_Rvs, A3D_UGDS_cells, Nlmnts)                                                 !-|| A3D_sch_NS_2D
!------------  A3D_Rvs(1:Nlmnts:1)%rho    = A3D_Rvs(1:Nlmnts:1)%rho   /A3D_UGDS_cells%A_lmnt(1:Nlmnts:1)
!------------  A3D_Rvs(1:Nlmnts:1)%rhoV%x = A3D_Rvs(1:Nlmnts:1)%rhoV%x/A3D_UGDS_cells%A_lmnt(1:Nlmnts:1)
!------------  A3D_Rvs(1:Nlmnts:1)%rhoV%y = A3D_Rvs(1:Nlmnts:1)%rhoV%y/A3D_UGDS_cells%A_lmnt(1:Nlmnts:1)
!------------  A3D_Rvs(1:Nlmnts:1)%rhoet  = A3D_Rvs(1:Nlmnts:1)%rhoet /A3D_UGDS_cells%A_lmnt(1:Nlmnts:1)
!------------!$OMP END PARALLEL WORKSHARE                                                                                                      !-|| A3D_sch_NS_2D
!-----------------------------------------------------------------------------------------------------------------------------------
!-          ------------------------------------------------------------------------------------------------------------------------
!-deallocate------------------------------------------------------------------------------------------------------------------------
!-          ------------------------------------------------------------------------------------------------------------------------
!-----------------------------------------------------------------------------------------------------------------------------------
  deallocate(vL   )
  deallocate(vR   )
  deallocate(xn   )
  deallocate(xArea)
  if(allocated(F)) deallocate(F    )
  deallocate(S_max)!<-use_in_dtstab::TODO
  deallocate(A3D_vrbls_L)
  deallocate(A3D_vrbls_R)
  deallocate(xnueq)
  deallocate(nGp  )
  deallocate(dTd_L)
  deallocate(dTd_R)
  deallocate(dud_L)
  deallocate(dud_R)
  deallocate(dvd_L)
  deallocate(dvd_R)
  deallocate(npwrs)
! deallocate(nc_npwrs)
!-----------------------------------------------------------------------------------------------------------------------------------
!----->  write(*,*) 'v          ',allocated(v          )
!----->  write(*,*) 'v_L        ',allocated(v_L        )
!----->  write(*,*) 'v_R        ',allocated(v_R        )
!----->  write(*,*) 'A3D_vrbls_L',allocated(A3D_vrbls_L)
!----->  write(*,*) 'A3D_vrbls_R',allocated(A3D_vrbls_R)
!----->  write(*,*) 'xn         ',allocated(xn         )
!----->  write(*,*) 'xArea      ',allocated(xArea      )
!----->  write(*,*) 'vL         ',allocated(vL         )
!----->  write(*,*) 'vR         ',allocated(vR         )
!----->  write(*,*) 'F          ',allocated(F          )
!----->  write(*,*) 'S_max      ',allocated(S_max      )
!----->  write(*,*) 'nGp        ',allocated(nGp        )
!----->  write(*,*) 'Smaxdg     ',allocated(Smaxdg     )
!----->  write(*,*) 'xnueq      ',allocated(xnueq      )
!----->  write(*,*) 'xnueqdg    ',allocated(xnueqdg    )
!----->  write(*,*) 'fwrk       ',allocated(fwrk       )
!----->  write(*,*) 'gwrk       ',allocated(gwrk       )
!----->! write(*,*) 'npwrs      ',allocated(npwrs      )
!----->! write(*,*) 'npwrsd     ',allocated(npwrsd     )
!----->! write(*,*) 'drvtvrule  ',allocated(drvtvrule  )
!----->  write(*,*) 'dTd_L      ',allocated(dTd_L      )
!----->  write(*,*) 'dTd_R      ',allocated(dTd_R      )
!----->  write(*,*) 'dud_L      ',allocated(dud_L      )
!----->  write(*,*) 'dud_R      ',allocated(dud_R      )
!----->  write(*,*) 'dvd_L      ',allocated(dvd_L      )
!----->  write(*,*) 'dvd_R      ',allocated(dvd_R      )
!-----------------------------------------------------------------------------------------------------------------------------------
!-                            ------------------------------------------------------------------------------------------------------
!-                            ------------------------------------------------------------------------------------------------------
!-                            ------------------------------------------------------------------------------------------------------
!-                            ------------------------------------------------------------------------------------------------------
!-                            ------------------------------------------------------------------------------------------------------
!-end subroutine A3D_sch_NS_2D------------------------------------------------------------------------------------------------------
!-                            ------------------------------------------------------------------------------------------------------
!-                            ------------------------------------------------------------------------------------------------------
!-                            ------------------------------------------------------------------------------------------------------
!-                            ------------------------------------------------------------------------------------------------------
!-                            ------------------------------------------------------------------------------------------------------
!-----------------------------------------------------------------------------------------------------------------------------------
  end subroutine A3D_sch_NS_2D
  subroutine A3D_err_NS_2D ( uold, unew, A3D_mssgs, error_u, isreallmnt)                                                     !-|| parallel_OpenMP
!-----------------------------------------------------------------------------------------------------------------------------------
!-                                                                                              ------------------------------------
!-subroutine A3D_err_NS_2D:                                                                     ------------------------------------
!-                                                                                              ------------------------------------
!-                                                                                              ------------------------------------
!-                                                                                              ------------------------------------
!-                                                                                              ------------------------------------
!-                                                                                              ------------------------------------
!-                                                                                              ------------------------------------
!-                                                                                              ------------------------------------
!-                                                                                              ------------------------------------
!-                                                                                              ------------------------------------
!-----------------------------------------------------------------------------------------------------------------------------------
!
!-----------------------------------------------------------------------
!-    ------------------------------------------------------------------
!-type------------------------------------------------------------------
!-    ------------------------------------------------------------------
!-----------------------------------------------------------------------
!-p?-!$ use OMP_LIB
  use A3D_vrbls_types
!-----------------------------------------------------------------------
  implicit none
!-----------------------------------------------------------------------
!-         -------------------------------------------------------------
!-interface-------------------------------------------------------------
!-         -------------------------------------------------------------
!-----------------------------------------------------------------------
  type(A3DuNS        ), dimension(:)          , allocatable, intent(in   ) :: uold
  type(A3DuNS        ), dimension(:)          , allocatable, intent(in   ) :: unew
  type(R3Dmssgs      )                                     , intent(in   ) :: A3D_mssgs
  logical             , dimension(:), optional, allocatable, intent(in   ) :: isreallmnt
  real                                                     , intent(  out) :: error_u
!-----------------------------------------------------------------------
!-               -------------------------------------------------------
!-local_variables-------------------------------------------------------
!-               -------------------------------------------------------
!-----------------------------------------------------------------------
  integer                                                        :: n, Nps
  real                                                           :: du_rho_2, du_rhoV_2, du_rhoet_2, u_rho_2, u_rhoV_2, u_rhoet_2
!-----------------------------------------------------------------------
!-             ---------------------------------------------------------
!-A3D_err_NS_2D---------------------------------------------------------
!-             ---------------------------------------------------------
!-----------------------------------------------------------------------
  Nps = size(uold)
!-----------------------------------------------------------------------
  du_rho_2   = 0.; u_rho_2   = 0.
  du_rhoV_2  = 0.; u_rhoV_2  = 0.
  du_rhoet_2 = 0.; u_rhoet_2 = 0.
!-----------------------------------------------------------------------------------------------------------------------------------||
  if (present(isreallmnt)) then
!$OMP PARALLEL DO SCHEDULE(STATIC) DEFAULT(SHARED) PRIVATE(n)                                 &                                   !-||
!$OMP             REDUCTION( + :du_rho_2, du_rhoV_2, du_rhoet_2, u_rho_2, u_rhoV_2, u_rhoet_2)                                    !-||
  do n = 1, Nps, 1; if (isreallmnt(n)) then
  u_rho_2    = u_rho_2   + uold(n)%rho**2
  u_rhoV_2   = u_rhoV_2  + uold(n)%rhoV%x**2 + uold(n)%rhoV%y**2
  u_rhoet_2  = u_rhoet_2 + uold(n)%rhoet**2
  du_rho_2   = du_rho_2  +(unew(n)%rho   -uold(n)%rho   )**2
  du_rhoV_2  = du_rhoV_2 +(unew(n)%rhoV%x-uold(n)%rhoV%x)**2&
                         +(unew(n)%rhoV%y-uold(n)%rhoV%y)**2
  du_rhoet_2 = du_rhoet_2+(unew(n)%rhoet -uold(n)%rhoet )**2
  end if; end do
!$OMP END PARALLEL DO                                                                                                             !-||
  else   
!$OMP PARALLEL DO SCHEDULE(STATIC) DEFAULT(SHARED) PRIVATE(n)                                 &                                   !-||
!$OMP             REDUCTION( + :du_rho_2, du_rhoV_2, du_rhoet_2, u_rho_2, u_rhoV_2, u_rhoet_2)                                    !-||
  do n = 1, Nps, 1
  u_rho_2    = u_rho_2   + uold(n)%rho**2
  u_rhoV_2   = u_rhoV_2  + uold(n)%rhoV%x**2 + uold(n)%rhoV%y**2
  u_rhoet_2  = u_rhoet_2 + uold(n)%rhoet**2
  du_rho_2   = du_rho_2  +(unew(n)%rho   -uold(n)%rho   )**2
  du_rhoV_2  = du_rhoV_2 +(unew(n)%rhoV%x-uold(n)%rhoV%x)**2&
                         +(unew(n)%rhoV%y-uold(n)%rhoV%y)**2
  du_rhoet_2 = du_rhoet_2+(unew(n)%rhoet -uold(n)%rhoet )**2
  end do
!$OMP END PARALLEL DO                                                                                                             !-||
  end if
!-----------------------------------------------------------------------------------------------------------------------------------||
  error_u = log10(sqrt((du_rho_2/u_rho_2+du_rhoV_2/u_rhoV_2+du_rhoet_2/u_rhoet_2)/4.))
!-----------------------------------------------------------------------------------------------------------------------------------
!-                            ------------------------------------------------------------------------------------------------------
!-end subroutine A3D_err_NS_2D------------------------------------------------------------------------------------------------------
!-                            ------------------------------------------------------------------------------------------------------
!-----------------------------------------------------------------------------------------------------------------------------------
  end subroutine A3D_err_NS_2D
  subroutine A3D_sch_AD_2D ( A3Dr_mssgs, A3D_runtime_NS2D, A3D_thrm_data,&
                             A3D_UGDS_cells, A3D_UGDS_dgs, A3D_UGR1_2D_cells, v, divF)
!-----------------------------------------------------------------------------------------------------------------------------------
!-                                                                                                                                --
!-subroutine A3D_sch_AD_2D                                                                                                        --
!-                                                                                                                                --
!-                                                                                                                                --
!-                                                                                                                                --
!-                                                                                                                                --
!-                                                                                                                                --
!-                                                                                                                                --
!-                                                                                                                                --
!-                                                                                                                                --
!-                                                                                                                                --
!-----------------------------------------------------------------------------------------------------------------------------------
!
!-----------------------------------------------------------------------
!-     -----------------------------------------------------------------
!-types-----------------------------------------------------------------
!-     -----------------------------------------------------------------
!-----------------------------------------------------------------------
!$ use OMP_LIB
   use A3D_thrm
   use A3D_mssgs
   use A3D_types
   use A3D_vrbls_types
   use R3D_types
   use A3D_data_structure
   use A3D_tensors
   use A3Dg_2D_routines
   use R3D_2D_lsq_WENO
   use R3D_to_plots_routines
   use A3D_RS_ARS
!--for_subroutine_divtauq-----------------------------------------------
   use R3D_maths
!-----------------------------------------------------------------------
  implicit none
!-----------------------------------------------------------------------
!-         -------------------------------------------------------------
!-interface-------------------------------------------------------------
!-         -------------------------------------------------------------
!-----------------------------------------------------------------------
  type(R3Dmssgs              ),                            intent(inout) :: A3Dr_mssgs
  type(A3DruntimeprmtrsNS2D  ),                            intent(inout) :: A3D_runtime_NS2D
  type(A3DthrmdataBSL        ),                            intent(in   ) :: A3D_thrm_data
  type(A3DUGDS2Dplgnstplg    ),                            intent(in   ) :: A3D_UGDS_cells
  type(A3DUGDS2Dplgnsdgsvrtcs),                            intent(in   ) :: A3D_UGDS_dgs
  type(A3DUGR12Dcells        ),                            intent(in   ) :: A3D_UGR1_2D_cells
       real                   , dimension(:), allocatable, intent(in   ) :: v
       real                   , dimension(:), allocatable, intent(inout) :: divF
!-----------------------------------------------------------------------
!-               -------------------------------------------------------
!-local_variables-------------------------------------------------------
!-               -------------------------------------------------------
!-----------------------------------------------------------------------
  type(R3Dvn0         ), dimension(:), allocatable                :: v_L
  type(R3Dvn0         ), dimension(:), allocatable                :: v_R
  integer, dimension(:,:), allocatable :: npwrs
!-----------------------------------------------------------------------
  integer      :: Ndgs, Nvrtcs, Ncells, Nlmnts, k_sch, k_srdr, ndg, np1, np2, nlmnt, nlmnt1, nlmnt2, NGs_dg, ind
  real         :: FdS_12, Fx, Fy, wGauss, dS_x, dS_y
!-----------------------------------------------------------------------------------------------------------------------------------
!-     -----------------------------------------------------------------------------------------------------------------------------
!-sizes-----------------------------------------------------------------------------------------------------------------------------
!-     -----------------------------------------------------------------------------------------------------------------------------
!-----------------------------------------------------------------------------------------------------------------------------------
  Ndgs  = size(A3D_UGDS_dgs%dg_data )
  Nvrtcs= size(A3D_UGDS_cells%x_vrtx)
  Ncells= size(A3D_UGDS_cells%x_lmnt); Nlmnts= Ncells
! write(*,*) 'A3D_sch_NS_2D ::', Ndgs, Nvrtcs, Ncells, size(A3D_UGR1_2D_cells%Gpnt_xyzw) !<-write::DELETE
!-----------------------------------------------------------------------------------------------------------------------------------
!-  --------------------------------------------------------------------------------------------------------------------------------
!-R1--------------------------------------------------------------------------------------------------------------------------------
!-  --------------------------------------------------------------------------------------------------------------------------------
!-----------------------------------------------------------------------------------------------------------------------------------
! k_srdr=2; if (.not.allocated(divF)) allocate(divF(Nlmnts))
  k_srdr=A3D_runtime_NS2D%k_srdr! write(*,*) ' A3D_sch_AD_2D: k_srdr = ', k_srdr
  if (.not.allocated(divF)) allocate(divF(Nlmnts))
  call R3D_polynomial_powers ( 2, k_srdr-1, npwrs, 0)
  call       R3D_2D_lsq_R1_WENO ( k_sch, k_srdr, A3D_UGDS_dgs%ndg_nlmnt, A3D_UGDS_dgs%dg_data, A3D_UGR1_2D_cells%nstncl,&             !
                                  A3D_UGDS_cells%x_vrtx, A3D_UGDS_cells%x_lmnt, A3D_UGR1_2D_cells%p_R1_coeffs,          &             !
                                  A3D_UGR1_2D_cells%dx_lmnt, A3D_UGR1_2D_cells%Gpnt_xyzw, v, v_R, v_L)                                !
!$OMP PARALLEL WORKSHARE DEFAULT(PRIVATE) SHARED(divF, Nlmnts)                                                                    !-|| A3D_ADR_2D_unstrctrd
  divF(1:Nlmnts:1) = 0.                                                                                                                                 !     !    !   !
!$OMP END PARALLEL WORKSHARE                                                                                                      !-|| A3D_ADR_2D_unstrctrd
!====>!$OMP PARALLEL DO SCHEDULE(STATIC) DEFAULT(PRIVATE) REDUCTION( + : divF)                                                &         !-|| A3D_ADR_2D_unstrctrd
!====>!$OMP                              SHARED( Nedgs, n_data_edge, xp, x_element, Gpnt_xyzw, u_L, u_R)                                !-|| A3D_ADR_2D_unstrctrd
  do ndg = 1, Ndgs, 1; np1     =A3D_UGDS_dgs%dg_data(ndg)%n_point_1!-------------------------------------------------------         !     !    !   !
                       np2     =A3D_UGDS_dgs%dg_data(ndg)%n_point_2                                                        !                            !     !    !   !
                       nlmnt1  =A3D_UGDS_dgs%dg_data(ndg)%n_element_1! np1 = n_plgn1                                       !                            !     !    !   !
                       nlmnt2  =A3D_UGDS_dgs%dg_data(ndg)%n_element_2! np2 = n_plgn2                                       !                            !     !    !   !
  NGs_dg = size(A3D_UGR1_2D_cells%Gpnt_xyzw(ndg)%wGp); FdS_12 = 0.                                                         !                            !     !    !   !
  do ind = 1, NGs_dg, 1; wGauss = A3D_UGR1_2D_cells%Gpnt_xyzw(ndg)% wGp(ind)!--------------------------------------------  !                            !     !    !   !
                         dS_x   = A3D_UGR1_2D_cells%Gpnt_xyzw(ndg)%dSGp(ind)%x                                           ! !                            !     !    !   !
                         dS_y   = A3D_UGR1_2D_cells%Gpnt_xyzw(ndg)%dSGp(ind)%y                                           ! !                            !     !    !   !
  if ( nlmnt2     ==0) then; Fx = v_L(ndg)%v(ind)                                                                        ! !                            !     !    !   !
                             Fy = v_L(ndg)%v(ind)                                                                        ! !                            !     !    !   !
                             FdS_12 = FdS_12+wGauss*(Fx*dS_x+Fy*dS_y); end if                                            ! !                            !     !    !   !
  if ( nlmnt2     /=0) then; if (    dS_x>=0.) then; Fx = v_L(ndg)%v(ind); else; Fx = v_R(ndg)%v(ind); end if            ! !                            !     !    !   !
                             if (    dS_y>=0.) then; Fy = v_L(ndg)%v(ind); else; Fy = v_R(ndg)%v(ind); end if            ! !                            !     !    !   !
                             FdS_12 = FdS_12+wGauss*(Fx*dS_x+Fy*dS_y); end if; end do!<----------------------------------  !                            !     !    !   !
                       divF( nlmnt1     ) = divF( nlmnt1     )+FdS_12                                                      !                            !     !    !   !
  if ( nlmnt2     /=0) divF( nlmnt2     ) = divF( nlmnt2     )-FdS_12                                                      !                            !     !    !   !
! write(*,*) nlmnt1, FdS_12, divF( nlmnt1     )
  end do!<-----------------------------------------------------------------------------------------------------------------                             !     !    !   !
! stop
!====>!$OMP END PARALLEL DO                                                                                                             !-|| A3D_ADR_2D_unstrctrd
  divF(1:Nlmnts:1) = divF(1:Nlmnts:1)/A3D_UGDS_cells%A_lmnt(1:Nlmnts:1)
  deallocate(v_L)
  deallocate(v_R)
!-----------------------------------------------------------------------------------------------------------------------------------
!-                            ------------------------------------------------------------------------------------------------------
!-                            ------------------------------------------------------------------------------------------------------
!-                            ------------------------------------------------------------------------------------------------------
!-                            ------------------------------------------------------------------------------------------------------
!-                            ------------------------------------------------------------------------------------------------------
!-end subroutine A3D_sch_AD_2D------------------------------------------------------------------------------------------------------
!-                            ------------------------------------------------------------------------------------------------------
!-                            ------------------------------------------------------------------------------------------------------
!-                            ------------------------------------------------------------------------------------------------------
!-                            ------------------------------------------------------------------------------------------------------
!-                            ------------------------------------------------------------------------------------------------------
!-----------------------------------------------------------------------------------------------------------------------------------
  end subroutine A3D_sch_AD_2D
!-----------------------------------------------------------------------------------------------------------------------------------
!-                     -------------------------------------------------------------------------------------------------------------
!-end module A3D_sch_NS-------------------------------------------------------------------------------------------------------------
!-                     -------------------------------------------------------------------------------------------------------------
!-----------------------------------------------------------------------------------------------------------------------------------
  end module A3D_sch_NS
