  module A3D_tensors
!-----------------------------------------------------------------------------------------------------------------------------------
  use RSM_tensor_types
!-----------------------------------------------------------------------------------------------------------------------------------
  interface Idtnsr2
  module procedure Id_symtnsr2
  module procedure Id_tnsr2
  end interface Idtnsr2
!-----------------------------------------------------------------------------------------------------------------------------------
  interface zerotnsr2
  module procedure zero_symtnsr2
  module procedure zero_tnsr2
  module procedure zero_vctr
  end interface zerotnsr2
!-----------------------------------------------------------------------------------------------------------------------------------
  interface assignment (=)
  module procedure ass_symtnsr2
  module procedure ass_tnsr2
  module procedure ass_symtnsr2_tnsr2
  module procedure ass_vctr
  end interface
!-----------------------------------------------------------------------------------------------------------------------------------
  interface operator (+)
  module procedure add_symtnsr2
  module procedure add_tnsr2
  module procedure add_vctr
  end interface
!-----------------------------------------------------------------------------------------------------------------------------------
  interface operator (-)
  module procedure mns_symtnsr2
  module procedure mns_tnsr2
  module procedure mns_vctr
  module procedure opp_symtnsr2
  module procedure opp_tnsr2
  module procedure opp_vctr
  end interface
!-----------------------------------------------------------------------------------------------------------------------------------
  interface operator (*)
  module procedure tms_symtnsr2
  module procedure tms_tnsr2
  module procedure tms_vctr
  module procedure symtnsr2_tms
  module procedure tnsr2_tms
  module procedure vctr_tms
  end interface
!-----------------------------------------------------------------------------------------------------------------------------------
  interface operator (/)
  module procedure symtnsr2_dvd
  module procedure tnsr2_dvd
  module procedure vctr_dvd
  end interface
!-----------------------------------------------------------------------------------------------------------------------------------
  interface operator (**)
  module procedure symtnsr2_pwr
  end interface
!-----------------------------------------------------------------------------------------------------------------------------------
  interface operator (.twotnsr.)
  module procedure to_tnsr2
  end interface
!-----------------------------------------------------------------------------------------------------------------------------------
  interface operator (.trc.)
  module procedure trc_symtnsr2
  module procedure trc_tnsr2
  end interface
!-----------------------------------------------------------------------------------------------------------------------------------
  interface operator (.trnsps.)
  module procedure trnsps_symtnsr2
  module procedure trnsps_tnsr2
  end interface
!-----------------------------------------------------------------------------------------------------------------------------------
  interface operator (.dev.)
  module procedure dev_symtnsr2
  module procedure dev_tnsr2
  end interface
!-----------------------------------------------------------------------------------------------------------------------------------
  interface operator (.bLml.)
  module procedure bLml_symtnsr2
  end interface
!-----------------------------------------------------------------------------------------------------------------------------------
  interface operator (.dot.)
  module procedure dotprdct_symtnsr2
  module procedure dotprdct_tnsr2
  module procedure dotprdct_tnsr2_symtnsr2
  module procedure dotprdct_symtnsr2_tnsr2
  module procedure dotprdct_tnsr2_vctr
  module procedure dotprdct_symtnsr2_vctr
  end interface
!-----------------------------------------------------------------------------------------------------------------------------------
  interface operator (.cntrctn.)
  module procedure cntrctn_symtnsr2
  module procedure cntrctn_tnsr2
  end interface
!-----------------------------------------------------------------------------------------------------------------------------------
  interface operator (.sym.)
  module procedure sym_tnsr2
  end interface
!-----------------------------------------------------------------------------------------------------------------------------------
  interface operator (.symdot.)
  module procedure symdotprdct_symtnsr2
  end interface
!-----------------------------------------------------------------------------------------------------------------------------------
  contains
!-----------------------------------------------------------------------------------------------------------------------------------
!-                 -----------------------------------------------------------------------------------------------------------------
!-                 -----------------------------------------------------------------------------------------------------------------
!-                 -----------------------------------------------------------------------------------------------------------------
!-                 -----------------------------------------------------------------------------------------------------------------
!-                 -----------------------------------------------------------------------------------------------------------------
!-module procedures-----------------------------------------------------------------------------------------------------------------
!-                 -----------------------------------------------------------------------------------------------------------------
!-                 -----------------------------------------------------------------------------------------------------------------
!-                 -----------------------------------------------------------------------------------------------------------------
!-                 -----------------------------------------------------------------------------------------------------------------
!-                 -----------------------------------------------------------------------------------------------------------------
!-----------------------------------------------------------------------------------------------------------------------------------
!
!-----------------------------------------------------------------------------------------------------------------------------------
!-           -----------------------------------------------------------------------------------------------------------------------
!-interrogate-----------------------------------------------------------------------------------------------------------------------
!-           -----------------------------------------------------------------------------------------------------------------------
!-----------------------------------------------------------------------------------------------------------------------------------
  function issymtnsr2 (A)
  type (R3Dxyztensor2), intent(in   ) :: A
  logical                             :: issymtnsr2
  if ((A%xy==A%yx).and.(A%yz==A%zy).and.(A%zx==A%xz)) then; issymtnsr2 = .TRUE.; else; issymtnsr2 = .FALSE.; end if
  end function issymtnsr2
!-----------------------------------------------------------------------------------------------------------------------------------
!-               -------------------------------------------------------------------------------------------------------------------
!-identity_tensor-------------------------------------------------------------------------------------------------------------------
!-               -------------------------------------------------------------------------------------------------------------------
!-----------------------------------------------------------------------------------------------------------------------------------
  function Id_symtnsr2 (A)
  type (R3Dxyzsymtensor2), intent(in   ) :: A
  type (R3Dxyzsymtensor2)                :: Id_symtnsr2
  Id_symtnsr2 = R3Dxyzsymtensor2(1.,0.,1.,0.,1.,0.)
  end function Id_symtnsr2
!-----------------------------------------------------------------------
  function Id_tnsr2 (A)
  type (R3Dxyztensor2), intent(in   ) :: A
  type (R3Dxyztensor2)                :: Id_tnsr2
  Id_tnsr2 = R3Dxyztensor2(1.,0.,0.,0.,1.,0.,0.,0.,1.)
  end function Id_tnsr2
!-----------------------------------------------------------------------------------------------------------------------------------
!-           -----------------------------------------------------------------------------------------------------------------------
!-zero_tensor-----------------------------------------------------------------------------------------------------------------------
!-           -----------------------------------------------------------------------------------------------------------------------
!-----------------------------------------------------------------------------------------------------------------------------------
  function zero_symtnsr2 (A)
  type (R3Dxyzsymtensor2), intent(in   ) :: A
  type (R3Dxyzsymtensor2)                :: zero_symtnsr2
  zero_symtnsr2 = R3Dxyzsymtensor2(0.,0.,0.,0.,0.,0.)
  end function zero_symtnsr2
!-----------------------------------------------------------------------
  function zero_tnsr2 (A)
  type (R3Dxyztensor2), intent(in   ) :: A
  type (R3Dxyztensor2)                :: zero_tnsr2
  zero_tnsr2 = R3Dxyztensor2(0.,0.,0.,0.,0.,0.,0.,0.,0.)
  end function zero_tnsr2
!-----------------------------------------------------------------------
  function zero_vctr (r)
  type (R3Dxyzvector), intent(in   ) :: r
  type (R3Dxyzvector)                :: zero_vctr
  zero_vctr = R3Dxyzvector(0.,0.,0.)
  end function zero_vctr
!-----------------------------------------------------------------------------------------------------------------------------------
!-                        ----------------------------------------------------------------------------------------------------------
!-overload_usual_operators----------------------------------------------------------------------------------------------------------
!-                        ----------------------------------------------------------------------------------------------------------
!-----------------------------------------------------------------------------------------------------------------------------------
  subroutine ass_symtnsr2 (A,B)
  type (R3Dxyzsymtensor2), intent(  out) :: A
  type (R3Dxyzsymtensor2), intent(in   ) :: B
  A%xx = B%xx
  A%xy = B%xy
  A%yy = B%yy
  A%yz = B%yz
  A%zz = B%zz
  A%zx = B%zx
  end subroutine ass_symtnsr2
!-----------------------------------------------------------------------
  subroutine ass_tnsr2 (A,B)
  type (R3Dxyztensor2), intent(  out) :: A
  type (R3Dxyztensor2), intent(in   ) :: B
  A%xx = B%xx
  A%xy = B%xy
  A%xz = B%xz
  A%yx = B%yx
  A%yy = B%yy
  A%yz = B%yz
  A%zx = B%zx
  A%zy = B%zy
  A%zz = B%zz
  end subroutine ass_tnsr2
!-----------------------------------------------------------------------
  subroutine ass_symtnsr2_tnsr2 (A,B)
  type (R3Dxyztensor2   ), intent(  out) :: A
  type (R3Dxyzsymtensor2), intent(in   ) :: B
! A%xx = B%xx
! A%xy = B%xy
! A%xz = B%zx
! A%yx = B%xy
! A%yy = B%yy
! A%yz = B%yz
! A%zx = B%zx
! A%zy = B%yz
! A%zz = B%zz
  A = (.twotnsr.B)
  end subroutine ass_symtnsr2_tnsr2
!-----------------------------------------------------------------------
  subroutine ass_vctr (r,q)
  type (R3Dxyzvector), intent(  out) :: r
  type (R3Dxyzvector), intent(in   ) :: q
  r%x = q%x
  r%y = q%y
  r%z = q%z
  end subroutine ass_vctr
!-----------------------------------------------------------------------------------------------------------------------------------
  function add_symtnsr2 (A,B)
  type (R3Dxyzsymtensor2), intent(in   ) :: A
  type (R3Dxyzsymtensor2), intent(in   ) :: B
  type (R3Dxyzsymtensor2)                :: add_symtnsr2
  add_symtnsr2%xx = A%xx+B%xx
  add_symtnsr2%xy = A%xy+B%xy
  add_symtnsr2%yy = A%yy+B%yy
  add_symtnsr2%yz = A%yz+B%yz
  add_symtnsr2%zz = A%zz+B%zz
  add_symtnsr2%zx = A%zx+B%zx
  end function add_symtnsr2
!-----------------------------------------------------------------------
  function add_tnsr2 (A,B)
  type (R3Dxyztensor2), intent(in   ) :: A
  type (R3Dxyztensor2), intent(in   ) :: B
  type (R3Dxyztensor2)                :: add_tnsr2
  add_tnsr2%xx = A%xx+B%xx
  add_tnsr2%xy = A%xy+B%xy
  add_tnsr2%xz = A%xz+B%xz
  add_tnsr2%yx = A%yx+B%yx
  add_tnsr2%yy = A%yy+B%yy
  add_tnsr2%yz = A%yz+B%yz
  add_tnsr2%zx = A%zx+B%zx
  add_tnsr2%zy = A%zy+B%zy
  add_tnsr2%zz = A%zz+B%zz
  end function add_tnsr2
!-----------------------------------------------------------------------
  function add_vctr (r,q)
  type (R3Dxyzvector), intent(in   ) :: r
  type (R3Dxyzvector), intent(in   ) :: q
  type (R3Dxyzvector)                :: add_vctr
  add_vctr%x = r%x+q%x
  add_vctr%y = r%y+q%y
  add_vctr%z = r%z+q%z
  end function add_vctr
!-----------------------------------------------------------------------------------------------------------------------------------
  function mns_symtnsr2 (A,B)
  type (R3Dxyzsymtensor2), intent(in   ) :: A
  type (R3Dxyzsymtensor2), intent(in   ) :: B
  type (R3Dxyzsymtensor2)                :: mns_symtnsr2
  mns_symtnsr2%xx = A%xx-B%xx
  mns_symtnsr2%xy = A%xy-B%xy
  mns_symtnsr2%yy = A%yy-B%yy
  mns_symtnsr2%yz = A%yz-B%yz
  mns_symtnsr2%zz = A%zz-B%zz
  mns_symtnsr2%zx = A%zx-B%zx
  end function mns_symtnsr2
!-----------------------------------------------------------------------
  function mns_tnsr2 (A,B)
  type (R3Dxyztensor2), intent(in   ) :: A
  type (R3Dxyztensor2), intent(in   ) :: B
  type (R3Dxyztensor2)                :: mns_tnsr2
  mns_tnsr2%xx = A%xx-B%xx
  mns_tnsr2%xy = A%xy-B%xy
  mns_tnsr2%xz = A%xz-B%xz
  mns_tnsr2%yx = A%yx-B%yx
  mns_tnsr2%yy = A%yy-B%yy
  mns_tnsr2%yz = A%yz-B%yz
  mns_tnsr2%zx = A%zx-B%zx
  mns_tnsr2%zy = A%zy-B%zy
  mns_tnsr2%zz = A%zz-B%zz
  end function mns_tnsr2
!-----------------------------------------------------------------------
  function mns_vctr (r,q)
  type (R3Dxyzvector), intent(in   ) :: r
  type (R3Dxyzvector), intent(in   ) :: q
  type (R3Dxyzvector)                :: mns_vctr
  mns_vctr%x = r%x-q%x
  mns_vctr%y = r%y-q%y
  mns_vctr%z = r%z-q%z
  end function mns_vctr
!-----------------------------------------------------------------------------------------------------------------------------------
  function opp_symtnsr2 (A)
  type (R3Dxyzsymtensor2), intent(in   ) :: A
  type (R3Dxyzsymtensor2)                :: opp_symtnsr2
  opp_symtnsr2%xx = -A%xx
  opp_symtnsr2%xy = -A%xy
  opp_symtnsr2%yy = -A%yy
  opp_symtnsr2%yz = -A%yz
  opp_symtnsr2%zz = -A%zz
  opp_symtnsr2%zx = -A%zx
  end function opp_symtnsr2
!-----------------------------------------------------------------------
  function opp_tnsr2 (A)
  type (R3Dxyztensor2), intent(in   ) :: A
  type (R3Dxyztensor2)                :: opp_tnsr2
  opp_tnsr2%xx = -A%xx
  opp_tnsr2%xy = -A%xy
  opp_tnsr2%xz = -A%xz
  opp_tnsr2%yx = -A%yx
  opp_tnsr2%yy = -A%yy
  opp_tnsr2%yz = -A%yz
  opp_tnsr2%zx = -A%zx
  opp_tnsr2%zy = -A%zy
  opp_tnsr2%zz = -A%zz
  end function opp_tnsr2
!-----------------------------------------------------------------------
  function opp_vctr (r)
  type (R3Dxyzvector), intent(in   ) :: r
  type (R3Dxyzvector)                :: opp_vctr
  opp_vctr%x = -r%x
  opp_vctr%y = -r%y
  opp_vctr%z = -r%z
  end function opp_vctr
!-----------------------------------------------------------------------------------------------------------------------------------
  function tms_symtnsr2 (s,A)
  real                   , intent(in   ) :: s
  type (R3Dxyzsymtensor2), intent(in   ) :: A
  type (R3Dxyzsymtensor2)                :: tms_symtnsr2
  tms_symtnsr2%xx = s*A%xx
  tms_symtnsr2%xy = s*A%xy
  tms_symtnsr2%yy = s*A%yy
  tms_symtnsr2%yz = s*A%yz
  tms_symtnsr2%zz = s*A%zz
  tms_symtnsr2%zx = s*A%zx
  end function tms_symtnsr2
!-----------------------------------------------------------------------
  function tms_tnsr2 (s,A)
  real                , intent(in   ) :: s
  type (R3Dxyztensor2), intent(in   ) :: A
  type (R3Dxyztensor2)                :: tms_tnsr2
  tms_tnsr2%xx = s*A%xx
  tms_tnsr2%xy = s*A%xy
  tms_tnsr2%xz = s*A%xz
  tms_tnsr2%yx = s*A%yx
  tms_tnsr2%yy = s*A%yy
  tms_tnsr2%yz = s*A%yz
  tms_tnsr2%zx = s*A%zx
  tms_tnsr2%zy = s*A%zy
  tms_tnsr2%zz = s*A%zz
  end function tms_tnsr2
!-----------------------------------------------------------------------
  function tms_vctr (s,r)
  real               , intent(in   ) :: s
  type (R3Dxyzvector), intent(in   ) :: r
  type (R3Dxyzvector)                :: tms_vctr
  tms_vctr%x = s*r%x
  tms_vctr%y = s*r%y
  tms_vctr%z = s*r%z
  end function tms_vctr
!-----------------------------------------------------------------------------------------------------------------------------------
  function symtnsr2_tms (A,s)
  type (R3Dxyzsymtensor2), intent(in   ) :: A
  real                   , intent(in   ) :: s
  type (R3Dxyzsymtensor2)                :: symtnsr2_tms
  symtnsr2_tms%xx = s*A%xx
  symtnsr2_tms%xy = s*A%xy
  symtnsr2_tms%yy = s*A%yy
  symtnsr2_tms%yz = s*A%yz
  symtnsr2_tms%zz = s*A%zz
  symtnsr2_tms%zx = s*A%zx
  end function symtnsr2_tms
!-----------------------------------------------------------------------
  function tnsr2_tms (A,s)
  type (R3Dxyztensor2), intent(in   ) :: A
  real                , intent(in   ) :: s
  type (R3Dxyztensor2)                :: tnsr2_tms
  tnsr2_tms%xx = s*A%xx
  tnsr2_tms%xy = s*A%xy
  tnsr2_tms%xz = s*A%xz
  tnsr2_tms%yx = s*A%yx
  tnsr2_tms%yy = s*A%yy
  tnsr2_tms%yz = s*A%yz
  tnsr2_tms%zx = s*A%zx
  tnsr2_tms%zy = s*A%zy
  tnsr2_tms%zz = s*A%zz
  end function tnsr2_tms
!-----------------------------------------------------------------------
  function vctr_tms (r,s)
  type (R3Dxyzvector), intent(in   ) :: r
  real               , intent(in   ) :: s
  type (R3Dxyzvector)                :: vctr_tms
  vctr_tms%x = s*r%x
  vctr_tms%y = s*r%y
  vctr_tms%z = s*r%z
  end function vctr_tms
!-----------------------------------------------------------------------------------------------------------------------------------
  function symtnsr2_dvd (A,s)
  type (R3Dxyzsymtensor2), intent(in   ) :: A
  real                   , intent(in   ) :: s
  type (R3Dxyzsymtensor2)                :: symtnsr2_dvd
  symtnsr2_dvd%xx = A%xx/s
  symtnsr2_dvd%xy = A%xy/s
  symtnsr2_dvd%yy = A%yy/s
  symtnsr2_dvd%yz = A%yz/s
  symtnsr2_dvd%zz = A%zz/s
  symtnsr2_dvd%zx = A%zx/s
  end function symtnsr2_dvd
!-----------------------------------------------------------------------
  function tnsr2_dvd (A,s)
  type (R3Dxyztensor2), intent(in   ) :: A
  real                , intent(in   ) :: s
  type (R3Dxyztensor2)                :: tnsr2_dvd
  tnsr2_dvd%xx = A%xx/s
  tnsr2_dvd%xy = A%xy/s
  tnsr2_dvd%xz = A%xz/s
  tnsr2_dvd%yx = A%yx/s
  tnsr2_dvd%yy = A%yy/s
  tnsr2_dvd%yz = A%yz/s
  tnsr2_dvd%zx = A%zx/s
  tnsr2_dvd%zy = A%zy/s
  tnsr2_dvd%zz = A%zz/s
  end function tnsr2_dvd
!-----------------------------------------------------------------------
  function vctr_dvd (r,s)
  type (R3Dxyzvector), intent(in   ) :: r
  real               , intent(in   ) :: s
  type (R3Dxyzvector)                :: vctr_dvd
  vctr_dvd%x = r%x/s
  vctr_dvd%y = r%y/s
  vctr_dvd%z = r%z/s
  end function vctr_dvd
!-----------------------------------------------------------------------------------------------------------------------------------
  recursive function symtnsr2_pwr (A,n)
  type (R3Dxyzsymtensor2), intent(in   ) :: A
  integer                , intent(in   ) :: n
  type (R3Dxyzsymtensor2)                :: symtnsr2_pwr
  if (n==0) symtnsr2_pwr = Id_symtnsr2(A)
  if (n>=1) symtnsr2_pwr = .sym.(A.dot.(A**(n-1)))
  end function symtnsr2_pwr
!-----------------------------------------------------------------------------------------------------------------------------------
!-                ------------------------------------------------------------------------------------------------------------------
!-define_operators------------------------------------------------------------------------------------------------------------------
!-                ------------------------------------------------------------------------------------------------------------------
!-----------------------------------------------------------------------------------------------------------------------------------
  function to_tnsr2 (A)
  type (R3Dxyzsymtensor2), intent(in   ) :: A
  type (R3Dxyztensor2   )                :: to_tnsr2
  to_tnsr2%xx = A%xx
  to_tnsr2%xy = A%xy
  to_tnsr2%xz = A%zx
  to_tnsr2%yx = A%xy
  to_tnsr2%yy = A%yy
  to_tnsr2%yz = A%yz
  to_tnsr2%zx = A%zx
  to_tnsr2%zy = A%yz
  to_tnsr2%zz = A%zz
  end function to_tnsr2
!-----------------------------------------------------------------------------------------------------------------------------------
  function trc_symtnsr2 (A)
  type (R3Dxyzsymtensor2), intent(in   ) :: A
  real                                   :: trc_symtnsr2
  trc_symtnsr2 = A%xx+A%yy+A%zz
  end function trc_symtnsr2
!-----------------------------------------------------------------------
  function trc_tnsr2 (A)
  type (R3Dxyztensor2), intent(in   ) :: A
  real                                :: trc_tnsr2
  trc_tnsr2 = A%xx+A%yy+A%zz
  end function trc_tnsr2
!-----------------------------------------------------------------------------------------------------------------------------------
  function trnsps_symtnsr2 (A)
  type (R3Dxyzsymtensor2), intent(in   ) :: A
  type (R3Dxyzsymtensor2)                :: trnsps_symtnsr2
  trnsps_symtnsr2 = A
  end function trnsps_symtnsr2
!-----------------------------------------------------------------------
  function trnsps_tnsr2 (A)
  type (R3Dxyztensor2), intent(in   ) :: A
  type (R3Dxyztensor2)                :: trnsps_tnsr2
  trnsps_tnsr2%xx = A%xx
  trnsps_tnsr2%xy = A%yx
  trnsps_tnsr2%xz = A%zx
  trnsps_tnsr2%yx = A%xy
  trnsps_tnsr2%yy = A%yy
  trnsps_tnsr2%yz = A%zy
  trnsps_tnsr2%zx = A%xz
  trnsps_tnsr2%zy = A%yz
  trnsps_tnsr2%zz = A%zz
  end function trnsps_tnsr2
!-----------------------------------------------------------------------------------------------------------------------------------
  function dev_symtnsr2 (A)
  type (R3Dxyzsymtensor2), intent(in   ) :: A
  type (R3Dxyzsymtensor2)                :: dev_symtnsr2
  real                                   :: trcA
  trcA = .trc.A
! dev_symtnsr2%xx = A%xx-trcA/3.
! dev_symtnsr2%xy = A%xy
! dev_symtnsr2%yy = A%yy-trcA/3.
! dev_symtnsr2%yz = A%yz
! dev_symtnsr2%zz = A%zz-trcA/3.
! dev_symtnsr2%zx = A%zx
  dev_symtnsr2 = A-1./3.*trcA*Id_symtnsr2(A)
  end function dev_symtnsr2
!-----------------------------------------------------------------------
  function dev_tnsr2 (A)
  type (R3Dxyztensor2), intent(in   ) :: A
  type (R3Dxyztensor2)                :: dev_tnsr2
  real                                :: trcA
  trcA = .trc.A
  dev_tnsr2 = A-1./3.*trcA*Id_tnsr2(A)
  end function dev_tnsr2
!-----------------------------------------------------------------------------------------------------------------------------------
  function bLml_symtnsr2 (A)
  type (R3Dxyzsymtensor2), intent(in   ) :: A
  type (R3Dxyzsymtensor2)                :: bLml_symtnsr2
  real                                   :: trcA
  trcA = .trc.A+tiny(trcA) ! avoid division with 0, and ensure compatibility with legacy RSM (Vallet 1995)
! trcA = .trc.A+2.e-23     ! avoid division with 0, and ensure compatibility with legacy RSM (Vallet 1995)
! bLml_symtnsr2%xx = A%xx/trcA-1./3.
! bLml_symtnsr2%xy = A%xy/trcA
! bLml_symtnsr2%yy = A%yy/trcA-1./3.
! bLml_symtnsr2%yz = A%yz/trcA
! bLml_symtnsr2%zz = A%zz/trcA-1./3.
! bLml_symtnsr2%zx = A%zx/trcA
  bLml_symtnsr2 = A/trcA-1./3.*Id_symtnsr2(A)
  end function bLMl_symtnsr2
!-----------------------------------------------------------------------------------------------------------------------------------
  function dotprdct_symtnsr2 (A,B)
  type (R3Dxyzsymtensor2), intent(in   ) :: A
  type (R3Dxyzsymtensor2), intent(in   ) :: B
  type (R3Dxyztensor2)                   :: dotprdct_symtnsr2
  dotprdct_symtnsr2%xx = A%xx*B%xx+A%xy*B%xy+A%zx*B%zx ! A%xx*B%xx+A%xy*B%yx+A%xz*B%zx 
  dotprdct_symtnsr2%xy = A%xx*B%xy+A%xy*B%yy+A%zx*B%yz ! A%xx*B%xy+A%xy*B%yy+A%xz*B%zy
  dotprdct_symtnsr2%xz = A%xx*B%zx+A%xy*B%yz+A%zx*B%zz ! A%xx*B%xz+A%xy*B%yz+A%xz*B%zz
  dotprdct_symtnsr2%yx = A%xy*B%xx+A%yy*B%xy+A%yz*B%zx ! A%yx*B%xx+A%yy*B%yx+A%yz*B%zx
  dotprdct_symtnsr2%yy = A%xy*B%xy+A%yy*B%yy+A%yz*B%yz ! A%yx*B%xy+A%yy*B%yy+A%yz*B%zy
  dotprdct_symtnsr2%yz = A%xy*B%zx+A%yy*B%yz+A%yz*B%zz ! A%yx*B%xz+A%yy*B%yz+A%yz*B%zz
  dotprdct_symtnsr2%zx = A%zx*B%xx+A%yz*B%xy+A%zz*B%zx ! A%zx*B%xx+A%zy*B%yx+A%zz*B%zx
  dotprdct_symtnsr2%zy = A%zx*B%xy+A%yz*B%yy+A%zz*B%yz ! A%zx*B%xy+A%zy*B%yy+A%zz*B%zy
  dotprdct_symtnsr2%zz = A%zx*B%zx+A%yz*B%yz+A%zz*B%zz ! A%zx*B%xz+A%zy*B%yz+A%zz*B%zz
  end function dotprdct_symtnsr2
!-----------------------------------------------------------------------
  function dotprdct_tnsr2 (A,B)
  type (R3Dxyztensor2), intent(in   ) :: A
  type (R3Dxyztensor2), intent(in   ) :: B
  type (R3Dxyztensor2)                :: dotprdct_tnsr2
  dotprdct_tnsr2%xx = A%xx*B%xx+A%xy*B%yx+A%xz*B%zx 
  dotprdct_tnsr2%xy = A%xx*B%xy+A%xy*B%yy+A%xz*B%zy
  dotprdct_tnsr2%xz = A%xx*B%xz+A%xy*B%yz+A%xz*B%zz
  dotprdct_tnsr2%yx = A%yx*B%xx+A%yy*B%yx+A%yz*B%zx
  dotprdct_tnsr2%yy = A%yx*B%xy+A%yy*B%yy+A%yz*B%zy
  dotprdct_tnsr2%yz = A%yx*B%xz+A%yy*B%yz+A%yz*B%zz
  dotprdct_tnsr2%zx = A%zx*B%xx+A%zy*B%yx+A%zz*B%zx
  dotprdct_tnsr2%zy = A%zx*B%xy+A%zy*B%yy+A%zz*B%zy
  dotprdct_tnsr2%zz = A%zx*B%xz+A%zy*B%yz+A%zz*B%zz
  end function dotprdct_tnsr2
!-----------------------------------------------------------------------
  function dotprdct_tnsr2_symtnsr2 (A,B)
  type (R3Dxyztensor2   ), intent(in   ) :: A
  type (R3Dxyzsymtensor2), intent(in   ) :: B
  type (R3Dxyztensor2   )                :: dotprdct_tnsr2_symtnsr2
  dotprdct_tnsr2_symtnsr2 = A.dot.(.twotnsr.B)
  end function dotprdct_tnsr2_symtnsr2
!-----------------------------------------------------------------------
  function dotprdct_symtnsr2_tnsr2 (A,B)
  type (R3Dxyzsymtensor2), intent(in   ) :: A
  type (R3Dxyztensor2   ), intent(in   ) :: B
  type (R3Dxyztensor2   )                :: dotprdct_symtnsr2_tnsr2
  dotprdct_symtnsr2_tnsr2 = (.twotnsr.A).dot.B
  end function dotprdct_symtnsr2_tnsr2
!-----------------------------------------------------------------------
  function dotprdct_tnsr2_vctr (A,r)
  type (R3Dxyztensor2   ), intent(in   ) :: A
  type (R3Dxyzvector    ), intent(in   ) :: r
  type (R3Dxyzvector    )                :: dotprdct_tnsr2_vctr
  dotprdct_tnsr2_vctr%x=A%xx*r%x+A%xy*r%y+A%xz*r%z
  dotprdct_tnsr2_vctr%y=A%yx*r%x+A%yy*r%y+A%yz*r%z
  dotprdct_tnsr2_vctr%z=A%zx*r%x+A%zy*r%y+A%zz*r%z
  end function dotprdct_tnsr2_vctr
!-----------------------------------------------------------------------
  function dotprdct_symtnsr2_vctr  (A,r)
  type (R3Dxyzsymtensor2), intent(in   ) :: A
  type (R3Dxyzvector    ), intent(in   ) :: r
  type (R3Dxyzvector    )                :: dotprdct_symtnsr2_vctr
  dotprdct_symtnsr2_vctr  = (.twotnsr.A).dot.r
  end function dotprdct_symtnsr2_vctr
!-----------------------------------------------------------------------------------------------------------------------------------
  function cntrctn_symtnsr2 (A,B)
  type (R3Dxyzsymtensor2), intent(in   ) :: A
  type (R3Dxyzsymtensor2), intent(in   ) :: B
  real                                   :: cntrctn_symtnsr2
! cntrctn_symtnsr2 = A%xx*B%xx+2.*A%xy*B%xy+A%yy*B%yy+2.*A%yz*B%yz+A%zz*B%zz+2.*A%zx*B%zx
  cntrctn_symtnsr2 = .trc.(A.dot.B)
  end function cntrctn_symtnsr2
!-----------------------------------------------------------------------
  function cntrctn_tnsr2 (A,B)
  type (R3Dxyztensor2), intent(in   ) :: A
  type (R3Dxyztensor2), intent(in   ) :: B
  real                                   :: cntrctn_tnsr2
  cntrctn_tnsr2 = .trc.(A.dot.B)
  end function cntrctn_tnsr2
!-----------------------------------------------------------------------------------------------------------------------------------
  function sym_tnsr2 (A)
  type (R3Dxyztensor2)   , intent(in   ) :: A
  type (R3Dxyzsymtensor2)                :: sym_tnsr2
  sym_tnsr2%xx = A%xx
  sym_tnsr2%xy =(A%xy+A%yx)/2.
  sym_tnsr2%yy = A%yy
  sym_tnsr2%yz =(A%yz+A%zy)/2.
  sym_tnsr2%zz = A%zz
  sym_tnsr2%zx =(A%zx+A%xz)/2.
  end function sym_tnsr2
!-----------------------------------------------------------------------------------------------------------------------------------
  function symdotprdct_symtnsr2 (A,B) ! A.B+B.A
  type (R3Dxyzsymtensor2), intent(in   ) :: A
  type (R3Dxyzsymtensor2), intent(in   ) :: B
  type (R3Dxyzsymtensor2)                :: symdotprdct_symtnsr2
!-symdotprdct_symtnsr2 = .sym.((A.dot.B)+(B.dot.A))---------------------
  symdotprdct_symtnsr2%xx = A%xx*B%xx+A%xy*B%xy+A%zx*B%zx&
                          + B%xx*A%xx+B%xy*A%xy+B%zx*A%zx
  symdotprdct_symtnsr2%xy = A%xx*B%xy+A%xy*B%yy+A%zx*B%yz&
                          + B%xx*A%xy+B%xy*A%yy+B%zx*A%yz
  symdotprdct_symtnsr2%yy = A%xy*B%xy+A%yy*B%yy+A%yz*B%yz&
                          + B%xy*A%xy+B%yy*A%yy+B%yz*A%yz
  symdotprdct_symtnsr2%yz = A%xy*B%zx+A%yy*B%yz+A%yz*B%zz&
                          + B%xy*A%zx+B%yy*A%yz+B%yz*A%zz
  symdotprdct_symtnsr2%zz = A%zx*B%zx+A%yz*B%yz+A%zz*B%zz&
                          + B%zx*A%zx+B%yz*A%yz+B%zz*A%zz
  symdotprdct_symtnsr2%zx = A%zx*B%xx+A%yz*B%xy+A%zz*B%zx&
                          + B%zx*A%xx+B%yz*A%xy+B%zz*A%zx
  end function symdotprdct_symtnsr2
!-----------------------------------------------------------------------------------------------------------------------------------
  end module A3D_tensors
