  program A3D_NS_2D
!-----------------------------------------------------------------------------------------------------------------------------------
!-                                                                                                                                --
!-A3D_NS_2D:: development version of the unstructured code:: laminar, 2-D                                                         --
!-                                                                                                                                --
!-                                                                                                                                --
!-                                                                                                                                --
!-                                                                                                                                --
!-                                                                                                                                --
!-                                                                                                                                --
!-                                                                                                                                --
!-                                                                                                                                --
!-                                                                                                                       dec 2018 --
!-----------------------------------------------------------------------------------------------------------------------------------
!$ use OMP_LIB                                                                                                                    !-||
!-----------------------------------------------------------------------------------------------------------------------------------
! use, intrinsic :: iso_fortran_env
! use A3D_prmtrs
  use A3D_thrm
!-----------------------------------------------------------------------------------------------------------------------------------
   use A3D_mssgs
   use A3D_types
   use A3D_vrbls_types
   use R3D_types
   use A3D_data_structure
!  use A3D_mio
   use A3Dg_2D_routines
   use R3D_2D_lsq_WENO
   use R3D_to_plots_routines
   use R3D_maths
   use A3D_sch_NS
   use A3D_2D_reorder_unstrctrd
   use A3D_2D_sch_imp
!-----------------------------------------------------------------------------------------------------------------------------------
  implicit none
!-----------------------------------------------------------------------------------------------------------------------------------
  real                 :: cpu_time_RUN, cpu_time_s, elapsed_time_RUN, elapsed_time_RUN0, cpu_time_RUN0
  type(A3DthrmdataBSL) :: A3D_thrm_data
  type(A3Dvsc)         :: vsc_inf
  real                 :: T_inf, p_inf, xM_inf, Re_L, gamma, Rg, rho_inf, a_inf, V_inf, xL, xLE, x1, x2, y1, y2, dxLE, dx, ri, rj,&
                          delta, xui, Pr_inf, pt_inf, Tt_inf, r, yodlt, riLEFP, dLE, dy, d_from_LE
  integer              :: i, j, n, Ni, Nj, istep, jstep, iLE, Nic, Njc, istepc, jstepc, nc, ii, NjcLE, NicLE,               &
                          Ndgs, ndg, np1, np2, nlmnt1, nlmnt2, k_srdr, k_vrbs, k_safe, k_sch, Ncells, Nvrtcs, Nlmnts, nlmnt,&
                          Nbndlmnts, Nbnddgs, Nbnddgslmnt, nbndlmnt, nbnddg, ind, nlmntLE, np, NiLE, NjLE, istepLE, jstepLE,&
                          n1, n2, nc_iu, nc_id, nc_ou, nc_od, nc_wu, nc_wd, nc2iu, nc2id, nc2ou, nc2od, nc2wu, nc2wd,       &
                          Ncs, NGs_dg, Ndatas, Ngbs_lmnt, ngblmnt, ndata, Nvrts_nlmnt, Nlmnts_np, Ndgs_nlmnt, Nbndphntms,   &
                          nbndphntm, nphlmnt
!-->DELETE--->  integer, dimension(:), allocatable :: kBC !-->HACK TO IMPROVE
  real                 :: CFL_str, CFL, fctr, dnm, error_u, rdctn_u, error_u_old, rdctn_TRG, rstr
  integer              :: N_it_s, M_it_s, n_it, m_it, it, k_take, Mits, k_implct, JGS_it_s
  real                 :: cpu_time_RUN_max, elapsed_time_RUN_max
  integer              :: k_dbg, k_plts, k_RS, k_WENO, k_map, k_trdr, k_tint
  integer              :: kBC, m, NBCdatas, nBC
  real                 :: dw1, dw2, T, Riem_i, Riem_o, xnxn, Vn, Vpx, Vpy, s, cp, T_ISA, p_ISA, a, p, Vnlmnt, VnBC, VpxBC, VpyBC,&
                          VxBC, VyBC, rhoBC, pBC, TBC
  real                 :: rho, Vpxlmnt, Vpylmnt, Vx, Vy, alpha_inf, sgn
!=====>A3D::wave===>  real                 :: ax, ay, an, vph
  type(R3Dxyzvector   ):: xdg, xn
  integer              :: iof, k_thrm
  integer              :: N_procs, N_thrds, n_thrd
!-A3D_unstrctrd---------------------------------------------------------------------------------------------------------------------
  type(A3DruntimeprmtrsNS2D)                                      :: A3D_runtime_NS2D
  type(R3Dmssgs   )                                               :: A3Dr_mssgs
  type(A3DUGDS2Dplgnstplg    )                                    :: A3D_UGDS_cells
  type(A3DUGDS2Dplgnsdgsvrtcs)                                    :: A3D_UGDS_dgs
  type(A3DUGR12Dcells        )                                    :: A3D_UGR1_2D_cells
  type(A3D2DNSbndrs          )                                    :: A3D_UGDS_2D_bndrs
  logical                     , dimension(:), allocatable         :: isreallmnt
  character(len=:), allocatable :: gas
  character(len=:), allocatable :: tm_intgrtn
  character(len=256) :: buffer
!-----------------------------------------------------------------------------------------------------------------------------------
! type(R3Dpcoeffs            ), dimension(:), allocatable :: p_R1_coeffs
! type(R3Dpcoeffs            ), dimension(:), allocatable :: p_RD_coeffs
! type(R3Dpcoeffs            ), dimension(:), allocatable :: p_RN_coeffs
! type(R3Dpcoeffs            )                            :: p_Rc_coeffs
! real                        , dimension(:), allocatable :: udata
! real                        , dimension(:), allocatable :: apR
! real                        , dimension(:), allocatable :: a1p
! real                        , dimension(:), allocatable :: a2p
! real                        , dimension(:), allocatable :: a3p
! real                        , dimension(:), allocatable :: a4p
! real                        , dimension(:), allocatable :: a5p
! type(R3Dvn0                ), dimension(:), allocatable :: xIlsqBClmnts
! type(A3DvNS                ), dimension(:), allocatable :: A3Dv_bndlmnts
!-----------------------------------------------------------------------------------------------------------------------------------
!->A3D::early::polymorphic_allocate_not_functional_in_gfortran<7::to_test_on_BIONIC                                                  !<-BIONIC
! class(A3DprhoT             ),               allocatable         :: A3D_vrbl                                                        !<-BIONIC
! type (A3DprhoTV            )                                    :: A3D_vrbl_wrk                                                    !<-BIONIC
  logical                                       :: inq_exists
!-----------------------------------------------------------------------------------------------------------------------------------
  type(A3DvNS                ), dimension(:), allocatable         :: A3D_vrbls
  type(A3DuNS                ), dimension(:), allocatable         :: A3D_uold
  type(A3DuNS                ), dimension(:), allocatable         :: A3D_unew
  type(A3DuNS                ), dimension(:), allocatable         :: du
  type(A3DuNS                ), dimension(:), allocatable         :: A3D_Rvs
  real                        , dimension(:), allocatable         :: dtlcl
!=====>A3D::wave===>  real                        , dimension(:), allocatable         :: vold, vnew, dtAD, divF
!-A3D_unstrctrd::work_in_progress::to_remove----------------------------------------------------------------------------------------
  type(A3DnDstrcrd    )                            :: A3D_nD_strctrd ! temporary HACK (ultimately to A3Dg_2D)
  real   , dimension(:), allocatable :: xwrk, ywrk, fwrk, gwrk, hwrk, iwrk
  integer, dimension(:), allocatable :: nwrk
  integer, dimension(:,:), allocatable :: nnwrk
  integer, dimension(:,:), allocatable :: npwrs
  integer, dimension(:,:), allocatable :: npwrsd
  type(A3Dxyvector_int), dimension(:)  , allocatable                :: drvtvrule
  type(R3Dxyzvector   ), dimension(:), allocatable :: x_vrtx_wrk
  type(R3Dnn0         ), dimension(:), allocatable :: np_nlmnt_wrk
!-----------------------------------------------------------------------
  type(A3Dprfrmnc), dimension(:), allocatable :: A3D_prfrmnc
  character(len=101)                             :: io_format
  character(len=101)                             :: data_name
  character(len= 29)                             :: R3Ddate
  character(len= 21)                             :: R3Dversion
  real :: wck_time_s, cpu_timeit, wck_timeit, cpuit0, wckit0, wck_time_s_0, cpu_time_s_0
  integer :: nprcdr, Nprcdrs
!-Variables used for implicit time-marching-----------------------------------------------------------------------------------------
  integer                                                         :: Lin_it_s, Nlmnts_lvl, N_levels, n_level
  real                                                            :: omg          ! relaxation factor omega for BLU
  integer,                 dimension(:), allocatable              :: grp_lmnt
  real   ,                 dimension(:), allocatable              :: Smaxdg
  real   ,                 dimension(:), allocatable              :: xnueqdg
!   real                   , dimension(:), allocatable              :: dtlcl_nml
!   real                   , dimension(:), allocatable              :: dtlcl_str
  real                   , dimension(:), allocatable              :: grp_lmnt_real
  type(R3Dnn0           ), dimension(:), allocatable              :: nlmnt_lvl
  
!-----------------------------------------------------------------------------------------------------------------------------------
!-        --------------------------------------------------------------------------------------------------------------------------
!-        --------------------------------------------------------------------------------------------------------------------------
!-        --------------------------------------------------------------------------------------------------------------------------
!-        --------------------------------------------------------------------------------------------------------------------------
!-        --------------------------------------------------------------------------------------------------------------------------
!-starting--------------------------------------------------------------------------------------------------------------------------
!-        --------------------------------------------------------------------------------------------------------------------------
!-        --------------------------------------------------------------------------------------------------------------------------
!-        --------------------------------------------------------------------------------------------------------------------------
!-        --------------------------------------------------------------------------------------------------------------------------
!-        --------------------------------------------------------------------------------------------------------------------------
!-----------------------------------------------------------------------------------------------------------------------------------
  A3D_runtime_NS2D%A3D_prgrm = 'A3D_NS_2D'
  call R3D_version (R3Dversion, R3Ddate, 'A3D_NS_2D')
  call cpu_time(cpu_time_RUN0)
  elapsed_time_RUN0 = OMP_GET_WTIME()
  call cpu_time(A3D_runtime_NS2D%cpu_time_s_run0); A3D_runtime_NS2D%wck_time_s_run0=OMP_GET_WTIME()
!-----------------------------------------------------------------------
  Nprcdrs = 8; allocate(A3D_prfrmnc(Nprcdrs))
  A3D_prfrmnc( 1)%name='A3D_NS_u_to_v'
  A3D_prfrmnc( 2)%name='R3D_to_plots_2D_unstrcrd'
  A3D_prfrmnc( 3)%name='A3D_2D_stencil'
  A3D_prfrmnc( 4)%name='R3D_2D_lsq_R1_coeffs'
  A3D_prfrmnc( 5)%name='A3D_2D_edge_Gpnts'
  A3D_prfrmnc( 6)%name='A3D_NS_v_to_u'
  A3D_prfrmnc( 7)%name='A3D_sch_NS_2D'
  A3D_prfrmnc( 8)%name='A3D_err_NS_2D'
  do nprcdr = 1, Nprcdrs, 1; A3D_prfrmnc(nprcdr)%cpu_time_s=0.
                             A3D_prfrmnc(nprcdr)%wck_time_s=0.
                             A3D_prfrmnc(nprcdr)%N_times_called=0; end do
!-----------------------------------------------------------------------------------------------------------------------------------||
!$OMP PARALLEL
  N_procs = OMP_GET_NUM_PROCS  ()
  N_thrds = OMP_GET_NUM_THREADS()
!----!$OMP DO
!----  do n =1, 30000, 1
  n_thrd  = OMP_GET_THREAD_NUM ()+1
  write(unit= * ,fmt='(a,L1,a,i14,a,i14,a,i14)')&
       ' parallel? = ', OMP_IN_PARALLEL(),&
       ' A2D_NS_2D: n_thrd/N_thrds@N_procs = ', n_thrd, '/', N_thrds, '@', N_procs
!----  end do
!----!$OMP END DO
!$OMP END PARALLEL
!-----------------------------------------------------------------------------------------------------------------------------------
!-                           -------------------------------------------------------------------------------------------------------
!-                           -------------------------------------------------------------------------------------------------------
!-                           -------------------------------------------------------------------------------------------------------
!-                           -------------------------------------------------------------------------------------------------------
!-                           -------------------------------------------------------------------------------------------------------
!-read_computation_parameters-------------------------------------------------------------------------------------------------------
!-                           -------------------------------------------------------------------------------------------------------
!-                           -------------------------------------------------------------------------------------------------------
!-                           -------------------------------------------------------------------------------------------------------
!-                           -------------------------------------------------------------------------------------------------------
!-                           -------------------------------------------------------------------------------------------------------
!-----------------------------------------------------------------------------------------------------------------------------------
!---->  write(unit=*,fmt='(1x,a)') 'enter k_take'                      ; read (unit= * ,fmt= * ) k_take
  write(unit= * ,fmt= * ) ' enter N_it_s, cpu_time_RUN_max (h),  elapsed_time_RUN_max (h)'
  read (unit= * ,fmt= * )         N_it_s, cpu_time_RUN_max    ,  elapsed_time_RUN_max; cpu_time_RUN_max =     cpu_time_RUN_max*3600.
                                                                                   elapsed_time_RUN_max = elapsed_time_RUN_max*3600.
  write(unit= * ,fmt= * ) ' enter k_vrbs, k_dbg, k_plts'; read (unit= * ,fmt= * ) k_vrbs, k_dbg, k_plts
  write(unit= * ,fmt= * ) ' enter k_srdr, k_sch, k_RS, k_WENO, k_map'; read (unit= * ,fmt= * ) k_srdr, k_sch, k_RS, k_WENO, k_map
  write(unit= * ,fmt= * ) ' enter M_it_s, CFL, CFL*, rdctn_TRG, k_implct, k_trdr, k_tint, buffer'
  read (unit= * ,fmt= * )         M_it_s, CFL, CFL_str, rdctn_TRG, k_implct, k_trdr, k_tint, buffer
  write(unit= * ,fmt= * ) ' enter Lin_it_s, omg (relaxation factor)'; read (unit= * ,fmt= * ) Lin_it_s, omg
  rdctn_TRG=-abs(rdctn_TRG); rstr = CFL/CFL_str
  write(*,*) trim(buffer), '<--'; tm_intgrtn = trim(buffer); write(*,*) tm_intgrtn
  write(*,*) ' N_it_s, M_it_s, CFL, CFL*, rdctn_TRG, k_implct, r*, k_srdr, Lin_it_s, omg = ',&
               N_it_s, M_it_s, CFL, CFL_str, rdctn_TRG, k_implct, rstr, k_srdr, Lin_it_s, omg
!-----------------------------------------------------------------------
  A3Dr_mssgs%k_vrbs=k_vrbs                           
! io_format = 'vtk'                                                                                                                 !<-A3D::TODO
!-----------------------------------------------------------------------------------------------------------------------------------
!-    ------------------------------------------------------------------------------------------------------------------------------
!-    ------------------------------------------------------------------------------------------------------------------------------
!-    ------------------------------------------------------------------------------------------------------------------------------
!-    ------------------------------------------------------------------------------------------------------------------------------
!-    ------------------------------------------------------------------------------------------------------------------------------
!-A3Df------------------------------------------------------------------------------------------------------------------------------
!-    ------------------------------------------------------------------------------------------------------------------------------
!-    ------------------------------------------------------------------------------------------------------------------------------
!-    ------------------------------------------------------------------------------------------------------------------------------
!-    ------------------------------------------------------------------------------------------------------------------------------
!-    ------------------------------------------------------------------------------------------------------------------------------
!-----------------------------------------------------------------------------------------------------------------------------------
!
!-----------------------------------------------------------------------------------------------------------------------------------!-A3D_unstrctrd::HACK
!-          ------------------------------------------------------------------------------------------------------------------------!-A3D_unstrctrd::HACK
!-A3Df::read------------------------------------------------------------------------------------------------------------------------!-A3D_unstrctrd::HACK
!-          ------------------------------------------------------------------------------------------------------------------------!-A3D_unstrctrd::HACK
!-----------------------------------------------------------------------------------------------------------------------------------!-A3D_unstrctrd::HACK
  inquire(file='A3Df_NS_2D',exist=inq_exists); write(*,*) ' A3Df_NS_2D = ', inq_exists
  if (.not.inq_exists) stop ' A3Df_NS_2D not found: aborting'
  iof = 013
  open (unit=iof,file='A3Df_NS_2D',form='unformatted')
  read (unit=iof) it
!-----------------------------------------------------------------------
  if (it>0 ) then!-----------------------------------
  read (iof) A3D_runtime_NS2D%A3D_prgrm              !
  read (iof) A3D_runtime_NS2D%cpu_time_s     ,&      !
             A3D_runtime_NS2D%wck_time_s             !
  end if!<-------------------------------------------
  write(*,*) A3D_runtime_NS2D%A3D_prgrm, len(A3D_runtime_NS2D%A3D_prgrm)
!-----------------------------------------------------------------------
  read (unit=iof) k_thrm                       !<---
  if (k_thrm==0) then!------------------------ !<---
  read (unit=iof) buffer; gas = trim(buffer)  !!<---
  write(*,*) ' thrm:', gas                    !!<---
  read (unit=iof) A3D_thrm_data%gamma,&       !!<---
                  A3D_thrm_data%Rg   ,&       !!<---
                  A3D_thrm_data%xmu0 ,&       !!<---
                  A3D_thrm_data%xlb0 ,&       !!<---
                  A3D_thrm_data%Tmu0 ,&       !!<---
                  A3D_thrm_data%Smu  ,&       !!<---
                  A3D_thrm_data%Alb           !!<---
  end if!<------------------------------------ !<---
  read (unit=iof) Nvrtcs, Nlmnts, Ndgs, Nbndlmnts, Nbnddgs, Nbndphntms
  write(*,*)      Nvrtcs, Nlmnts, Ndgs, Nbndlmnts, Nbnddgs, Nbndphntms
!-----------------------------------------------------------------------
!-        --------------------------------------------------------------
!-allocate--------------------------------------------------------------
!-        --------------------------------------------------------------
!-----------------------------------------------------------------------
  allocate(A3D_UGDS_cells%x_vrtx  (Nvrtcs))
  allocate(A3D_UGDS_cells%np_nlmnt(Nlmnts))
  allocate(A3D_UGDS_cells% x_lmnt (Nlmnts))
  allocate(A3D_UGDS_cells%dl_lmnt (Nlmnts))
  allocate(A3D_UGDS_cells% d_lmnt (Nlmnts))
  allocate(A3D_UGDS_cells% A_lmnt (Nlmnts))
!-----------------------------------------------------------------------
  allocate( A3D_UGDS_dgs%dg_data  (  Ndgs) )
  allocate( A3D_UGDS_dgs%ndg_nlmnt(Nlmnts) )
  allocate( A3D_UGDS_dgs%nlmnt_np (Nvrtcs) )
!-----------------------------------------------------------------------
  allocate( A3D_UGDS_2D_bndrs%nlmnt_nbndlmnt  (Nbndlmnts) )
  allocate( A3D_UGDS_2D_bndrs%ndg_nbndlmnt    (Nbndlmnts) )
  allocate( A3D_UGDS_2D_bndrs%nlmnt_nbnddg    (Nbnddgs  ) )
  allocate( A3D_UGDS_2D_bndrs%nbndlmnt_nbnddg (Nbnddgs  ) )
  allocate( A3D_UGDS_2D_bndrs%ndg_nbnddg      (Nbnddgs  ) )
  allocate( A3D_UGDS_2D_bndrs%kBC_lmnt        (Nbndlmnts) )
  allocate( A3D_UGDS_2D_bndrs%kBC_dg          (Nbnddgs  ) )
  allocate( A3D_UGDS_2D_bndrs%nBCdata         (Nbnddgs  ) )!<---
  allocate( A3D_UGDS_2D_bndrs%nlmnt_nbndphntm (Nbndphntms))
  allocate( A3D_UGDS_2D_bndrs%nbnddg_nbndphntm(Nbndphntms))
  allocate( A3D_UGDS_2D_bndrs%kBC_phntm       (Nbndphntms))
!-----------------------------------------------------------------------
  allocate(             isreallmnt(Nlmnts))
  allocate(               A3D_uold(Nlmnts))
!-----------------------------------------------------------------------
!-    ------------------------------------------------------------------
!-read------------------------------------------------------------------
!-    ------------------------------------------------------------------
!-----------------------------------------------------------------------
  read (unit=iof) ( A3D_UGDS_cells%x_vrtx(np)%x, np = 1, Nvrtcs, 1)
  read (unit=iof) ( A3D_UGDS_cells%x_vrtx(np)%y, np = 1, Nvrtcs, 1)
  read (unit=iof) ( A3D_UGDS_cells%x_vrtx(np)%z, np = 1, Nvrtcs, 1)
  do nlmnt = 1, Nlmnts, 1!---------------------------------------------------------------
  read (unit=iof) Nvrts_nlmnt; allocate(A3D_UGDS_cells%np_nlmnt(nlmnt)%n(Nvrts_nlmnt))   !
  read (unit=iof) ( A3D_UGDS_cells%np_nlmnt(nlmnt)%n(ind), ind = 1, Nvrts_nlmnt, 1)      !
  end do!<-------------------------------------------------------------------------------
  read (unit=iof) ( A3D_UGDS_cells% x_lmnt(nlmnt)%x, nlmnt = 1, Nlmnts, 1)
  read (unit=iof) ( A3D_UGDS_cells% x_lmnt(nlmnt)%y, nlmnt = 1, Nlmnts, 1)
  read (unit=iof) ( A3D_UGDS_cells% x_lmnt(nlmnt)%z, nlmnt = 1, Nlmnts, 1)
  read (unit=iof) ( A3D_UGDS_cells%dl_lmnt(nlmnt)  , nlmnt = 1, Nlmnts, 1)
  read (unit=iof) ( A3D_UGDS_cells% d_lmnt(nlmnt)  , nlmnt = 1, Nlmnts, 1)
  read (unit=iof) ( A3D_UGDS_cells% A_lmnt(nlmnt)  , nlmnt = 1, Nlmnts, 1)
  do np = 1, Nvrtcs, 1!-----------------------------------------------------------
  read (unit=iof) Nlmnts_np; allocate(A3D_UGDS_dgs%nlmnt_np(np)%n(Nlmnts_np))     !
  read (unit=iof) ( A3D_UGDS_dgs%nlmnt_np(np)%n(ind), ind = 1, Nlmnts_np, 1)      !
  end do!<------------------------------------------------------------------------
  read (unit=iof) ( A3D_UGDS_dgs%dg_data(ndg)%n_element_1, ndg = 1, Ndgs, 1)
  read (unit=iof) ( A3D_UGDS_dgs%dg_data(ndg)%n_element_2, ndg = 1, Ndgs, 1)
  read (unit=iof) ( A3D_UGDS_dgs%dg_data(ndg)%  n_point_1, ndg = 1, Ndgs, 1)
  read (unit=iof) ( A3D_UGDS_dgs%dg_data(ndg)%  n_point_2, ndg = 1, Ndgs, 1)
  do nlmnt = 1, Nlmnts, 1!-------------------------------------------------------------
  read (unit=iof) Ndgs_nlmnt; allocate(A3D_UGDS_dgs%ndg_nlmnt(nlmnt)%n(Ndgs_nlmnt))    !
  read (unit=iof) ( A3D_UGDS_dgs%ndg_nlmnt(nlmnt)%n(ind), ind = 1, Ndgs_nlmnt, 1)      !
  end do!<-----------------------------------------------------------------------------
!-----------------------------------------------------------------------
  read (unit=iof) ( A3D_UGDS_2D_bndrs%nlmnt_nbndlmnt  (nbndlmnt) , nbndlmnt  = 1, Nbndlmnts , 1)
  read (unit=iof) ( A3D_UGDS_2D_bndrs%ndg_nbndlmnt    (nbndlmnt) , nbndlmnt  = 1, Nbndlmnts , 1)
  read (unit=iof) ( A3D_UGDS_2D_bndrs%nlmnt_nbnddg    (nbnddg)   , nbnddg    = 1, Nbnddgs   , 1)
  read (unit=iof) ( A3D_UGDS_2D_bndrs%nbndlmnt_nbnddg (nbnddg)   , nbnddg    = 1, Nbnddgs   , 1)
  read (unit=iof) ( A3D_UGDS_2D_bndrs%ndg_nbnddg      (nbnddg)   , nbnddg    = 1, Nbnddgs   , 1)
  read (unit=iof) ( A3D_UGDS_2D_bndrs%kBC_lmnt        (nbndlmnt) , nbndlmnt  = 1, Nbndlmnts , 1)
  read (unit=iof) ( A3D_UGDS_2D_bndrs%kBC_dg          (nbnddg)   , nbnddg    = 1, Nbnddgs   , 1)
  read (unit=iof) ( A3D_UGDS_2D_bndrs%nBCdata         (nbnddg)   , nbnddg    = 1, Nbnddgs   , 1)!<---
  read (unit=iof) ( A3D_UGDS_2D_bndrs%nlmnt_nbndphntm (nbndphntm), nbndphntm = 1, Nbndphntms, 1)
  read (unit=iof) ( A3D_UGDS_2D_bndrs%nbnddg_nbndphntm(nbndphntm), nbndphntm = 1, Nbndphntms, 1)
  read (unit=iof) ( A3D_UGDS_2D_bndrs%kBC_phntm       (nbndphntm), nbndphntm = 1, Nbndphntms, 1)
!-----------------------------------------------------------------------
  read (unit=iof) ( isreallmnt(nlmnt)       , nlmnt = 1, Nlmnts, 1)
  read (unit=iof) (   A3D_uold(nlmnt)%rho   , nlmnt = 1, Nlmnts, 1)
  read (unit=iof) (   A3D_uold(nlmnt)%rhoV%x, nlmnt = 1, Nlmnts, 1)
  read (unit=iof) (   A3D_uold(nlmnt)%rhoV%y, nlmnt = 1, Nlmnts, 1)
  read (unit=iof) (   A3D_uold(nlmnt)%rhoet , nlmnt = 1, Nlmnts, 1)
!-----------------------------------------------------------------------
  read (unit=iof) NBCdatas; allocate(A3D_UGDS_2D_bndrs%BCdata(NBCdatas))         !<---
  write(*,*) ' NBCdatas = ', NBCdatas                                            !<---
  read (unit=iof) (A3D_UGDS_2D_bndrs%BCdata(nBC)%vrbls%rho, nBC = 1, NBCdatas, 1)!<---
  read (unit=iof) (A3D_UGDS_2D_bndrs%BCdata(nBC)%vrbls%  p, nBC = 1, NBCdatas, 1)!<---
  read (unit=iof) (A3D_UGDS_2D_bndrs%BCdata(nBC)%vrbls%  T, nBC = 1, NBCdatas, 1)!<---
  read (unit=iof) (A3D_UGDS_2D_bndrs%BCdata(nBC)%vrbls%V%x, nBC = 1, NBCdatas, 1)!<---
  read (unit=iof) (A3D_UGDS_2D_bndrs%BCdata(nBC)%vrbls%V%y, nBC = 1, NBCdatas, 1)!<---
  read (unit=iof) (A3D_UGDS_2D_bndrs%BCdata(nBC)%vrbls%V%z, nBC = 1, NBCdatas, 1)!<---
  read (unit=iof) (A3D_UGDS_2D_bndrs%BCdata(nBC)%qn       , nBC = 1, NBCdatas, 1)!<---
  close(unit=iof)
!-----------------------------------------------------------------------
  call cpu_time(cpu_time_s_0); wck_time_s_0=OMP_GET_WTIME()
  call       A3D_NS_u_to_v ( A3D_uold, A3D_vrbls, A3D_thrm_data, A3Dr_mssgs)
  call cpu_time(cpu_time_s  ); wck_time_s  =OMP_GET_WTIME()
  nprcdr=1
  A3D_prfrmnc(nprcdr)%cpu_time_s=A3D_prfrmnc(nprcdr)%cpu_time_s+(cpu_time_s-cpu_time_s_0)
  A3D_prfrmnc(nprcdr)%wck_time_s=A3D_prfrmnc(nprcdr)%wck_time_s+(wck_time_s-wck_time_s_0)
  A3D_prfrmnc(nprcdr)%N_times_called=A3D_prfrmnc(nprcdr)%N_times_called+1
!-----------------------------------------------------------------------------------------------------------------------------------
!-     -----------------------------------------------------------------------------------------------------------------------------
!-Sizes-----------------------------------------------------------------------------------------------------------------------------
!-     -----------------------------------------------------------------------------------------------------------------------------
!-----------------------------------------------------------------------------------------------------------------------------------
  gamma = A3D_thrm_data%gamma; Rg=A3D_thrm_data%Rg
!-----------------------------------------------------------------------
  write(*,*) 'x_vrtx   :', size(A3D_UGDS_cells%x_vrtx  )
  write(*,*) 'np_nlmnt :', size(A3D_UGDS_cells%np_nlmnt)
  write(*,*) 'x_lmnt   :', size(A3D_UGDS_cells%x_lmnt  )
  write(*,*) 'dl_lmnt  :', size(A3D_UGDS_cells%dl_lmnt )
  write(*,*) 'd_lmnt   :', size(A3D_UGDS_cells%d_lmnt  )
  write(*,*) 'A_lmnt   :', size(A3D_UGDS_cells%A_lmnt  )
!-----------------------------------------------------------------------
  write(*,*) 'nlmnt_np :', size(A3D_UGDS_dgs%nlmnt_np  )
  write(*,*) 'dg_data  :', size(A3D_UGDS_dgs%dg_data   )
  write(*,*) 'ndg_nlmnt:', size(A3D_UGDS_dgs%ndg_nlmnt )
  write(*,*) A3D_thrm_data%gamma, A3D_thrm_data%Rg
!-----------------------------------------------------------------------------------------------------------------------------------
  allocate(fwrk(Nlmnts))
  call cpu_time(cpu_time_s_0); wck_time_s_0=OMP_GET_WTIME()
  !-plot initial variables::to remove-----------------------------------------------------------------------------------------------
                          fwrk(1:Nlmnts:1)=A3D_vrbls(1:Nlmnts:1)%V%x
  call R3D_to_plots_2D_unstrcrd ( 013, 'Vxnp1_ini', 'write', 'vtk', 'Vx', 'Vx',&
                                  A3D_UGDS_cells%x_vrtx, A3D_UGDS_cells%np_nlmnt, fwrk, 'cell')
  !-----------------------------------------------------------------------
                          fwrk(1:Nlmnts:1)=A3D_vrbls(1:Nlmnts:1)%V%y
  call R3D_to_plots_2D_unstrcrd ( 013, 'Vynp1_ini', 'write', 'vtk', 'Vy', 'Vy',&
                                  A3D_UGDS_cells%x_vrtx, A3D_UGDS_cells%np_nlmnt, fwrk, 'cell')
  !-----------------------------------------------------------------------
                          fwrk(1:Nlmnts:1)=A3D_vrbls(1:Nlmnts:1)%p
  call R3D_to_plots_2D_unstrcrd ( 013, 'pnp1_ini', 'write', 'vtk', 'p', 'p',&
                                  A3D_UGDS_cells%x_vrtx, A3D_UGDS_cells%np_nlmnt, fwrk, 'cell')
  !-----------------------------------------------------------------------
                          fwrk(1:Nlmnts:1)=A3D_vrbls(1:Nlmnts:1)%rho
  call R3D_to_plots_2D_unstrcrd ( 013, 'rhonp1_ini', 'write', 'vtk', 'rho', 'rho',&
                                  A3D_UGDS_cells%x_vrtx, A3D_UGDS_cells%np_nlmnt, fwrk, 'cell')
  !-----------------------------------------------------------------------
                          fwrk(1:Nlmnts:1)=A3D_vrbls(1:Nlmnts:1)%T
  call R3D_to_plots_2D_unstrcrd ( 013, 'Tnp1_ini', 'write', 'vtk', 'T', 'T',&
                                  A3D_UGDS_cells%x_vrtx, A3D_UGDS_cells%np_nlmnt, fwrk, 'cell')
  !-----------------------------------------------------------------------------------------------------------------------------------
  do nlmnt = 1, Nlmnts, 1; if (isreallmnt(nlmnt)) then; fwrk(nlmnt)=0.
                                                  else; fwrk(nlmnt)=1; end if; end do
  call R3D_to_plots_2D_unstrcrd ( 013, 'bcs', 'write', 'vtk', 'bcs', 'bcs',&
                                  A3D_UGDS_cells%x_vrtx, A3D_UGDS_cells%np_nlmnt, fwrk, 'cell')
  call cpu_time(cpu_time_s  ); wck_time_s  =OMP_GET_WTIME()
  nprcdr=2
  A3D_prfrmnc(nprcdr)%cpu_time_s=A3D_prfrmnc(nprcdr)%cpu_time_s+(cpu_time_s-cpu_time_s_0)
  A3D_prfrmnc(nprcdr)%wck_time_s=A3D_prfrmnc(nprcdr)%wck_time_s+(wck_time_s-wck_time_s_0)
  A3D_prfrmnc(nprcdr)%N_times_called=A3D_prfrmnc(nprcdr)%N_times_called+1
  deallocate(fwrk)
!-----------------------------------------------------------------------------------------------------------------------------------
!-                ------------------------------------------------------------------------------------------------------------------
!-stencils_and_pR1------------------------------------------------------------------------------------------------------------------
!-                ------------------------------------------------------------------------------------------------------------------
!-----------------------------------------------------------------------------------------------------------------------------------
  A3D_runtime_NS2D%k_srdr=k_srdr; write(*,*) ' A3D_NS_2D: k_srdr = ', k_srdr
  A3D_runtime_NS2D%k_sch =k_sch ; write(*,*) ' A3D_NS_2D: k_sch  = ', k_sch
  A3D_runtime_NS2D%k_RS  =k_RS  ; write(*,*) ' A3D_NS_2D: k_RS   = ', k_RS
  A3D_runtime_NS2D%k_WENO=k_WENO; write(*,*) ' A3D_NS_2D: k_WENO = ', k_WENO
  A3D_runtime_NS2D%k_map =k_map ; write(*,*) ' A3D_NS_2D: k_map  = ', k_map
  k_vrbs=1
  k_safe=1
!-----------------------------------------------------------------------
  call cpu_time(cpu_time_s_0); wck_time_s_0=OMP_GET_WTIME()
  call A3D_2D_stencil ( k_vrbs, k_srdr, 1, 1, k_safe,&
                        A3D_UGDS_dgs%ndg_nlmnt, A3D_UGDS_dgs%dg_data, A3D_UGR1_2D_cells%nstncl, A3D_UGR1_2D_cells%A_lsq_cond_data)
  call cpu_time(cpu_time_s  ); wck_time_s  =OMP_GET_WTIME()
  nprcdr=3
  A3D_prfrmnc(nprcdr)%cpu_time_s=A3D_prfrmnc(nprcdr)%cpu_time_s+(cpu_time_s-cpu_time_s_0)
  A3D_prfrmnc(nprcdr)%wck_time_s=A3D_prfrmnc(nprcdr)%wck_time_s+(wck_time_s-wck_time_s_0)
  A3D_prfrmnc(nprcdr)%N_times_called=A3D_prfrmnc(nprcdr)%N_times_called+1
!-----------------------------------------------------------------------
  write(*,*) 'nstncl         :', size(A3D_UGR1_2D_cells%nstncl)!<-working_write
  write(*,*) 'A_lsq_cond_data:', size(A3D_UGR1_2D_cells%A_lsq_cond_data)!<-working_write
!-----------------------------------------------------------------------
  call cpu_time(cpu_time_s_0); wck_time_s_0=OMP_GET_WTIME()
  call R3D_2D_lsq_R1_coeffs (      0, k_sch, k_srdr, 1,&
                              A3D_UGR1_2D_cells%nstncl, A3D_UGDS_cells%np_nlmnt, A3D_UGDS_dgs%ndg_nlmnt, A3D_UGDS_dgs%dg_data,&
                              A3D_UGDS_cells%x_vrtx, A3D_UGDS_cells%x_lmnt,&
                              A3D_UGR1_2D_cells%p_R1_coeffs, A3D_UGR1_2D_cells%dx_lmnt, A3D_UGR1_2D_cells%A_lsq_cond_data)
  call cpu_time(cpu_time_s  ); wck_time_s  =OMP_GET_WTIME()
  nprcdr=4
  A3D_prfrmnc(nprcdr)%cpu_time_s=A3D_prfrmnc(nprcdr)%cpu_time_s+(cpu_time_s-cpu_time_s_0)
  A3D_prfrmnc(nprcdr)%wck_time_s=A3D_prfrmnc(nprcdr)%wck_time_s+(wck_time_s-wck_time_s_0)
  A3D_prfrmnc(nprcdr)%N_times_called=A3D_prfrmnc(nprcdr)%N_times_called+1
!-----------------------------------------------------------------------
  call cpu_time(cpu_time_s_0); wck_time_s_0=OMP_GET_WTIME()
  call A3D_2D_edge_Gpnts ( k_srdr, A3D_UGDS_dgs%dg_data, A3D_UGDS_cells%x_vrtx, A3D_UGDS_cells%x_lmnt, A3D_UGR1_2D_cells%Gpnt_xyzw)
  call cpu_time(cpu_time_s  ); wck_time_s  =OMP_GET_WTIME()
  nprcdr=5
  A3D_prfrmnc(nprcdr)%cpu_time_s=A3D_prfrmnc(nprcdr)%cpu_time_s+(cpu_time_s-cpu_time_s_0)
  A3D_prfrmnc(nprcdr)%wck_time_s=A3D_prfrmnc(nprcdr)%wck_time_s+(wck_time_s-wck_time_s_0)
  A3D_prfrmnc(nprcdr)%N_times_called=A3D_prfrmnc(nprcdr)%N_times_called+1
  Ndgs  = size(A3D_UGDS_dgs%dg_data )
  Nvrtcs= size(A3D_UGDS_cells%x_vrtx)
  Ncells= size(A3D_UGDS_cells%x_lmnt)
  Nlmnts= Ncells
  write(*,*) Ndgs, Nvrtcs, Ncells, size(A3D_UGR1_2D_cells%Gpnt_xyzw)                                                                 !<-working_write
!-----------------------------------------------------------------------------------------------------------------------------------!<-A3D::TODO::BCs_constrained_reconstruction
!-                                         -----------------------------------------------------------------------------------------!<-A3D::TODO::BCs_constrained_reconstruction
!-A3D::TODO::BCs_constrained_reconstruction-----------------------------------------------------------------------------------------!<-A3D::TODO::BCs_constrained_reconstruction
!-                                         -----------------------------------------------------------------------------------------!<-A3D::TODO::BCs_constrained_reconstruction
!-----------------------------------------------------------------------------------------------------------------------------------!<-A3D::TODO::BCs_constrained_reconstruction
! call A3D_2D_lsq_BC_coeffs (      0, k_sch, k_srdr, 1,&                                                                            !<-A3D::TODO::BCs_constrained_reconstruction
!                             A3D_UGR1_2D_cells%nstncl, A3D_UGDS_cells%np_nlmnt, A3D_UGDS_dgs%ndg_nlmnt, A3D_UGDS_dgs%dg_data,&     !<-A3D::TODO::BCs_constrained_reconstruction
!                             A3D_UGDS_cells%x_vrtx, A3D_UGDS_cells%x_lmnt,&                                                        !<-A3D::TODO::BCs_constrained_reconstruction
!                             p_RD_coeffs, p_RN_coeffs, xIlsqBClmnts,&                                                              !<-A3D::TODO::BCs_constrained_reconstruction
!                             A3D_UGR1_2D_cells%dx_lmnt, A3D_UGR1_2D_cells%A_lsq_cond_data,&                                        !<-A3D::TODO::BCs_constrained_reconstruction
!                             A3D_UGR1_2D_cells%Gpnt_xyzw, A3D_UGDS_2D_bndrs)                                                       !<-A3D::TODO::BCs_constrained_reconstruction
!-HACK_for_bndrlmnts----------------------------------------------------                                                            !<-A3D::TODO::BCs_constrained_reconstruction
! allocate(p_R1_coeffs(Nbndlmnts)); do nbndlmnt = 1, Nbndlmnts, 1; nlmnt=A3D_UGDS_2D_bndrs%nlmnt_nbndlmnt(nbndlmnt)                 !<-A3D::TODO::BCs_constrained_reconstruction
!                                   Ncs=size(A3D_UGR1_2D_cells%p_R1_coeffs(nlmnt)%coeffs)                                           !<-A3D::TODO::BCs_constrained_reconstruction
!                                   allocate(p_R1_coeffs(nbndlmnt)%coeffs(Ncs))                                                     !<-A3D::TODO::BCs_constrained_reconstruction
!                                   Ngbs_lmnt=size(A3D_UGR1_2D_cells%nstncl(nlmnt)%n)                                               !<-A3D::TODO::BCs_constrained_reconstruction
! do nc = 1, Ncs, 1; allocate(p_R1_coeffs(nbndlmnt)%coeffs(nc)%v(Ngbs_lmnt+1))                                                      !<-A3D::TODO::BCs_constrained_reconstruction
! p_R1_coeffs(nbndlmnt)%coeffs(nc)%v(1:Ngbs_lmnt:1)=A3D_UGR1_2D_cells%p_R1_coeffs(nlmnt)%coeffs(nc)%v(1:Ngbs_lmnt:1); end do        !<-A3D::TODO::BCs_constrained_reconstruction
! p_R1_coeffs(nbndlmnt)%coeffs(1)%v(Ngbs_lmnt+1)=1.                                                                                 !<-A3D::TODO::BCs_constrained_reconstruction
! do nc = 2, Ncs, 1; p_R1_coeffs(nbndlmnt)%coeffs(nc)%v(Ngbs_lmnt+1)=0.; end do                                                     !<-A3D::TODO::BCs_constrained_reconstruction
! do nc = 1, Ncs, 1; do ind= 1, Ngbs_lmnt, 1                                                                                        !<-A3D::TODO::BCs_constrained_reconstruction
! p_R1_coeffs(nbndlmnt)%coeffs(nc)%v(Ngbs_lmnt+1)=p_R1_coeffs(nbndlmnt)%coeffs(nc)%v(Ngbs_lmnt+1)&                                  !<-A3D::TODO::BCs_constrained_reconstruction
!                                        -A3D_UGR1_2D_cells%p_R1_coeffs(nlmnt)%coeffs(nc)%v(ind); end do; end do                    !<-A3D::TODO::BCs_constrained_reconstruction
! end do                                                                                                                            !<-A3D::TODO::BCs_constrained_reconstruction
!::-->  do ind = 1, Ngbs_lsq, 1; nlmnt_ngb = nstncl(nlmnt)%n(ind)!-----------------------------------------------------             !<-A3D::TODO::BCs_constrained_reconstruction
!::-->  do nc  = 1, Ncs, 1!-----------------------------------------------------------------------------------------   !            !<-A3D::TODO::BCs_constrained_reconstruction
!::-->  apR1(nc) = apR1(nc)+p_R1_coeffs(nlmnt)%coeffs(nc)%v(ind)*(uc(nlmnt_ngb)-uc(nlmnt))                          !  !            !<-A3D::TODO::BCs_constrained_reconstruction
!::-->  end do; end do!<-----------------------------------------------------------------------------------------------             !<-A3D::TODO::BCs_constrained_reconstruction
!-----------------------------------------------------------------------------------------------------------------------------------
!-               -------------------------------------------------------------------------------------------------------------------
!-A3D::monitoring-------------------------------------------------------------------------------------------------------------------
!-               -------------------------------------------------------------------------------------------------------------------
!-----------------------------------------------------------------------------------------------------------------------------------
  if(it==0) then;       open (unit=077,file='A3D_NS_2D_error'                  ); else
  inquire(file='A3D_NS_2D_error',exist=inq_exists); write(*,*) ' A3D_NS_2D_error = ', inq_exists
  if (inq_exists) then; open (unit=077,file='A3D_NS_2D_error',position='append')
                  else; open (unit=077,file='A3D_NS_2D_error'                  ); end if; end if
!=====>A3D::wave===>!-----------------------------------------------------------------------------------------------------------------------------------
!=====>A3D::wave===>!-   -------------------------------------------------------------------------------------------------------------------------------
!=====>A3D::wave===>!-ADR-------------------------------------------------------------------------------------------------------------------------------
!=====>A3D::wave===>!-   -------------------------------------------------------------------------------------------------------------------------------
!=====>A3D::wave===>!-----------------------------------------------------------------------------------------------------------------------------------
!=====>A3D::wave===>  ax = cos(alpha_inf); ay = sin(alpha_inf)
!=====>A3D::wave===>  ax = 1.            ; ay = 1.
!=====>A3D::wave===>  allocate (vold(Nlmnts)); do nlmnt = 1, Nlmnts, 1
!=====>A3D::wave===>                           vold(nlmnt) = sin(atan2(0.,-1.)*(A3D_UGDS_cells%x_lmnt(nlmnt)%x+A3D_UGDS_cells%x_lmnt(nlmnt)%y)); end do
!=====>A3D::wave===>  allocate (vnew(Nlmnts)); do nlmnt = 1, Nlmnts, 1; vnew(nlmnt) = vold(nlmnt); end do
!=====>A3D::wave===>  allocate (dtAD(Nlmnts)); do nlmnt = 1, Nlmnts, 1; dtAD(nlmnt) = A3D_UGDS_cells%d_lmnt(nlmnt)/sqrt(ax**2+ay**2); end do
!-----------------------------------------------------------------------------------------------------------------------------------!
!-                                                     -----------------------------------------------------------------------------!
!-Read or creation of the grid-reordering file grp_lmnt-----------------------------------------------------------------------------!
!-                                                     -----------------------------------------------------------------------------!
!-----------------------------------------------------------------------------------------------------------------------------------!
  if (k_implct == 1 .and. (tm_intgrtn == 'BLU' .or. tm_intgrtn == 'FLU')) then                                                      !
  inquire(file='grp_lmnt',exist=inq_exists)                                                                                         !
  if (inq_exists) then!------------------------------------------------------------------------------------------------!            !
  if (allocated(grp_lmnt)) deallocate(grp_lmnt);allocate(grp_lmnt(Nlmnts))                                             !            !
  write(*,*) "Reading existing reordered grid ... "                                                                    !            !
  open  (unit=078,  file='grp_lmnt', form='unformatted')                                                               !            !
  read  (unit=078) (grp_lmnt(nlmnt), nlmnt = 1, Nlmnts, 1)                                                             !            !
  close (unit=078)                                                                                                     !            !
  N_levels = maxval(grp_lmnt(:))                                                                                       !            !
  if (allocated(nlmnt_lvl)) deallocate(nlmnt_lvl);allocate(nlmnt_lvl(N_levels))                                        !            !
  do n_level = 1, N_levels!-----------------------------------------------------------------------------!              !            !
  Nlmnts_lvl = 0                                                                                        !              !            !
  do nlmnt = 1, Nlmnts!-----------------------------------------------------------------!               !              !            !
  if (grp_lmnt(nlmnt) == n_level) Nlmnts_lvl = Nlmnts_lvl + 1                           !               !              !            !
  end do!-------------------------------------------------------------------------------                !              !            !
  allocate(nlmnt_lvl(n_level)%n(Nlmnts_lvl))                                                            !              !            !
  ind = 0                                                                                               !              !            !
  do nlmnt = 1, Nlmnts!-----------------------------------------------------------------!               !              !            !
  if (grp_lmnt(nlmnt) == n_level) then                                                  !               !              !            !
  ind = ind + 1                                                                         !               !              !            !
  nlmnt_lvl(n_level)%n(ind) = nlmnt                                                     !               !              !            !
  end if                                                                                !               !              !            !
  end do!-------------------------------------------------------------------------------                !              !            !
  end do!-----------------------------------------------------------------------------------------------               !            !
  write(*,*) "Reordered grid read with success ... "                                                                   !            !
  else                                                                                                                 !            !
  write(*,*) "Creating reordered grid ... "                                                                            !            !
  call reorder_unstrctrd(A3D_UGDS_dgs%ndg_nlmnt, A3D_UGDS_dgs%dg_data, grp_lmnt, nlmnt_lvl)                            !            !
  open  (unit=078,  file='grp_lmnt', form='unformatted')                                                               !            !
  write (unit=078) (grp_lmnt(nlmnt), nlmnt = 1, Nlmnts, 1)                                                             !            !
  close (unit=078)                                                                                                     !            !
  end if!--------------------------------------------------------------------------------------------------------------             !
                                                                                                                                    !
  allocate(grp_lmnt_real(size(grp_lmnt)))                                                                                           !
  grp_lmnt_real(:) = real(grp_lmnt(:))                                                                                              !
  call R3D_to_plots_2D_unstrcrd ( 014, 'reordered_grid', 'write', 'vtk', 'result', 'group', &                                       !
                  A3D_UGDS_cells%x_vrtx, A3D_UGDS_cells%np_nlmnt, grp_lmnt_real, 'cell')                                            !
  end if                                                                                                                            !
  !---------------------------------------------------------------------------------------------------------------------------------
    
!   if (allocated(dtlcl_nml)) deallocate(dtlcl_nml); allocate(dtlcl_nml(Nlmnts))
!   if (allocated(dtlcl_str)) deallocate(dtlcl_str); allocate(dtlcl_str(Nlmnts))
!-explore phantom cells::to remove--------------------------------------------------------------------------------------------------
! do nbndphntm = 1, Nbndphntms, 1; nphlmnt  = A3D_UGDS_2D_bndrs% nlmnt_nbndphntm(nbndphntm)
!                                  nbnddg   = A3D_UGDS_2D_bndrs%nbnddg_nbndphntm(nbndphntm)                                       
!                                  ndg      = A3D_UGDS_2D_bndrs%ndg_nbnddg  (nbnddg)                                              
!                                  kBC      = A3D_UGDS_2D_bndrs%kBC_dg      (nbnddg)                                              
!                                  nlmnt    = A3D_UGDS_2D_bndrs%nlmnt_nbnddg(nbnddg)                                              
!                               nbndlmnt    = A3D_UGDS_2D_bndrs%nbndlmnt_nbnddg(nbnddg)                                           
!                                  nBC      = A3D_UGDS_2D_bndrs%nBCdata        (nbnddg)  
! write(*,*) A3D_UGDS_cells%x_lmnt(nlmnt)%x, A3D_UGDS_cells%x_lmnt(nphlmnt)%x
! end do
!-----------------------------------------------------------------------------------------------------------------------------------
!-              --------------------------------------------------------------------------------------------------------------------
!-iteration_loop--------------------------------------------------------------------------------------------------------------------
!-              --------------------------------------------------------------------------------------------------------------------
!-----------------------------------------------------------------------------------------------------------------------------------
  call cpu_time(cpu_time_s)
!-->A3D::cpu  if (it == 0) cpuit0 = -cpu_time_s
!-->A3D::cpu  if (it /= 0) cpuit0 = cpuit(it)-cpu_time_s
  if(it==0) then
  A3D_runtime_NS2D%cpu_time_s_it0 = 0.
  A3D_runtime_NS2D%wck_time_s_it0 = 0.
  else
  A3D_runtime_NS2D%cpu_time_s_it0 = A3D_runtime_NS2D%cpu_time_s
  A3D_runtime_NS2D%wck_time_s_it0 = A3D_runtime_NS2D%wck_time_s
  end if
  A3D_runtime_NS2D%cpu_time_s_it0 = A3D_runtime_NS2D%cpu_time_s_it0-cpu_time_s
  A3D_runtime_NS2D%wck_time_s_it0 = A3D_runtime_NS2D%wck_time_s_it0-OMP_GET_WTIME()
!-----------------------------------------------------------------------
  do n_it = 1, N_it_s, 1; it = it+1
  call cpu_time(cpu_time_s_0); wck_time_s_0=OMP_GET_WTIME()
  call       A3D_NS_v_to_u ( A3D_vrbls, A3D_uold, A3D_thrm_data, A3Dr_mssgs)
  call       A3D_NS_v_to_u ( A3D_vrbls, A3D_unew, A3D_thrm_data, A3Dr_mssgs)
  call cpu_time(cpu_time_s  ); wck_time_s  =OMP_GET_WTIME()
  nprcdr=6
  A3D_prfrmnc(nprcdr)%cpu_time_s=A3D_prfrmnc(nprcdr)%cpu_time_s+(cpu_time_s-cpu_time_s_0)
  A3D_prfrmnc(nprcdr)%wck_time_s=A3D_prfrmnc(nprcdr)%wck_time_s+(wck_time_s-wck_time_s_0)
  A3D_prfrmnc(nprcdr)%N_times_called=A3D_prfrmnc(nprcdr)%N_times_called+2
!=====>A3D::wave===>  do nlmnt = 1, Nlmnts, 1; vold(nlmnt) = vnew(nlmnt); end do
! rdctn_u   =0.
! error_uold=0.
  error_u_old=0.
!=dual time-stepping disabled for steady-state problems================================================
!    do m_it = 1, M_it_s, 1
!======================================================================================================
  call cpu_time(cpu_time_s_0); wck_time_s_0=OMP_GET_WTIME()
  call A3D_sch_NS_2D ( A3Dr_mssgs, A3D_runtime_NS2D, A3D_thrm_data,&
                       A3D_UGDS_cells, A3D_UGDS_dgs, A3D_UGR1_2D_cells, A3D_vrbls, A3D_Rvs, A3D_UGDS_2D_bndrs, dtlcl, Smaxdg, xnueqdg)!,&
  call cpu_time(cpu_time_s  ); wck_time_s  =OMP_GET_WTIME()
  nprcdr=7
  A3D_prfrmnc(nprcdr)%cpu_time_s=A3D_prfrmnc(nprcdr)%cpu_time_s+(cpu_time_s-cpu_time_s_0)
  A3D_prfrmnc(nprcdr)%wck_time_s=A3D_prfrmnc(nprcdr)%wck_time_s+(wck_time_s-wck_time_s_0)
  A3D_prfrmnc(nprcdr)%N_times_called=A3D_prfrmnc(nprcdr)%N_times_called+1
!=====>A3D::wave===>  call       A3D_sch_AD_2D ( A3Dr_mssgs, A3D_runtime_NS2D, A3D_thrm_data,&
!=====>A3D::wave===>                             A3D_UGDS_cells, A3D_UGDS_dgs, A3D_UGR1_2D_cells, vnew, divF)
!-----------------------------------------------------------------------------------------------------------------------------------
  dtlcl(1:Nlmnts:1)=CFL*dtlcl(1:Nlmnts:1)
  if(k_implct==0) then
  write(*,*) "explicit schemes currently disabled, try k_implct = 1 !!!!!!!!!!"
  stop
!-RK--------------------------------------------------------------------------------------------------------------------------------
  if (tm_intgrtn=='RK') then
!$OMP PARALLEL DO SCHEDULE(STATIC) DEFAULT(PRIVATE)                                                                     &         !-|| A3D_NS_2D
!$OMP                              SHARED( Nlmnts, A3D_unew, A3D_uold, A3D_Rvs, dtlcl, CFL, CFL_str, isreallmnt, m_it)            !-|| A3D_NS_2D
  do nlmnt = 1, Nlmnts, 1
  if(isreallmnt(nlmnt)) then
! fctr          = -dtlcl(nlmnt)/(1.+CFL_str/CFL)
! dnm           = 1./(dtlcl(nlmnt)/CFL_str*CFL)
! A3D_unew(nlmnt)%rho    = A3D_unew(nlmnt)%rho    +fctr*((A3D_unew(nlmnt)%rho   -A3D_uold(nlmnt)%rho   )*dnm+A3D_Rvs(nlmnt)%rho   )
! A3D_unew(nlmnt)%rhoV%x = A3D_unew(nlmnt)%rhoV%x +fctr*((A3D_unew(nlmnt)%rhoV%x-A3D_uold(nlmnt)%rhoV%x)*dnm+A3D_Rvs(nlmnt)%rhoV%x)
! A3D_unew(nlmnt)%rhoV%y = A3D_unew(nlmnt)%rhoV%y +fctr*((A3D_unew(nlmnt)%rhoV%y-A3D_uold(nlmnt)%rhoV%y)*dnm+A3D_Rvs(nlmnt)%rhoV%y)
! A3D_unew(nlmnt)%rhoet  = A3D_unew(nlmnt)%rhoet  +fctr*((A3D_unew(nlmnt)%rhoet -A3D_uold(nlmnt)%rhoet )*dnm+A3D_Rvs(nlmnt)%rhoet )
  A3D_unew(nlmnt)%rho    = A3D_uold(nlmnt)%rho    -real(m_it/2.)*dtlcl(nlmnt)*A3D_Rvs(nlmnt)%rho
  A3D_unew(nlmnt)%rhoV%x = A3D_uold(nlmnt)%rhoV%x -real(m_it/2.)*dtlcl(nlmnt)*A3D_Rvs(nlmnt)%rhoV%x
  A3D_unew(nlmnt)%rhoV%y = A3D_uold(nlmnt)%rhoV%y -real(m_it/2.)*dtlcl(nlmnt)*A3D_Rvs(nlmnt)%rhoV%y
  A3D_unew(nlmnt)%rhoet  = A3D_uold(nlmnt)%rhoet  -real(m_it/2.)*dtlcl(nlmnt)*A3D_Rvs(nlmnt)%rhoet
  end if
  end do
!$OMP END PARALLEL DO                                                                                                             !-|| A3D_NS_2D
!=====>A3D::wave===>  do nlmnt = 1, Nlmnts, 1
!=====>A3D::wave===>  if(isreallmnt(nlmnt)) then
!=====>A3D::wave===>! fctr          = -dtAD(nlmnt)/(1.+CFL_str/CFL)
!=====>A3D::wave===>! dnm           = 1./(dtAD(nlmnt)/CFL_str*CFL)
!=====>A3D::wave===>! vnew(nlmnt) = vnew(nlmnt)+fctr*((vnew(nlmnt)-vold(nlmnt))*dnm+divF(nlmnt))
!=====>A3D::wave===>! vnew(nlmnt) = vold(nlmnt)-real(m_it/2.)*minval(dtAD)/5.*divF(nlmnt)
!=====>A3D::wave===>  vnew(nlmnt) = vold(nlmnt)-real(m_it/2.)*dtAD(nlmnt)/5.*divF(nlmnt)
!=====>A3D::wave===>! write(*,*) nlmnt, vnew(nlmnt), vold(nlmnt), minval(dtAD), divF(nlmnt)
!=====>A3D::wave===>  end if
!=====>A3D::wave===>  end do
! stop
  end if
!-eDTS------------------------------------------------------------------------------------------------------------------------------
  if (tm_intgrtn=='eDTS') then!------------------------------------------------------------------------------------------------
!$OMP PARALLEL DO SCHEDULE(STATIC) DEFAULT(PRIVATE)                                                                     &      !  !-|| A3D_NS_2D
!$OMP                              SHARED( Nlmnts, A3D_unew, A3D_uold, A3D_Rvs, dtlcl, CFL, CFL_str, isreallmnt, m_it)         !  !-|| A3D_NS_2D
  do nlmnt = 1, Nlmnts, 1                                                                                                      !
  if(isreallmnt(nlmnt)) then!------------------------------------------------------------------------------------------------  !
  fctr          = -dtlcl(nlmnt)/(1.+CFL_str/CFL)                                                                             ! !
  dnm           = 1./(dtlcl(nlmnt)/CFL_str*CFL)                                                                              ! !
  A3D_unew(nlmnt)%rho    = A3D_unew(nlmnt)%rho    +fctr*((A3D_unew(nlmnt)%rho   -A3D_uold(nlmnt)%rho   )*dnm+A3D_Rvs(nlmnt)%rho   )
  A3D_unew(nlmnt)%rhoV%x = A3D_unew(nlmnt)%rhoV%x +fctr*((A3D_unew(nlmnt)%rhoV%x-A3D_uold(nlmnt)%rhoV%x)*dnm+A3D_Rvs(nlmnt)%rhoV%x)
  A3D_unew(nlmnt)%rhoV%y = A3D_unew(nlmnt)%rhoV%y +fctr*((A3D_unew(nlmnt)%rhoV%y-A3D_uold(nlmnt)%rhoV%y)*dnm+A3D_Rvs(nlmnt)%rhoV%y)
  A3D_unew(nlmnt)%rhoet  = A3D_unew(nlmnt)%rhoet  +fctr*((A3D_unew(nlmnt)%rhoet -A3D_uold(nlmnt)%rhoet )*dnm+A3D_Rvs(nlmnt)%rhoet )
  end if!--------------------------------------------------------------------------------------------------------------------  !
  end do!<---------------------------------------------------------------------------------------------------------------------
!$OMP END PARALLEL DO                                                                                                             !-|| A3D_NS_2D
  end if
!-----------------------------------------------------------------------------------------------------------------------------------
  end if
!===A3D::implct===>  if(k_implct==1) then
!===A3D::implct===>  JGS_it_s=1
!===A3D::implct===>  call A3D_JGS_NS_2D ( A3Dr_mssgs, A3D_runtime_NS2D, A3D_thrm_data,&
!===A3D::implct===>                       A3D_UGDS_cells, A3D_UGDS_dgs, A3D_UGR1_2D_cells, A3D_UGDS_2D_bndrs, A3D_uold, A3D_Rvs,&
!===A3D::implct===>                       du, dtlcl, rstr, JGS_it_s)
!===A3D::implct===>!=====>!$OMP PARALLEL DO SCHEDULE(STATIC) DEFAULT(PRIVATE)                                                                     &         !-|| A3D_NS_2D
!===A3D::implct===>!=====>!$OMP                              SHARED( Nlmnts, A3D_unew, A3D_uold, A3D_Rvs, dtlcl, CFL, CFL_str)                              !-|| A3D_NS_2D
!===A3D::implct===>  do nlmnt = 1, Nlmnts, 1
!===A3D::implct===>  A3D_unew(nlmnt)%rho    = A3D_uold(nlmnt)%rho   +du(nlmnt)%rho
!===A3D::implct===>  A3D_unew(nlmnt)%rhoV%x = A3D_uold(nlmnt)%rhoV%x+du(nlmnt)%rhoV%x
!===A3D::implct===>  A3D_unew(nlmnt)%rhoV%y = A3D_uold(nlmnt)%rhoV%y+du(nlmnt)%rhoV%y
!===A3D::implct===>  A3D_unew(nlmnt)%rhoet  = A3D_uold(nlmnt)%rhoet +du(nlmnt)%rhoet
!===A3D::implct===>  end do
!===A3D::implct===>!=====>!$OMP END PARALLEL DO                                                                                                             !-|| A3D_NS_2D
!===A3D::implct===>  end if
!-----------------------------------------------------------------------------------------------------------------------------------
!-                                      --------------------------------------------------------------------------------------------
!-implicit iterations (double time-step)--------------------------------------------------------------------------------------------
!-                                      --------------------------------------------------------------------------------------------
!-----------------------------------------------------------------------------------------------------------------------------------
  if(k_implct==1) then
!     ! For STEADY STATE FLOW: enforce Newton method on the inner loop
!     CFL_str     = CFL*1.0E10
!     !$OMP PARALLEL WORKSHARE
!     dtlcl_nml(:)=            dtlcl(:)
!     dtlcl_str(:)=CFL_str/CFL*dtlcl(:)
!     !$OMP END PARALLEL WORKSHARE
!     !$OMP PARALLEL DO SCHEDULE(STATIC) DEFAULT(PRIVATE) SHARED(Nlmnts, A3D_unew, A3D_uold, A3D_Rvs, dtlcl_nml, isreallmnt)      
!     do nlmnt = 1, Nlmnts, 1; if(isreallmnt(nlmnt)) then
!         A3D_Rvs(nlmnt)%rho    = (A3D_unew(nlmnt)%rho    - A3D_uold(nlmnt)%rho)   /dtlcl_nml(nlmnt) + A3D_Rvs(nlmnt)%rho
!         A3D_Rvs(nlmnt)%rhoV%x = (A3D_unew(nlmnt)%rhoV%x - A3D_uold(nlmnt)%rhoV%x)/dtlcl_nml(nlmnt) + A3D_Rvs(nlmnt)%rhoV%x
!         A3D_Rvs(nlmnt)%rhoV%y = (A3D_unew(nlmnt)%rhoV%y - A3D_uold(nlmnt)%rhoV%y)/dtlcl_nml(nlmnt) + A3D_Rvs(nlmnt)%rhoV%y
!         A3D_Rvs(nlmnt)%rhoet  = (A3D_unew(nlmnt)%rhoet  - A3D_uold(nlmnt)%rhoet) /dtlcl_nml(nlmnt) + A3D_Rvs(nlmnt)%rhoet
!     end if; end do
!     !$OMP END PARALLEL DO    
    VxBC  = A3D_UGDS_2D_bndrs%BCdata(nBC)%vrbls%V%x                                                                               
    VyBC  = A3D_UGDS_2D_bndrs%BCdata(nBC)%vrbls%V%y                                                                               
   rhoBC  = A3D_UGDS_2D_bndrs%BCdata(nBC)%vrbls%rho                                                                                
     pBC  = A3D_UGDS_2D_bndrs%BCdata(nBC)%vrbls%p                                                                                 
     TBC  = A3D_UGDS_2D_bndrs%BCdata(nBC)%vrbls%T
        
    if (tm_intgrtn == 'Jacobi') call jacobi_unstrctrd  (Lin_it_s, A3D_thrm_data, A3D_UGDS_cells, A3D_UGDS_dgs, A3D_UGR1_2D_cells, A3D_UGDS_2D_bndrs, isreallmnt,&
                                                        A3D_Rvs, A3D_vrbls, A3D_uold, A3D_unew, xnueqdg, Smaxdg, dtlcl, rhoBC, pBC, VxBC, VyBC, TBC) 
                                                
    if (tm_intgrtn == 'BLU')    call blu_sgs_unstrctrd (Lin_it_s, omg, A3D_thrm_data, A3D_UGDS_cells, A3D_UGDS_dgs, A3D_UGR1_2D_cells, A3D_UGDS_2D_bndrs, isreallmnt,&
                                                        A3D_Rvs, A3D_vrbls, A3D_uold, A3D_unew, xnueqdg, Smaxdg, dtlcl, grp_lmnt, nlmnt_lvl, rhoBC, pBC, VxBC, VyBC, TBC)  

    if (tm_intgrtn == 'FLU')    call flu_sgs_unstrctrd (A3D_thrm_data, A3D_UGDS_cells, A3D_UGDS_dgs, A3D_UGR1_2D_cells, A3D_UGDS_2D_bndrs, isreallmnt,&
                                                        A3D_Rvs, A3D_vrbls, A3D_uold, A3D_unew, xnueqdg, Smaxdg, dtlcl, grp_lmnt, nlmnt_lvl, rhoBC, pBC, VxBC, VyBC, TBC)  
  end if
  
  call cpu_time(cpu_time_s_0); wck_time_s_0=OMP_GET_WTIME()
  call       A3D_NS_u_to_v ( A3D_unew, A3D_vrbls, A3D_thrm_data, A3Dr_mssgs)
  call cpu_time(cpu_time_s  ); wck_time_s  =OMP_GET_WTIME()
  nprcdr=1
  A3D_prfrmnc(nprcdr)%cpu_time_s=A3D_prfrmnc(nprcdr)%cpu_time_s+(cpu_time_s-cpu_time_s_0)
  A3D_prfrmnc(nprcdr)%wck_time_s=A3D_prfrmnc(nprcdr)%wck_time_s+(wck_time_s-wck_time_s_0)
  A3D_prfrmnc(nprcdr)%N_times_called=A3D_prfrmnc(nprcdr)%N_times_called+1
!-----------------------------------------------------------------------------------------------------------------------------------
!-         -------------------------------------------------------------------------------------------------------------------------
!-apply_BCs-------------------------------------------------------------------------------------------------------------------------
!-         -------------------------------------------------------------------------------------------------------------------------
!-----------------------------------------------------------------------------------------------------------------------------------
  Nbnddgs    = size(A3D_UGDS_2D_bndrs%kBC_dg         )
  Nbndlmnts  = size(A3D_UGDS_2D_bndrs%nlmnt_nbndlmnt )
  Nbndphntms = size(A3D_UGDS_2D_bndrs%nlmnt_nbndphntm)
!$OMP PARALLEL DO SCHEDULE(STATIC) DEFAULT(PRIVATE)                                                                     &         !-|| A3D_NS_2D
!$OMP                              SHARED( Nbnddgs, Nbndlmnts, Nbndphntms, A3D_UGDS_2D_bndrs, A3D_UGR1_2D_cells,        &         !-|| A3D_NS_2D
!$OMP                                      A3D_UGDS_cells, A3D_vrbls, gamma, Rg)                                                  !-|| A3D_NS_2D
  do nbndphntm = 1, Nbndphntms, 1; nphlmnt  = A3D_UGDS_2D_bndrs% nlmnt_nbndphntm(nbndphntm)!--------------------------------------
                                   nbnddg   = A3D_UGDS_2D_bndrs%nbnddg_nbndphntm(nbndphntm)                                       !
                                   ndg      = A3D_UGDS_2D_bndrs%ndg_nbnddg  (nbnddg)                                              !
                                   kBC      = A3D_UGDS_2D_bndrs%kBC_dg      (nbnddg)                                              !
                                   nlmnt    = A3D_UGDS_2D_bndrs%nlmnt_nbnddg(nbnddg)                                              !
                                nbndlmnt    = A3D_UGDS_2D_bndrs%nbndlmnt_nbnddg(nbnddg)                                           !
                                   nBC      = A3D_UGDS_2D_bndrs%nBCdata        (nbnddg)                                           !
  xnxn=sqrt(A3D_UGR1_2D_cells%Gpnt_xyzw(ndg)%dSGp(1)%x**2+A3D_UGR1_2D_cells%Gpnt_xyzw(ndg)%dSGp(1)%y**2)                          !
  sgn = sign(1.,A3D_UGR1_2D_cells%Gpnt_xyzw(ndg)%dSGp(1)%x*(A3D_UGDS_cells%x_lmnt(nlmnt)%x-A3D_UGDS_cells%x_lmnt(nphlmnt)%x)&     !
               +A3D_UGR1_2D_cells%Gpnt_xyzw(ndg)%dSGp(1)%y*(A3D_UGDS_cells%x_lmnt(nlmnt)%y-A3D_UGDS_cells%x_lmnt(nphlmnt)%y))     !
  xn%x =sgn*A3D_UGR1_2D_cells%Gpnt_xyzw(ndg)%dSGp(1)%x/xnxn                                                                       !
  xn%y =sgn*A3D_UGR1_2D_cells%Gpnt_xyzw(ndg)%dSGp(1)%y/xnxn                                                                       !
!-----------------------------------------------------------------------                                                          !
!-                     -------------------------------------------------                                                          !
!-kBC=0::adiabatic_wall-------------------------------------------------                                                          !
!-                     -------------------------------------------------                                                          !
!-----------------------------------------------------------------------                                                          !
  if(kBC== 0) then!-----------------------------------------------------------------------------------------------------------    !
  rho= A3D_vrbls(nlmnt)%rho                                                                                                   !   !
  p  = A3D_vrbls(nlmnt)%p                                                                                                     !   !
  Vx =-A3D_vrbls(nlmnt)%V%x                                                                                                   !   !
  Vy =-A3D_vrbls(nlmnt)%V%y                                                                                                   !   !
  T   = p/Rg/rho                                                                                                              !   !
!=====>A3D::wave===>  vph = vnew(nlmnt)                                                                                                              !   !
  end if!<--------------------------------------------------------------------------------------------------------------------    !
!-----------------------------------------------------------------------                                                          !
!-                                   -----------------------------------                                                          !
!-kBC=40::Riemann_invariants@infinity-----------------------------------                                                          !
!-                                   -----------------------------------                                                          !
!-----------------------------------------------------------------------                                                          !
  if(kBC==40) then!-------------------------------------------------------------------------------------------------------------  !
  Vnlmnt = A3D_vrbls(nlmnt)%V%x *xn%x+A3D_vrbls(nlmnt)%V%y *xn%y                                                                ! !
  Vpxlmnt= A3D_vrbls(nlmnt)%V%x -Vnlmnt*xn%x                                                                                    ! !
  Vpylmnt= A3D_vrbls(nlmnt)%V%y -Vnlmnt*xn%y                                                                                    ! !
   VxBC  = A3D_UGDS_2D_bndrs%BCdata(nBC)%vrbls%V%x                                                                              ! !
   VyBC  = A3D_UGDS_2D_bndrs%BCdata(nBC)%vrbls%V%y                                                                              ! !
  rhoBC  = A3D_UGDS_2D_bndrs%BCdata(nBC)%vrbls%rho                                                                              ! ! 
    pBC  = A3D_UGDS_2D_bndrs%BCdata(nBC)%vrbls%p                                                                                ! !
    TBC  = A3D_UGDS_2D_bndrs%BCdata(nBC)%vrbls%T                                                                                ! !
   VnBC  = VxBC*xn%x+VyBC*xn%y                                                                                                  ! !
  VpxBC  = VxBC-VnBC*xn%x                                                                                                       ! !
  VpyBC  = VyBC-VnBC*xn%y                                                                                                       ! !
  Riem_i = VnBC  +2./(gamma-1.)*sqrt(gamma*pBC/rhoBC)                                                                           ! !
!-->OLD VnBC   = A3D_UGDS_2D_bndrs%uBC*xn%x+A3D_UGDS_2D_bndrs%vBC*xn%y                                                                ! !
!-->OLD VpxBC  = A3D_UGDS_2D_bndrs%uBC-VnBC*xn%x                                                                                      ! !
!-->OLD VpyBC  = A3D_UGDS_2D_bndrs%vBC-VnBC*xn%y                                                                                      ! !
!-->OLD Riem_i =VnBC  +2./(gamma-1.)*sqrt(gamma*A3D_UGDS_2D_bndrs%pBC/A3D_UGDS_2D_bndrs%rhoBC)                                        ! !
  Riem_o =Vnlmnt-2./(gamma-1.)*sqrt(gamma*A3D_vrbls(nlmnt)%p   /A3D_vrbls(nlmnt)%rho   )                                        ! !
  Vn     =(Riem_i+Riem_o)/2.                                                                                                    ! !
  T      =((gamma-1.)/4.*(Riem_i-Riem_o))**2/gamma/Rg                                                                           ! !
  if (Vnlmnt>=0.) then!-inflow------------------------------------------------------------------------------------------------  ! !
  rho=rhoBC*(T/TBC)**(   1./(gamma-1.))                                                                                       ! ! !
  p  =  pBC*(T/TBC)**(gamma/(gamma-1.))                                                                                       ! ! !
  Vx =VpxBC+Vn*xn%x                                                                                                           ! ! !
  Vy =VpyBC+Vn*xn%y                                                                                                           ! ! !
  else!-outflow---------------------------------------------------------------------------------------------------------------  ! !
  rho=A3D_vrbls(nlmnt)%rho*(T/A3D_vrbls(nlmnt)%T)**(   1./(gamma-1.))                                                         ! ! !
  p  =A3D_vrbls(nlmnt)%p  *(T/A3D_vrbls(nlmnt)%T)**(gamma/(gamma-1.))                                                         ! ! !
  Vx =Vpxlmnt+Vn*xn%x                                                                                                         ! ! !
  Vy =Vpylmnt+Vn*xn%y                                                                                                         ! ! !
  end if!<--------------------------------------------------------------------------------------------------------------------  ! !
! write(*,*) kBC, nbndphntm, nlmnt, nphlmnt, xn%x, xn%y, A3D_UGDS_cells%x_lmnt(nlmnt)%x, A3D_UGDS_cells%x_lmnt(nlmnt)%y,&       ! !
!            Vnlmnt, p, rho, T, Vn, Vx, Vy, VnBC                                                                                ! !
!=====>A3D::wave===>  an = ax*xn%x+ay*xn%y
!=====>A3D::wave===>  if(an>=0.) then; vph = sin(atan2(0.,-1.)*((A3D_UGDS_cells%x_lmnt(nphlmnt)%x-ax*real(it)*maxval(dtAD)/5.)&
!=====>A3D::wave===>                                           +(A3D_UGDS_cells%x_lmnt(nphlmnt)%y-ay*real(it)*maxval(dtAD)/5.)))
!=====>A3D::wave===>             else; vph = vnew(nlmnt); end if                                                      
  end if!<----------------------------------------------------------------------------------------------------------------------  !
  A3D_vrbls(nphlmnt)%rho = rho                                                                                                    !
  A3D_vrbls(nphlmnt)%V%x = Vx                                                                                                     !
  A3D_vrbls(nphlmnt)%V%y = Vy                                                                                                     !
  A3D_vrbls(nphlmnt)%p   = p                                                                                                      !
  A3D_vrbls(nphlmnt)%T   = T                                                                                                      !
!=====>A3D::wave===>  vnew(nphlmnt) = vph
  end do!<------------------------------------------------------------------------------------------------------------------------
!$OMP END PARALLEL DO                                                                                                             !-|| A3D_NS_2D
!-----------------------------------------------------------------------------------------------------------------------------------
  call cpu_time(cpu_time_s_0); wck_time_s_0=OMP_GET_WTIME()
  call       A3D_NS_v_to_u ( A3D_vrbls, A3D_unew, A3D_thrm_data, A3Dr_mssgs)
  call cpu_time(cpu_time_s  ); wck_time_s  =OMP_GET_WTIME()
  nprcdr=6
  A3D_prfrmnc(nprcdr)%cpu_time_s=A3D_prfrmnc(nprcdr)%cpu_time_s+(cpu_time_s-cpu_time_s_0)
  A3D_prfrmnc(nprcdr)%wck_time_s=A3D_prfrmnc(nprcdr)%wck_time_s+(wck_time_s-wck_time_s_0)
  A3D_prfrmnc(nprcdr)%N_times_called=A3D_prfrmnc(nprcdr)%N_times_called+1
!-NaN detection::to remove----------------------------------------------------------------------------------------------------------
  DO nlmnt = 1, Nlmnts, 1
    IF (ISNAN(A3D_unew(nlmnt)%rho))    write(*,*) 'rho   in element #', nlmnt, 'is NaN !!!'
    IF (ISNAN(A3D_unew(nlmnt)%rhoV%x)) write(*,*) 'rhoVx in element #', nlmnt, 'is NaN !!!'
    IF (ISNAN(A3D_unew(nlmnt)%rhoV%y)) write(*,*) 'rhoVy in element #', nlmnt, 'is NaN !!!'
    IF (ISNAN(A3D_unew(nlmnt)%rhoet))  write(*,*) 'rhoet in element #', nlmnt, 'is NaN !!!'
  END DO       
  call cpu_time(cpu_time_s_0); wck_time_s_0=OMP_GET_WTIME()
  call       A3D_err_NS_2D ( A3D_uold, A3D_unew, A3Dr_mssgs, error_u)
  call cpu_time(cpu_time_s  ); wck_time_s  =OMP_GET_WTIME()
  nprcdr=8
  A3D_prfrmnc(nprcdr)%cpu_time_s=A3D_prfrmnc(nprcdr)%cpu_time_s+(cpu_time_s-cpu_time_s_0)
  A3D_prfrmnc(nprcdr)%wck_time_s=A3D_prfrmnc(nprcdr)%wck_time_s+(wck_time_s-wck_time_s_0)
  A3D_prfrmnc(nprcdr)%N_times_called=A3D_prfrmnc(nprcdr)%N_times_called+1
!-----------------------------------------------------------------------------------------------------------------------------------
!-                  ----------------------------------------------------------------------------------------------------------------
!-cnvrgnc_monitoring----------------------------------------------------------------------------------------------------------------
!-                  ----------------------------------------------------------------------------------------------------------------
!-----------------------------------------------------------------------------------------------------------------------------------
  Mits = m_it
  rdctn_u = alog10(abs(10.**error_u-10.**error_u_old))-error_u_old! write(*,*) reduction
! write(unit= * ,fmt='(2(a,i14),a,e14.7,1x,i14,1x,e14.7,e14.7)') 'it = ', it, ' n_it = ', n_it, ' error= ', error_u, m_it, rdctn_u, error_u_old
  error_u_old=error_u
  if(rdctn_u<=rdctn_TRG) exit
!-----------------------------------------------------------------------------------------------------------------------------------
!=dual time-stepping disabled for steady-state problems================================================
!   end do
!======================================================================================================
!-----------------------------------------------------------------------------------------------------------------------------------
  call cpu_time(cpu_time_s);        cpu_time_RUN = cpu_time_s-A3D_runtime_NS2D%cpu_time_s_run0
  wck_time_s = OMP_GET_WTIME(); elapsed_time_RUN = wck_time_s-A3D_runtime_NS2D%wck_time_s_run0
  cpu_timeit = cpu_time_s+A3D_runtime_NS2D%cpu_time_s_it0
  wck_timeit = wck_time_s+A3D_runtime_NS2D%wck_time_s_it0
  A3D_runtime_NS2D%cpu_time_s = cpu_timeit
  A3D_runtime_NS2D%wck_time_s = wck_timeit
! write(*,*) wck_time_s, A3D_runtime_NS2D%wck_time_s_run0, A3D_runtime_NS2D%cpu_time_s_run0, wck_timeit, cpu_timeit
!-->A3D::cpu  cpuit (it) = cpu_time_s
! if (R3Dr_mssgs%k_dbg>=1) write(*,*) ' elapsed_time_RUN = ', elapsed_time_RUN
  write(unit= * ,fmt='(a1,2(a,i14),a,2(e14.7))',ADVANCE='NO') achar(13), 'it = ', it, ' n_it = ', n_it, ' error= ', error_u, rdctn_u
! write(unit= * ,fmt='(a1,2(a,i14),a,e14.7)'             ) achar(13), 'it = ', it, ' n_it = ', n_it, ' error= ', error_u
! nlmnt=nc_iu; write(*,*) 'nc_iu: ', nc_iu, nc_iu+istepc, nc_iu+2*istepc, nc_iu+3*istepc, nc2iu
! Ngbs_lmnt=size(A3D_UGR1_2D_cells%nstncl(nlmnt)%n); do ind = 1, Ngbs_lmnt, 1; write(*,*) A3D_UGR1_2D_cells%nstncl(nlmnt)%n(ind); end do
!======>  if(nlmnt==nc2iu) write(*,*) 'nc2iu: ', nc2iu, Vnlmnt
  write(unit=077,fmt='(1x,i14,3(1x,e14.7),1x,i14,1x,e14.7)') it     ,&
                                                             error_u,&
                                                             A3D_runtime_NS2D%wck_time_s,&
                                                             A3D_runtime_NS2D%cpu_time_s,&
                                                                                    Mits,&
                                                                                 rdctn_u
!=====>A3D::wave===>  write(*,*) it, real(it)*minval(dtAD)/5.
  if ( elapsed_time_RUN >= elapsed_time_RUN_max ) exit
  if (     cpu_time_RUN >=     cpu_time_RUN_max ) exit
!-----------------------------------------------------------------------------------------------------------------------------------
  end do
  write(unit= * ,fmt='(a1)',ADVANCE='YES') achar(13)
  write(*,*) '     cpu_time_RUN = ',     cpu_time_RUN, '(s) = ',     cpu_time_RUN/3600., '(h)'
  write(*,*) ' elapsed_time_RUN = ', elapsed_time_RUN, '(s) = ', elapsed_time_RUN/3600., '(h)'
  write(*,*) '     cpu_time (s) = ', cpu_timeit      , '(s) = ', cpu_timeit      /3600., '(h)'
  write(*,*) '     wck_time (h) = ', wck_timeit      , '(s) = ', wck_timeit      /3600., '(h)'
  close(unit=077)
!-----------------------------------------------------------------------------------------------------------------------------------
!-    ------------------------------------------------------------------------------------------------------------------------------
!-    ------------------------------------------------------------------------------------------------------------------------------
!-    ------------------------------------------------------------------------------------------------------------------------------
!-    ------------------------------------------------------------------------------------------------------------------------------
!-    ------------------------------------------------------------------------------------------------------------------------------
!-A3Df------------------------------------------------------------------------------------------------------------------------------
!-    ------------------------------------------------------------------------------------------------------------------------------
!-    ------------------------------------------------------------------------------------------------------------------------------
!-    ------------------------------------------------------------------------------------------------------------------------------
!-    ------------------------------------------------------------------------------------------------------------------------------
!-    ------------------------------------------------------------------------------------------------------------------------------
!-----------------------------------------------------------------------------------------------------------------------------------
!
!-----------------------------------------------------------------------------------------------------------------------------------!-A3D_unstrctrd::HACK
!-           -----------------------------------------------------------------------------------------------------------------------!-A3D_unstrctrd::HACK
!-A3Df::write-----------------------------------------------------------------------------------------------------------------------!-A3D_unstrctrd::HACK
!-           -----------------------------------------------------------------------------------------------------------------------!-A3D_unstrctrd::HACK
!-----------------------------------------------------------------------------------------------------------------------------------!-A3D_unstrctrd::HACK
  open (unit=iof,file='A3Df_NS_2D',form='unformatted')
  write(unit=iof) it
!-----------------------------------------------------------------------
  if (it>0 ) then!-----------------------------------
  write(iof) A3D_runtime_NS2D%A3D_prgrm              !
  write(iof) A3D_runtime_NS2D%cpu_time_s     ,&      !
             A3D_runtime_NS2D%wck_time_s             !
  end if!<-------------------------------------------
!-----------------------------------------------------------------------
  k_thrm = 0                                   !<---
  write(unit=iof) k_thrm                       !<---
  if (k_thrm==0) then!------------------------ !<---
  buffer = 'air'                              !!<---
  write(unit=iof) buffer                      !!<---
  write(unit=iof) A3D_thrm_data%gamma,&       !!<---
                  A3D_thrm_data%Rg   ,&       !!<---
                  A3D_thrm_data%xmu0 ,&       !!<---
                  A3D_thrm_data%xlb0 ,&       !!<---
                  A3D_thrm_data%Tmu0 ,&       !!<---
                  A3D_thrm_data%Smu  ,&       !!<---
                  A3D_thrm_data%Alb           !!<---
  end if!<------------------------------------ !<---
!-----------------------------------------------------------------------
  write(unit=iof) Nvrtcs, Nlmnts, Ndgs, Nbndlmnts, Nbnddgs, Nbndphntms
  write(*,*)      Nvrtcs, Nlmnts, Ndgs, Nbndlmnts, Nbnddgs, Nbndphntms
!-----------------------------------------------------------------------
!-     -----------------------------------------------------------------
!-write-----------------------------------------------------------------
!-     -----------------------------------------------------------------
!-----------------------------------------------------------------------
  write(unit=iof) ( A3D_UGDS_cells%x_vrtx(np)%x, np = 1, Nvrtcs, 1)
  write(unit=iof) ( A3D_UGDS_cells%x_vrtx(np)%y, np = 1, Nvrtcs, 1)
  write(unit=iof) ( A3D_UGDS_cells%x_vrtx(np)%z, np = 1, Nvrtcs, 1)
  do nlmnt = 1, Nlmnts, 1!---------------------------------------------------------------
  write(unit=iof) Nvrts_nlmnt                                                            !
  write(unit=iof) ( A3D_UGDS_cells%np_nlmnt(nlmnt)%n(ind), ind = 1, Nvrts_nlmnt, 1)      !
  end do!<-------------------------------------------------------------------------------
  write(unit=iof) ( A3D_UGDS_cells% x_lmnt(nlmnt)%x, nlmnt = 1, Nlmnts, 1)
  write(unit=iof) ( A3D_UGDS_cells% x_lmnt(nlmnt)%y, nlmnt = 1, Nlmnts, 1)
  write(unit=iof) ( A3D_UGDS_cells% x_lmnt(nlmnt)%z, nlmnt = 1, Nlmnts, 1)
  write(unit=iof) ( A3D_UGDS_cells%dl_lmnt(nlmnt)  , nlmnt = 1, Nlmnts, 1)
  write(unit=iof) ( A3D_UGDS_cells% d_lmnt(nlmnt)  , nlmnt = 1, Nlmnts, 1)
  write(unit=iof) ( A3D_UGDS_cells% A_lmnt(nlmnt)  , nlmnt = 1, Nlmnts, 1)
  do np = 1, Nvrtcs, 1!-----------------------------------------------------------
  write(unit=iof) Nlmnts_np                                                       !
  write(unit=iof) ( A3D_UGDS_dgs%nlmnt_np(np)%n(ind), ind = 1, Nlmnts_np, 1)      !
  end do!<------------------------------------------------------------------------
  write(unit=iof) ( A3D_UGDS_dgs%dg_data(ndg)%n_element_1, ndg = 1, Ndgs, 1)
  write(unit=iof) ( A3D_UGDS_dgs%dg_data(ndg)%n_element_2, ndg = 1, Ndgs, 1)
  write(unit=iof) ( A3D_UGDS_dgs%dg_data(ndg)%  n_point_1, ndg = 1, Ndgs, 1)
  write(unit=iof) ( A3D_UGDS_dgs%dg_data(ndg)%  n_point_2, ndg = 1, Ndgs, 1)
  do nlmnt = 1, Nlmnts, 1!-------------------------------------------------------------
  write(unit=iof) Ndgs_nlmnt                                                           !
  write(unit=iof) ( A3D_UGDS_dgs%ndg_nlmnt(nlmnt)%n(ind), ind = 1, Ndgs_nlmnt, 1)      !
  end do!<-----------------------------------------------------------------------------
!-----------------------------------------------------------------------
  write(unit=iof) ( A3D_UGDS_2D_bndrs%nlmnt_nbndlmnt  (nbndlmnt) , nbndlmnt  = 1, Nbndlmnts , 1)
  write(unit=iof) ( A3D_UGDS_2D_bndrs%ndg_nbndlmnt    (nbndlmnt) , nbndlmnt  = 1, Nbndlmnts , 1)
  write(unit=iof) ( A3D_UGDS_2D_bndrs%nlmnt_nbnddg    (nbnddg)   , nbnddg    = 1, Nbnddgs   , 1)
  write(unit=iof) ( A3D_UGDS_2D_bndrs%nbndlmnt_nbnddg (nbnddg)   , nbnddg    = 1, Nbnddgs   , 1)
  write(unit=iof) ( A3D_UGDS_2D_bndrs%ndg_nbnddg      (nbnddg)   , nbnddg    = 1, Nbnddgs   , 1)
  write(unit=iof) ( A3D_UGDS_2D_bndrs%kBC_lmnt        (nbndlmnt) , nbndlmnt  = 1, Nbndlmnts , 1)
  write(unit=iof) ( A3D_UGDS_2D_bndrs%kBC_dg          (nbnddg)   , nbnddg    = 1, Nbnddgs   , 1)
  write(unit=iof) ( A3D_UGDS_2D_bndrs%nBCdata         (nbnddg)   , nbnddg    = 1, Nbnddgs   , 1)!<---
  write(unit=iof) ( A3D_UGDS_2D_bndrs%nlmnt_nbndphntm (nbndphntm), nbndphntm = 1, Nbndphntms, 1)
  write(unit=iof) ( A3D_UGDS_2D_bndrs%nbnddg_nbndphntm(nbndphntm), nbndphntm = 1, Nbndphntms, 1)
  write(unit=iof) ( A3D_UGDS_2D_bndrs%kBC_phntm       (nbndphntm), nbndphntm = 1, Nbndphntms, 1)
!-----------------------------------------------------------------------
  write(unit=iof) ( isreallmnt(nlmnt)       , nlmnt = 1, Nlmnts, 1)
  write(unit=iof) (   A3D_unew(nlmnt)%rho   , nlmnt = 1, Nlmnts, 1)
  write(unit=iof) (   A3D_unew(nlmnt)%rhoV%x, nlmnt = 1, Nlmnts, 1)
  write(unit=iof) (   A3D_unew(nlmnt)%rhoV%y, nlmnt = 1, Nlmnts, 1)
  write(unit=iof) (   A3D_unew(nlmnt)%rhoet , nlmnt = 1, Nlmnts, 1)
!-----------------------------------------------------------------------
  write(unit=iof) NBCdatas                                                       !<---
  write(unit=iof) (A3D_UGDS_2D_bndrs%BCdata(nBC)%vrbls%rho, nBC = 1, NBCdatas, 1)!<---
  write(unit=iof) (A3D_UGDS_2D_bndrs%BCdata(nBC)%vrbls%  p, nBC = 1, NBCdatas, 1)!<---
  write(unit=iof) (A3D_UGDS_2D_bndrs%BCdata(nBC)%vrbls%  T, nBC = 1, NBCdatas, 1)!<---
  write(unit=iof) (A3D_UGDS_2D_bndrs%BCdata(nBC)%vrbls%V%x, nBC = 1, NBCdatas, 1)!<---
  write(unit=iof) (A3D_UGDS_2D_bndrs%BCdata(nBC)%vrbls%V%y, nBC = 1, NBCdatas, 1)!<---
  write(unit=iof) (A3D_UGDS_2D_bndrs%BCdata(nBC)%vrbls%V%z, nBC = 1, NBCdatas, 1)!<---
  write(unit=iof) (A3D_UGDS_2D_bndrs%BCdata(nBC)%qn       , nBC = 1, NBCdatas, 1)!<---
!-----------------------------------------------------------------------
  close(unit=iof)
!-----------------------------------------------------------------------------------------------------------------------------------
!-                ------------------------------------------------------------------------------------------------------------------
!-                ------------------------------------------------------------------------------------------------------------------
!-                ------------------------------------------------------------------------------------------------------------------
!-                ------------------------------------------------------------------------------------------------------------------
!-                ------------------------------------------------------------------------------------------------------------------
!-A3D_NS_2D::plots------------------------------------------------------------------------------------------------------------------
!-                ------------------------------------------------------------------------------------------------------------------
!-                ------------------------------------------------------------------------------------------------------------------
!-                ------------------------------------------------------------------------------------------------------------------
!-                ------------------------------------------------------------------------------------------------------------------
!-                ------------------------------------------------------------------------------------------------------------------
!-----------------------------------------------------------------------------------------------------------------------------------
  call cpu_time(cpu_time_s_0); wck_time_s_0=OMP_GET_WTIME()
  allocate(gwrk(Nvrtcs))
  allocate(hwrk(Nvrtcs))
  allocate(iwrk(Nvrtcs))
  allocate(fwrk(Nlmnts)); fwrk(1:Nlmnts:1)=A3D_vrbls(1:Nlmnts:1)%V%x
  call R3D_to_plots_2D_unstrcrd ( 013, 'Vxnp1', 'write', 'vtk', 'Vx', 'Vx',&
                                  A3D_UGDS_cells%x_vrtx, A3D_UGDS_cells%np_nlmnt, fwrk, 'cell')
!-----------------------------------------------------------------------
                          fwrk(1:Nlmnts:1)=A3D_vrbls(1:Nlmnts:1)%V%y
  call R3D_to_plots_2D_unstrcrd ( 013, 'Vynp1', 'write', 'vtk', 'Vy', 'Vy',&
                                  A3D_UGDS_cells%x_vrtx, A3D_UGDS_cells%np_nlmnt, fwrk, 'cell')
!-----------------------------------------------------------------------
                          fwrk(1:Nlmnts:1)=A3D_vrbls(1:Nlmnts:1)%p
  call R3D_to_plots_2D_unstrcrd ( 013, 'pnp1', 'write', 'vtk', 'p', 'p',&
                                  A3D_UGDS_cells%x_vrtx, A3D_UGDS_cells%np_nlmnt, fwrk, 'cell')
!-----------------------------------------------------------------------
                          fwrk(1:Nlmnts:1)=A3D_vrbls(1:Nlmnts:1)%rho
  call R3D_to_plots_2D_unstrcrd ( 013, 'rhonp1', 'write', 'vtk', 'rho', 'rho',&
                                  A3D_UGDS_cells%x_vrtx, A3D_UGDS_cells%np_nlmnt, fwrk, 'cell')
!-----------------------------------------------------------------------
                          fwrk(1:Nlmnts:1)=A3D_vrbls(1:Nlmnts:1)%T
  call R3D_to_plots_2D_unstrcrd ( 013, 'Tnp1', 'write', 'vtk', 'T', 'T',&
                                  A3D_UGDS_cells%x_vrtx, A3D_UGDS_cells%np_nlmnt, fwrk, 'cell')
!-----------------------------------------------------------------------
                          fwrk(1:Nlmnts:1)=sqrt((A3D_vrbls(1:Nlmnts:1)%V%x**2+A3D_vrbls(1:Nlmnts:1)%V%y**2)/&
                                                (A3D_thrm_data%gamma*A3D_thrm_data%Rg*A3D_vrbls(1:Nlmnts:1)%T))
  call R3D_to_plots_2D_unstrcrd ( 013, 'xMnp1', 'write', 'vtk', 'xM', 'xM',&
                                  A3D_UGDS_cells%x_vrtx, A3D_UGDS_cells%np_nlmnt, fwrk, 'cell')
!-----------------------------------------------------------------------
                          fwrk(1:Nlmnts:1)=A3D_UGDS_cells%d_lmnt(1:Nlmnts:1)
  call R3D_to_plots_2D_unstrcrd ( 013, 'dlnp1', 'write', 'vtk', 'dl', 'dl',&
                                  A3D_UGDS_cells%x_vrtx, A3D_UGDS_cells%np_nlmnt, fwrk, 'cell')
!-----------------------------------------------------------------------
                          fwrk(1:Nlmnts:1)=1./(dtlcl(1:Nlmnts:1)/A3D_UGDS_cells%d_lmnt(1:Nlmnts:1))
  call R3D_to_plots_2D_unstrcrd ( 013, 'Snp1', 'write', 'vtk', 'S', 'S',&
                                  A3D_UGDS_cells%x_vrtx, A3D_UGDS_cells%np_nlmnt, fwrk, 'cell')
!-----------------------------------------------------------------------
                          fwrk(1:Nlmnts:1)=dtlcl(1:Nlmnts:1)
  call R3D_to_plots_2D_unstrcrd ( 013, 'dtnp1', 'write', 'vtk', 'dt', 'dt',&
                                  A3D_UGDS_cells%x_vrtx, A3D_UGDS_cells%np_nlmnt, fwrk, 'cell')
!=plot velocity vector field: reconstruct Vx, Vy on vertices=========================================================
                          fwrk(1:Nlmnts:1)=A3D_vrbls(1:Nlmnts:1)%V%x                                                 !
  call A3D_2D_lsq_R1_vrtcs ( k_srdr, A3D_UGDS_cells%x_vrtx, A3D_UGDS_dgs%nlmnt_np, A3D_UGDS_cells%x_lmnt,&           !
                             A3D_UGR1_2D_cells%p_R1_coeffs, A3D_UGR1_2D_cells%nstncl, A3D_UGR1_2D_cells%dx_lmnt,&    !
                             fwrk, gwrk )                                                                            !
                          fwrk(1:Nlmnts:1)=A3D_vrbls(1:Nlmnts:1)%V%y                                                 !
  call A3D_2D_lsq_R1_vrtcs ( k_srdr, A3D_UGDS_cells%x_vrtx, A3D_UGDS_dgs%nlmnt_np, A3D_UGDS_cells%x_lmnt,&           !
                             A3D_UGR1_2D_cells%p_R1_coeffs, A3D_UGR1_2D_cells%nstncl, A3D_UGR1_2D_cells%dx_lmnt,&    !
                             fwrk, hwrk )                                                                            !
                          iwrk(1:Nvrtcs:1)=sqrt(gwrk**2.+hwrk**2.)                                                   !
  call R3D_to_plots_2D_unstrcrd_vector ( 013, 'Vvector', 'write', 'vtk', 'V', 'Vmagnitude', 'Vvector',&              !
                                         A3D_UGDS_cells%x_vrtx, A3D_UGDS_cells%np_nlmnt, iwrk, gwrk, hwrk)           !
                                                                                                                     !
!                                                                                                         Xiasu YANG !
!                                                                                                         07/03/2019 !
!====================================================================================================================
!=====>A3D::wave===>!-----------------------------------------------------------------------
!=====>A3D::wave===>                          fwrk(1:Nlmnts:1)=vnew(1:Nlmnts:1)
!=====>A3D::wave===>  call R3D_to_plots_2D_unstrcrd ( 013, 'wvnp1', 'write', 'vtk', 'wv', 'wv',&
!=====>A3D::wave===>                                  A3D_UGDS_cells%x_vrtx, A3D_UGDS_cells%np_nlmnt, fwrk, 'cell')
  deallocate(fwrk); deallocate(gwrk); deallocate(hwrk); deallocate(iwrk)
  call cpu_time(cpu_time_s  ); wck_time_s  =OMP_GET_WTIME()
  nprcdr=2
  A3D_prfrmnc(nprcdr)%cpu_time_s=A3D_prfrmnc(nprcdr)%cpu_time_s+(cpu_time_s-cpu_time_s_0)
  A3D_prfrmnc(nprcdr)%wck_time_s=A3D_prfrmnc(nprcdr)%wck_time_s+(wck_time_s-wck_time_s_0)
  A3D_prfrmnc(nprcdr)%N_times_called=A3D_prfrmnc(nprcdr)%N_times_called+9
!-----------------------------------------------------------------------------------------------------------------------------------
!-    ------------------------------------------------------------------------------------------------------------------------------
!-work------------------------------------------------------------------------------------------------------------------------------
!-    ------------------------------------------------------------------------------------------------------------------------------
!-----------------------------------------------------------------------------------------------------------------------------------
  do nprcdr = 1, Nprcdrs, 1
  write(*,*) 'called ', A3D_prfrmnc(nprcdr)%N_times_called,&
             ' times, eta = ', A3D_prfrmnc(nprcdr)%cpu_time_s/A3D_prfrmnc(nprcdr)%wck_time_s, ': ',&
             A3D_prfrmnc(nprcdr)%name
  end do
!-----------------------------------------------------------------------------------------------------------------------------------
!-    ------------------------------------------------------------------------------------------------------------------------------
!-work------------------------------------------------------------------------------------------------------------------------------
!-    ------------------------------------------------------------------------------------------------------------------------------
!-----------------------------------------------------------------------------------------------------------------------------------
  write(*,*) '==>', sign(1.,30.), sign(1.,-30.),sign(1.,0.)                                                                          !<-working_write
!-----------------------------------------------------------------------------------------------------------------------------------
!-                     -------------------------------------------------------------------------------------------------------------
!-                     -------------------------------------------------------------------------------------------------------------
!-                     -------------------------------------------------------------------------------------------------------------
!-                     -------------------------------------------------------------------------------------------------------------
!-                     -------------------------------------------------------------------------------------------------------------
!-end program A3D_NS_2D-------------------------------------------------------------------------------------------------------------
!-                     -------------------------------------------------------------------------------------------------------------
!-                     -------------------------------------------------------------------------------------------------------------
!-                     -------------------------------------------------------------------------------------------------------------
!-                     -------------------------------------------------------------------------------------------------------------
!-                     -------------------------------------------------------------------------------------------------------------
!-----------------------------------------------------------------------------------------------------------------------------------
  end program A3D_NS_2D
